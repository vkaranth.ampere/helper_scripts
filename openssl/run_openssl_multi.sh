#!/bin/bash

out=openssl_multi.txt
#if [ ! -z $1 ]; then
#	out=$1
#fi
#alg=(md2 mdc2 md4 md5 hmac sha1 rmd160 rc4 des-cbc des-cbc des-ede3 idea-cbc seed-cbc rc2-cbc blowfish cast-cbc aes-128-cbc aes-192-cbc aes-256-cbc camellia sha256 sha512 whirlpool aes-128-ige aes-192-ige aes-256-ige ghash rand)
#
#for a in ${alg[@]}; do
#	openssl speed -multi 64 $a | tee tmp.txt
#	awk '/D_FORTIFY_SOURCE/{y=1;next}y' tmp.txt >> $out
#        
#done
#
#alg=(rsa dsa ecdsa)
#for a in ${alg[@]}; do
#	openssl speed -multi 64 $a | tee tmp.txt
#	awk '/sign/{y=1;next}y' tmp.txt >> $out
#done

alg=(ecdh)
for a in ${alg[@]}; do
	openssl speed -multi 64 $a | tee tmp.txt
	awk '/op/{y=1;next}y' tmp.txt >> $out
done
