#!/bin/bash

export GCC_VERSION=${GCC_VERSION-"12.2.0"}
SCRIPT_DIR_NAME="$(realpath "$(dirname "$(readlink -f "$0")")")"
export SCRIPT_DIR_NAME
export GCC_HOME=/opt/install/gcc-${GCC_VERSION}
#export GCC_HOME=${SCRIPT_DIR_NAME}/install/gcc-${GCC_VERSION}
export GCC_BIN_DIR=${GCC_HOME}/bin
export JDK_TARBALL="jdk-11.0.12+5.tar.gz"
export JDK_VERSION=11
export JAVA_HOME="${SCRIPT_DIR_NAME}/install/openjdk-${JDK_VERSION}"
export BAZEL_VERSION="5.3.0"
export HINT_FILE="/cloud_benchmarks/start_pmu_collection.txt"

#export HARNESS_ORIGIN=$(git config --get remote.origin.url}
#export HARNESS_VERSION=$(git rev-parse HEAD)
echo_and_run() {
    (set -x; "$@")

    if [[ $? != 0 ]]; then
        set -f
        echo -e "ERROR/FAILURE: \n" "$@"
        set +f
        exit 1
    fi
}

#if both SERVER_CORES and CLIENT_CORES are set, there is no
#need to set them based on cpus and machine type.
#if only one of the variables are set, report error.
is_get_cpus_needed() {
  if [[ ${SERVER_CORES+x} && ${CLIENT_CORES+x} ]] ; then
    return 0
  elif [[ ${SERVER_CORES+x} || ${CLIENT_CORES+x} ]] ; then
    echo "You need to specify both SERVER_CORES and CLIENT_CORES"
    exit 1
  else
    echo "Generating core bindings."
    return 1
  fi
}

get_cpus() {
  valid_threads=( 2 4 8 16 32 )
  if [[ " ${valid_threads[@]} " =~ " ${1} " ]]; then
    thread=${1}
  else
    echo "Unsupported thread count. Supported values are: { 2, 4, 8, 16, 32}."
    exit 1
  fi
  # To assign cpus that are on the same physical core, the same ccd, ccx.
  declare -A ARM_server=(
    [2]='0-1' [4]='0-3' [8]='0-7' [16]='0-15' [32]='0-31' )
  declare -A ARM_client=(
    [2]='2-3' [4]='4-7' [8]='8-15' [16]='16-31' [32]='32-63' )
  model='ARM'

  export SERVER_CORES=${ARM_server[$thread]}
  export CLIENT_CORES=${ARM_client[$thread]}

}

install_packages() {
  if [[ -z ${GCC_INSTALLED+x} ]]; then
    bash "${SCRIPT_DIR_NAME}/prepare.sh"
  fi
}

get_platform_dependent_flags() {
#TODO(yunlian@)
#Add a flag to set flags for ARM, current flag is for Connan only.
    if uname -a |grep aarch64; then
      export EXTRA_CFLAGS="-O3 -mcpu=neoverse-n1 -march=armv8.2-a+lse+crc+crypto+dotprod -moutline-atomics -Wl,--build-id"
      export EXTRA_CXXFLAGS="-O3 -mcpu=neoverse-n1 -march=armv8.2-a+lse+crc+crypto+dotprod -moutline-atomics -Wl,--build-id"
    else
      echo "Cannot figure out platform specific compiler flags."
      echo "We currently only support aarch64, check \"uname -a\""
      exit 1
    fi
    EXTRA_CFLAGS="${EXTRA_CFLAGS} -Wl,-rpath,${GCC_HOME}/lib64"
    EXTRA_CXXFLAGS="${EXTRA_CXXFLAGS} -Wl,-rpath,${GCC_HOME}/lib64"
}

dump_knobs() {
    set +e
    logfile=${1:-${TEST_HOME}/knobs.${RUNTAG}.txt}

    echo_and_run hostname >> "$logfile" 2>&1
    echo_and_run uname -a >> "$logfile" 2>&1
    echo_and_run cat /etc/os-release >> "$logfile" 2>&1

    # We append some environment info to the logfile
    echo_and_run getconf PAGESIZE >> "$logfile" 2>&1

    echo_and_run cat /proc/meminfo >> "$logfile" 2>&1

    echo_and_run sudo swapon -show >> "$logfile" 2>&1

    for i in /sys/kernel/mm/hugepages/*/nr_hugepages; do
      echo_and_run echo "$i: $(cat "$i")" >> "$logfile" 2>&1
    done

    echo_and_run cpupower frequency-info >> "$logfile" 2>&1
    echo_and_run cpupower idle-info >> "$logfile" 2>&1
    echo_and_run cat /sys/kernel/mm/transparent_hugepage/enabled >> \
    "$logfile" 2>&1
    echo_and_run cat /sys/kernel/mm/transparent_hugepage/defrag >> \
    "$logfile" 2>&1
    if [[ -f /proc/sys/kernel/numa_balancing ]]; then
      echo_and_run cat /proc/sys/kernel/numa_balancing >> "$logfile" 2>&1
    fi
    if [[ -f /etc/sysconfig/irqbalance ]]; then
      echo_and_run cat /etc/sysconfig/irqbalance >> "$logfile" 2>&1
    elif [[ -f /etc/default/irqbalance ]]; then
      echo_and_run cat /etc/default/irqbalance >> "$logfile" 2>&1
    fi
    echo_and_run cat /proc/interrupts >> "$logfile" 2>&1
    if command -v dmidecode &> /dev/null ; then
      echo_and_run sudo dmidecode  >> "$logfile" 2>&1
    fi
    echo_and_run numastat -mn >> "$logfile" 2>&1
    echo_and_run sudo sysctl -a >> "$logfile" 2>&1
    echo_and_run cat /proc/cpuinfo >> "$logfile" 2>&1
}

mount_ramdisk() {
  if [[ ${NO_MOUNT_TMPFS+x} ]]; then
    return 0
  fi
  mkdir -p "$1"
  memory_size="${3:-128}"
  sudo mount -t tmpfs -o size="${memory_size}"g,mpol=prefer:0 tmpfs "$1"
  sudo chown -R "${2:-${USER}}" "$1"
  sudo chmod -R 750  "$1"
  df || true
}

umount_ramdisk() {
  if [[ ${NO_MOUNT_TMPFS+x} ]]; then
    return 0
  fi
  sudo umount "$1"
  df || true
}

install_gcc() {
  cd "${SCRIPT_DIR_NAME}"
  rm -rf "gcc-${GCC_VERSION}.tar.gz"
	wget \
  "https://bigsearcher.com/mirrors/gcc/releases/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.gz"
	build_gcc
}

build_gcc () {
    cd "${SCRIPT_DIR_NAME}"
    tar xf "gcc-${GCC_VERSION}.tar.gz"
    cd "${SCRIPT_DIR_NAME}/gcc-${GCC_VERSION}"
    ./contrib/download_prerequisites
    INSTALL=/opt/install/gcc-12.2.0 CFLAGS=-O3 CXXFLAGS=-O3 ./configure \
        --prefix="${GCC_HOME}"/ \
        --enable-languages=c++,c --disable-bootstrap \
        --enable-stage1-languages=c,c++ --disable-multilib \
        --disable-libsanitizer --enable-linker-build-id
    [[ "$?" != 0 ]] && {
        echo "error: gcc configure"
        exit 1
    }
    make -j50 || {
        echo "error: gcc build"
        exit 1
    }
    make install || {
        echo "error: gcc install"
        exit 1
    }
    cd "${SCRIPT_DIR_NAME}"
    rm -rf "gcc-${GCC_VERSION}"
    rm -rf "gcc-${GCC_VERSION}.tar.gz"
}

install_bootstrap_openjdk() {
    if [[ ! -z ${GCC_INSTALLED+x} ]]; then
      return 0
    fi
    cd "${SCRIPT_DIR_NAME}"
    wget "https://github.com/openjdk/jdk11u-dev/archive/refs/tags/${JDK_TARBALL}"
    tar xf "${JDK_TARBALL}"
    get_platform_dependent_flags
    MY_CFLAGS="${EXTRA_CFLAGS} -lgcc_s -fPIC "
    EXTRA_LDFLAGS="-Wl,--allow-multiple-definition -Wl,-lgcc_s -lgcc_s -Wl,-rpath,${GCC_HOME}/lib64"
    mkdir -p "${JAVA_HOME}"
    cd "${SCRIPT_DIR_NAME}"
    JAVA_SOURCE_DIR="$(ls -d */ | grep jdk)"
    cd "${SCRIPT_DIR_NAME}/${JAVA_SOURCE_DIR}"
    CC="${GCC_BIN_DIR}/gcc" CXX="${GCC_BIN_DIR}/g++" bash ./configure \
      --prefix="${JAVA_HOME}" --disable-warnings-as-errors \
      --with-extra-cflags="${MY_CFLAGS}" \
      --with-extra-cxxflags="${MY_CFLAGS}" \
      --with-extra-ldflags="${EXTRA_LDFLAGS}" \
      2>&1 |tee configure.out
    make -j
    make install
    cd "${SCRIPT_DIR_NAME}"
    rm -rf jdk*
}

install_bazel() {
  if [[ ! -z ${GCC_INSTALLED+x} ]]; then
    return 0
  fi
  cd "${SCRIPT_DIR_NAME}"
  wget "https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VERSION}/bazel-${BAZEL_VERSION}-linux-arm64"
  sudo mv "bazel-${BAZEL_VERSION}-linux-arm64" /usr/bin/bazel
  sudo chmod a+rx /usr/bin/bazel
}

create_hint_file() {
  touch $HINT_FILE
}
