#!/bin/bash
set -e
export TEST_HOME=${TEST_HOME-${PWD}/mcd}
export MCD_VERSION=1.6.17
export MCD_HOME=${TEST_HOME}
export MCD_SRC=$MCD_HOME/memcached-$MCD_VERSION
export MCPERF_SRC=$MCD_HOME/memcache-perf
export MCD_INSTALL=$MCD_HOME/install
export MCD_BINDIR=$MCD_INSTALL/memcached-$MCD_VERSION
export MCD=$MCD_INSTALL/memcached-$MCD_VERSION/bin/memcached
export MCPERF=$MCD_INSTALL/memcached-$MCD_VERSION/bin/mcperf
SCRIPT_DIR_NAME="$(dirname "$(readlink -f "$0")")"
export SCRIPT_DIR_NAME
export THREADS=8
source ./common.sh
export MEMORY=32678

# mcperf parameters affecting the generated load
export MCPERF_DEPTH=10
export MCPERF_KEYSIZE=30
export MCPERF_VALUESIZE=200
export MCPERF_RECORDS=10000
export MCPERF_UPDATE=0.0
export MCPERF_CONNECTIONS_PER_SERVER=1
export MCPERF_BENCHMARK_RUNTIME_SECONDS=60

export PROFILE=5

RUN_ALL_STAGES=1

install_benchmark() {

  cd "${SCRIPT_DIR_NAME}"
  install_packages

  mkdir -p "$TEST_HOME"
  cd "$TEST_HOME"
  mkdir -p "$MCD_INSTALL"
  cd "$TEST_HOME"
  #wget "https://www.memcached.org/files/memcached-${MCD_VERSION}.tar.gz"
  #git clone https://github.com/shaygalon/memcache-perf.git
  #tar xf "memcached-${MCD_VERSION}.tar.gz"
  cd "$MCD_SRC"
  get_platform_dependent_flags

  CC="${GCC_BIN_DIR}/gcc" CXX="${GCC_BIN_DIR}/g++" \
  CFLAGS="${EXTRA_CFLAGS} -Wno-error=maybe-uninitialized" \
  CXXFLAGS="${EXTRA_CXXFLAGS} -Wno-error=maybe-uninitialized" \
  ./configure --prefix="${TEST_HOME}"

  make -j 50
  make install

  cd "$MCPERF_SRC"
  git checkout 0afbe9b1dc2518283214504d92a7cd3d2599c006
  CC="${GCC_BIN_DIR}/gcc" CXX="${GCC_BIN_DIR}/g++" \
    LD="${GCC_BIN_DIR}/g++" CFLAGS="${EXTRA_CFLAGS} -fPIE " \
    XFLAGS="${EXTRA_CXXFLAGS} -fPIE " make
  cp mcperf "${TEST_HOME}"/bin
  rm -rf "${TEST_HOME}"/include
  rm -rf "${TEST_HOME}"/install
  rm -rf "${TEST_HOME}"/share
  rm -rf "${TEST_HOME}"/memcache*
}


parse_args() {
    local OPTIND
    while getopts "c:s:t:m:d:hIiz:vK:V:r:u:C:T:p:" opt; do
      case "$opt" in
        h)
            echo "$0 [-h] [-i] [-p] [-c] [-s] [-I] [-m MEMCACHED[-v]"
            echo "    -c: The cores that runs client. default 16-27"
            echo "    -s: The cores that server. default 0-15"
            echo "    -t: Number of threads to use for both memcached and mcperf, individually"
            echo "    -i: Build and install gcc, memcached before run them"
            echo "    -h: Show this help message"
            echo "    -I: Only build the benchmark"
            echo "    -m: The memory allocated for memcached in MB. Default is 32768."
            echo "    -d: Max depth of pipeline requests, default is 10."
            echo "    -z: run only a specific stage. One of server, client_load, client_bench. This is to isolate actual server work (which needs PMUs collected) and client work. Does not affect -i flag."
            echo "    -K: Length of memcached keys. default ${MCPERF_KEYSIZE}"
            echo "    -V: Length of memcached values. default ${MCPERF_VALUESIZE}"
            echo "    -r: Number of memcached records to use. default ${MCPERF_RECORDS}"
            echo "    -u: Ratio of set:get commands. default ${MCPERF_UPDATE}"
            echo "    -C: Mcperf connections to establish per server"
            echo "    -T: Seconds for which to run the benchmark phase. default 60"
            echo "    -p: The default profile to use. 0-9 only, most importantly, 5-9"
            echo "    -v: verbose"
            exit
            ;;
        v)
            set -vx
            ;;
        i)
            export INSTALL_SOFTWARE=1
            ;;
        I)
            export INSTALL_SOFTWARE=1
            export INSTALL_ONLY=1
            ;;
        p)
            export PROFILE="$OPTARG"
            ;;
        c)
            CLIENT_CORES="$OPTARG"
            ;;
        s)
            SERVER_CORES="$OPTARG"
            ;;
        t)
            THREADS="$OPTARG"
            ;;
        m)
            MEMORY="$OPTARG"
            ;;
        d)
            MCPERF_DEPTH="$OPTARG"
            ;;
        K)
            MCPERF_KEYSIZE="$OPTARG"
            ;;
        V)
            MCPERF_VALUESIZE="$OPTARG"
            ;;
        r)
            MCPERF_RECORDS="$OPTARG"
            ;;
        u)
            MCPERF_UPDATE="$OPTARG"
            ;;
        C)
            MCPERF_CONNECTIONS_PER_SERVER="$OPTARG"
            ;;
        z)
            unset RUN_ALL_STAGES
            case "$OPTARG" in
              "server") SERVER_ONLY=1 ;;
              "client_setup") CLIENT_SETUP_ONLY=1 ;;
              "client_bench") CLIENT_BENCH_ONLY=1 ;;
              *) echo "ILLEGAL -z option" && exit 1 ;;
            esac
            ;;
        T)
            MCPERF_BENCHMARK_RUNTIME_SECONDS="$OPTARG"
            ;;
      esac
    done
}

run_bench() {
  cd "${TEST_HOME}"
  if [[ ! -z $SERVER_ONLY || $RUN_ALL_STAGES ]]; then
    # when running all stages, must run as daemon, wheras must block when
    # running server only, so that container doesn't exit
    if [[ $RUN_ALL_STAGES ]]; then
      DAEMON_ARG="-d"
    else
      DAEMON_ARG=""
    fi
    sudo pkill memcached || sleep 3

    #-L doesnt work on fedora
    #numactl -m0 -C "${SERVER_CORES}" "${TEST_HOME}"/bin/memcached -m "${MEMORY}" -u root -t "${THREADS}" -L ${DAEMON_ARG}
    numactl -m0 -C "${SERVER_CORES}" "${TEST_HOME}"/bin/memcached -m "${MEMORY}" -u root -t "${THREADS}"  ${DAEMON_ARG}
  fi


  if [[ $RUN_ALL_STAGES ]]; then sleep 20; fi

  if [[ ! -z $PROFILE ]]; then
    MCPERF_LOAD_ARGS=(--profile=${PROFILE})

    MCPERF_LOAD_ARGS=("${MCPERF_LOAD_ARGS[@]}"
        -T "${CLIENT_THREADS}")

  else
    MCPERF_LOAD_ARGS=(-T "${THREADS}"
                      -d "${MCPERF_DEPTH}"
                      -K "${MCPERF_KEYSIZE}"
                      -V "${MCPERF_VALUESIZE}"
                      -r "${MCPERF_RECORDS}"
                      -u "${MCPERF_UPDATE}"
                      -c "${MCPERF_CONNECTIONS_PER_SERVER}")
  fi
  if [[ ! -z $CLIENT_SETUP_ONLY || $RUN_ALL_STAGES ]]; then
    echo "Start to load data."
    numactl -m0 -C "${CLIENT_CORES}" "${TEST_HOME}"/bin/mcperf -s 127.0.0.1 -t 20 "${MCPERF_LOAD_ARGS[@]}" --loadonly
  fi

  MCPERF_BENCH_ARGS=("${MCPERF_LOAD_ARGS[@]}"
                     -t "${MCPERF_BENCHMARK_RUNTIME_SECONDS}")

  if [[ ! -z $CLIENT_BENCH_ONLY || $RUN_ALL_STAGES ]]; then
    sleep 10;
    echo "Start to run benchmark."
    numactl -m0 -C "${CLIENT_CORES}" "${TEST_HOME}"/bin/mcperf -s 127.0.0.1 "${MCPERF_BENCH_ARGS[@]}" --noload  2>&1 |tee output_"${PROFILE}_${THREADS}".txt
  fi

  if [[ $RUN_ALL_STAGES ]]; then sudo pkill memcached; fi
}

parse_args "$@"

CLIENT_THREADS="${THREADS}"

if [[ "$INSTALL_SOFTWARE" == 1 ]]; then
  install_benchmark
  if [[ "$INSTALL_ONLY" == 1 ]]; then
        echo "Installation only"
        exit 0
  fi
elif [[ ! -d "${TEST_HOME}" ]]; then
  echo Working space "${TEST_HOME}" not found, add -i option to install first.
  exit 1
fi

if [[ $(is_get_cpus_needed) ]]; then

  get_cpus "${THREADS}"

  if [[ ! -z $PROFILE && $PROFILE -ge 7 ]]; then
    CLIENT_CORES_TMP="${CLIENT_CORES}"
    THREADS="$(( ${THREADS} / 4 ))"
    get_cpus "${THREADS}"
    CLIENT_CORES="${CLIENT_CORES_TMP}"
  fi

fi

trap "echo;echo;echo;echo;echo Interrupted!; \
  sudo pkill memcached ; \
  sudo pkill mcperf ; \
  exit 255" \
  SIGINT SIGTERM
run_bench
