#!/bin/bash

echo "Install system packages..."

source ./common.sh

if which apt-get >& /dev/null ; then
  sudo apt-get update -y
  sudo apt-get install -y texinfo sysstat autoconf pkg-config libtool make \
        bison flex zlib1g-dev libreadline-dev numactl bzip2 sysstat autoconf \
        build-essential libc6-dev wget git lsof procps openssl libssl-dev \
        linux-cpupower linux-perf libevent-dev libzmq3-dev cups fontconfig \
        time wget libasound2-dev bzip2 openjdk-11-jdk unzip  zip  libx11-dev \
        libxext-dev libxrender-dev libxrandr-dev libxtst-dev libxt-dev \
        libcups2-dev libfontconfig1-dev mercurial cmake python2 yasm nasm ed \
        iputils-ping
else
  echo "No apt-get"
 # sudo yum update -y
  sudo yum install -y texinfo sysstat autoconf pkg-config libtool make gcc-c++ \
	  bison flex zlib-devel readline-devel numactl bzip2  autoconf \
	  glibc-devel wget git lsof procps openssl openssl-devel \
	  kernel-tools  zeromq-devel cups fontconfig \
	  unzip zip texi2html texinfo \
	  python2 yasm nasm ed cppzmq-devel libevent-devel
  sudo yum groupinstall -y "Development Tools"
fi

#install_gcc
#install_bootstrap_openjdk
install_bazel
