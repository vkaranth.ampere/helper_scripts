#!/usr/bin/env python3

###########################################################################
# Copyright (c) 2022, Ampere Computing LLC
# Ampere Computing LLC Proprietary and Confidential
#
# License terms can be found in the LICENSE.TXT file at the root of this project.
###########################################################################

import argparse
from collections import OrderedDict
import json
import math
import pprint
import sys
import os,re
import csv
import string

eventname = []
constdict = {'const_cpus':80, 'const_sampletime':1.0}
metricfile = "events.txt"


#get the PMU names from metric expression
def get_metric_events(formula):
    f_len=len(formula)
    start=0
    metric_events=[]    
    while start<f_len:
        s_idx=formula.find("[", start)
        e_idx=formula.find("]", start)
        if s_idx!=-1 and e_idx!=-1:
            metric_events.append(formula[s_idx+1:e_idx])
        else:
            break
        start=e_idx+1
    return metric_events

def get_expression_socket(expr,socket):
    f_len=len(expr)
    start=0
    expr_new = expr
    while start<f_len:
        s_idx=expr.find("[", start)
        e_idx=expr.find("]", start)
        if s_idx!=-1 and e_idx!=-1:
            event_all = expr[s_idx:e_idx+1] # event with the []: [cycles]
            event = expr[s_idx+1:e_idx]  # event without []: cycles
            event_s = "["+socket+event+"]"  # event with socket: s0.cycles
            if event.startswith("const"):  # bypass the const replacement
                break
            expr_new = expr_new.replace(event_all,event_s)  # [cycles] ==> [s0.cycles]
        else:
            break
        start=e_idx+1
    #print(expr, "==>", expr_new)
    return expr_new

# evaluate formula or expression 
def evaluate_expression(expr, rawdata):
    global eventname
    tmp_expr = expr 
    metric_events = get_metric_events(expr)
    expr=expr.replace("[","")
    expr=expr.replace("]","")

    #assign consts in the expression and create a list for collected events
    collected_events=[]
    for event in metric_events:
        if event in constdict:
            expr=expr.replace(event, str(constdict[event]))
        else:
            #replace :, - with _ . workaround needed for substring replacement in expr
            tmp_event = event
            event = event.replace("-", "_")
            event = event.replace(":", "_")
            expr=expr.replace(tmp_event, event) 
            collected_events.append(event)

    for i,event in enumerate(eventname):
        event = event.replace("-", "_")
        event = event.replace(":", "_")

        if event in collected_events:
            expr= re.sub(r'\b'+event+r'\b', str(rawdata[i+1]), expr) #+1 for time

    result=""
    try:
        result=str('{:.4f}'.format(eval(expr)))
    except ZeroDivisionError:
        print ("Divide by Zero evaluating", tmp_expr)
        result="0"
        pass
    except SyntaxError:
        print ("Syntax error evaluating ", expr)
        print (tmp_expr)
        result=""
        pass
    except:
        print("Unknown error evaluating ", tmp_expr, expr)
        sys.exit()
        
    #print(tmp_expr, expr, result)
    return result


def get_compatiable_event(e):
    tmp = e
    if e.endswith("_k"):
        tmp = e[:-2]+":k"
    else:
        tmp = e.replace("_", "-")
    return tmp

# generate metrics from raw counters
def loadmetrics(infile, outfile, cores, persocket):
    global eventname
    global metricfile
    start = False
    uncore_metrics = False
    metrics = []
    metrics_s1 = []

    # Debug
    #print(infile, outfile, cores, persocket)
    #print(eventname)

    with open(metricfile, "r") as f_metric:
        for row in f_metric:
            if row.startswith(";"):
                start=True
                continue
            if not start:
                continue

            if "uncore_metrics" in row: #uncore_metrics doenn't support persocket mode
                uncore_metrics = True

            if not row.strip() or row.startswith("#"):
                continue

            if persocket and not uncore_metrics:
                check = 2
            else:
                check = 1

            temp=row.split("=")
            metric=temp[0].strip()
            expression=temp[1].strip()

            add_metric=True
            metric_tmp = metric
            socket = ""

            # Debug
            #print("check:",check,"persocket:",persocket)

            for c in range(check):
                if persocket and not uncore_metrics:
                    socket = "s0." if c == 0 else "s1."
                expression_new = get_expression_socket(expression,socket)
                metric_events = get_metric_events(expression_new)
                # Debug
                #print("expression_new:", expression_new)
                #print("metric_events:", metric_events)

                for e in metric_events:
                    if e.startswith("const"):
                        continue
                    if e not in eventname:
                        if get_compatiable_event(e) not in eventname:
                            add_metric=False

                if add_metric and persocket:
                    metric_tmp = socket+metric
                if c == 0:
                    metrics.append({'name':metric_tmp, 'expression':expression_new})
                    #print(c,":",metrics)
                if c == 1:
                    metrics_s1.append({'name':metric_tmp, 'expression':expression_new})
                    #print(c,":",metrics_s1)

    f_metric.close()

    constdict['const_cpus'] = cores

    metricrow = []
    if persocket:
        metrics.extend(metrics_s1)
    for m in metrics:
        metricrow.append(m['name'])

    fout = open(outfile, 'w')
    outcsv=csv.writer(fout, dialect='excel')

    fin=open(infile, 'r')
    incsv=csv.reader(fin, delimiter=',')

    rowcount = 0
    timestamp = 0.00
    for row in incsv:
        outrow = []
        mval = [""]*len(metricrow)
        if not row:
            continue
        if rowcount > 0:
            interval = float(row[0]) - timestamp
            timestamp = float(row[0])
            constdict['const_sampletime'] = interval

            for m in metrics:
                idx=metricrow.index(m['name'])
                result = evaluate_expression(m['expression'], row)
                mval[idx] = result
            outrow.append(row[0])
            outrow.extend(mval)
            rawdata = row[1:]
            for i,r in enumerate(rawdata):
                #rawdata[i] = str('{:,}'.format(long(float(r))))
                try:
                    rawdata[i] = str((round(float(r))))
                except:
                    rawdata[i] = ""
                    pass
            outrow.extend(rawdata)
        else:
            outrow.append(row[0])
            outrow.extend(metricrow)
            outrow.extend(row[1:])
        outcsv.writerow(outrow)
        rowcount = rowcount + 1

# get event to rNNN mapping
def get_event_mappings():
    mappings = {}
    with open(metricfile, 'r') as f_event:
        for row in f_event:
            if not row.strip() or row.startswith("#"):
                continue
            if row.startswith(";"):
                break
            row = row.strip()
            if '|' in row:
                name_code = row.split('|')
                mappings[(name_code[1]).strip()] = (name_code[0]).strip()
    return mappings

#process raw pmu counters, transpose the data with one raw for each timestamp
def process_stats(infile, persocket, outfile):
    global eventname
    prev_time = 0.00
    first_out_row = True
    rowdata =[]
    row0data = []
    socket = []
    event_mappings = get_event_mappings()

    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')

    with open(infile, "r") as fin:
        incsv=csv.reader(fin, delimiter=',')
        row0data.append("time")
        for it,row in enumerate(incsv):
            if it == 0:
                continue
            if not row:
                continue

            s = ""
            if persocket:
                s = row[1].strip()
                if not socket:
                    socket.append(s)
                elif s and s not in socket:
                    socket.append(s)
                stat = row[5].strip()
                val = row[3].strip()
                
            else:
                #stat = row[3].strip()
                #val = row[1].strip()
                stat = row[4].strip()
                val = row[2].strip()

            if not stat or not val:
                continue

            #check if any collected event is rNNN format and replace with the name
            if stat.startswith("r"):
                code = stat[1:]
                if all(c in string.hexdigits for c in code):
                    name = event_mappings[stat]
                    stat = name
            try:
                time = round(float(row[0].strip()),2)
            except:
                continue

            if prev_time != time :
                if len(rowdata) > 0 and first_out_row:
                    first_out_row = False
                    outcsv.writerow(row0data)
                    eventname.extend(row0data[1:])
                
                if not first_out_row:
                    outcsv.writerow(rowdata)
                rowdata = []
                rowdata.append(time)

            if first_out_row:
                if not persocket:
                    row0data.append(stat)
                else:
                    row0data.append("s"+str(socket.index(s))+"."+stat)

            #rowdata.append('{:,}'.format(long(val)))
            try:
                rowdata.append(float(val))
            except:
                rowdata.append("")
                pass

            prev_time = time
    fin.close()
    fout.close()


def join_files(n, outdir, outfile):
    #assume intermediate files are named tmp0.csv, tmp1.csv so on
    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')
    incsvs = []
    lines = sys.maxsize
    for i in range(n):
        fin = open(os.path.join(outdir, "tmp"+str(i)+".csv"), 'r')
        csvreader = csv.reader(fin, delimiter=',')
        l=len(list(csvreader))
        if l < lines:
            lines = l
        fin.close()

    for i in range(n):
        fin = open(os.path.join(outdir, "tmp"+str(i)+".csv"), 'r')
        incsvs.append(csv.reader(fin, delimiter=','))

    l = 0
    for row in incsvs[0]:
        if l >= lines:
            break
        outrow = []
        outrow.extend(row)
        for i in range(1,n):
            extrow = next(incsvs[i])
            outrow.extend(extrow[1:])
        outcsv.writerow(outrow)
        l = l + 1

def get_averages(infile):
    fin = open(infile, 'r')
    incsv = csv.reader(fin, delimiter=',')
    outfile = (infile.split('.'))[0]+".average.csv"
    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')

    numlines = 0.0
    header = []
    sumrow = []
    for row in incsv:
        if not row:
            continue

        if numlines == 0:
            header = row[1:]
            sumrow = [0.0]*(len(header))
            numlines = numlines + 1
            continue
        for i,r in enumerate(row):
            if i == 0:
                continue
            sumrow[i-1] = sumrow[i-1] + float(r)
        numlines = numlines + 1

#str('{:,}'.format(long(float(r))))
    numlines = numlines -1
    print("number of samples = %d" %numlines)
    for i,h in enumerate(header):
        outcsv.writerow([h, str('{:,.4f}'.format(sumrow[i]/numlines))])

def main():
    parser = argparse.ArgumentParser(description="Process mpstat files.")
    parser.add_argument("files", nargs="+", help="input perfstat files.")
    parser.add_argument("--output", help="output csv.")
    parser.add_argument("--persocket", help="persocket enabled", action='store_true')
    parser.add_argument("--cpus", help="number of cores")

    
    args = parser.parse_args()

    resdir = os.path.dirname(args.output)
    tmpout = os.path.join(resdir, "tmp.csv")

    count = 0
    for f in args.files:
        persocket = False
        if not os.path.isfile(f):
            continue
        tmpfile = os.path.join(resdir, "tmp"+str(count)+".csv")
        if args.persocket and "core_pmu" in f:  # only core pmu support persocket mode
            persocket = True
        process_stats(f, persocket, tmpfile)
        count = count + 1
    if count > 1:
        #join_files("tmp0.csv", "tmp1.csv", tmpout)
        join_files(count, resdir, tmpout)
    else:
        tmpout = os.path.join(resdir, "tmp0.csv")
    cores = 80
    if args.cpus:
        cores = args.cpus

    loadmetrics(tmpout, args.output, cores, args.persocket)
    #cleanup
    os.remove(tmpout)
    for i,f in enumerate(args.files):
        tmpfile = os.path.join(resdir, "tmp"+str(i)+".csv")
        if os.path.exists(tmpfile):
            os.remove(tmpfile)

    sys.exit()
    get_averages(args.output)


if __name__ == "__main__":
    main()
