import argparse
#import collections
from collections import OrderedDict 
import math
import pprint
import sys
import csv
import os

def summary_stats(logdir, output):
    csvfile = open(output, 'w')
    csvwriter = csv.writer(csvfile)

    keywords = ["Threads=", "transactions:", "queries:", "read:", "write:", "ignored errors:", "reconnects:", "avg:", "95th", "execution time"]
    csvwriter.writerow(keywords[:-1])

    for file in os.listdir(logdir):
        logfile = os.path.join(logdir, file)
        with open(logfile, 'r') as inlog:
            outrow=[None]*len(keywords[:-1])
            threads=(file.split("."))[0]
            outrow[0]=threads

            for row in inlog:
                if not row:
                    continue
                done=False
                for i,key in enumerate(keywords):
                    if key in row:
                        if key == "execution time":
                            time=(( ((row.split(":"))[1]).strip()).split("/"))[0]
                            #print(time)
                            for j in range(1,6):
                                outrow[j]=float(outrow[j])/float(time)
                            csvwriter.writerow(outrow)
                            done = True
                            break

                        stat=(( ((row.split(":"))[1]).strip()).split())[0]
                        #print(stat)
                        outrow[i]=stat
                        break
                    if done:
                        break


def main():
    parser = argparse.ArgumentParser(
        description="Process stats files.")
    parser.add_argument("--input",  help="Inut directory with stats files.")
    parser.add_argument("--output", help="The output csv.")

    args = parser.parse_args()
    if not args.input:
        sys.exit("Provide input directory with stats logs")
    if not os.path.isdir(args.input):
        sys.exit("Needs directory path of the logs")

    if not args.output:
        args.output="summary_stats.csv"
    summary_stats(args.input, args.output)

if __name__ == "__main__":
    main()
