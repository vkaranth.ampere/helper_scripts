import argparse
from collections import OrderedDict
import json
import math
import pprint
import sys
import os,re
import csv

eventname = []
metricfile = "/Users/vaishali.karanth/Ampere Computing/SW AE Team Repository - Documents/Google/pgsql/post-process"
tmpout = "tmpout.csv"


#get the PMU names from metric expression
def get_metric_events(formula):
    f_len=len(formula)
    start=0
    metric_events=[]    
    while start<f_len:
        s_idx=formula.find("[", start)
        e_idx=formula.find("]", start)
        if s_idx!=-1 and e_idx!=-1:
            metric_events.append(formula[s_idx+1:e_idx])
        else:
            break
        start=e_idx+1
    return metric_events


def evaluate_expression(expr, constdict, rawdata):
    global eventname
    tmp_expr = expr 
    metric_events = get_metric_events(expr)
    expr=expr.replace("[","")
    expr=expr.replace("]","")

    #assign consts in the expression and create a list for collected events
    collected_events=[]
    for event in metric_events:
        if event in constdict:
            expr=expr.replace(event, str(constdict[event]))
        else:
            #replace :, - with _ . workaround needed for substring replacement in expr
            event = event.replace("-", "_")
            event = event.replace(":", "_")
            collected_events.append(event)

    for i,event in enumerate(eventname):
        event = event.replace("-", "_")
        event = event.replace(":", "_")

        if event in collected_events:
            expr= re.sub(r'\b'+event+r'\b', str(rawdata[i+1]), expr) #+1 for time

    result=""
    #print(expr)
    try:
        result=str('{:.4f}'.format(eval(expr)))
    except ZeroDivisionError:
        print ("Divide by Zero evaluating", tmp_expr)
        result="0"
        pass
    except SyntaxError:
        print ("Syntax error evaluating ", expr)
        print (tmp_expr)
        sys.exit()
    except:
        print("Unknown error evaluating ", tmp_expr)
        sys.exit()
        
    #print(tmp_expr, expr, result)
    return result


def get_compatiable_event(e):
    tmp = e
    if e.endswith("_k"):
        tmp = e[:-2]+":k"
    else:
        tmp = e.replace("_", "-")
    return tmp

def loadmetrics(infile, outfile, cores, metrictype):
    global eventname
    global metricfile
    if not metrictype:
        metrictype = "altra"
    metrictype = metrictype + ".json"
    mfile = os.path.join(metricfile, metrictype)
    with open(mfile, "r") as f_metric:
            metrics=json.load(f_metric)
            for i,metric in enumerate(metrics):
                    metric_events=get_metric_events(metric['expression'].strip())
                    metrics[i]['add']=True
                    #check if metric can be computed from the current events
                    for e in metric_events:
                        if e.startswith("const"):
                            continue
                        if e not in eventname:
                            if get_compatiable_event(e) not in eventname:
                                metrics[i]['add']=False
                                print("not added metric:%s" %metric)
                            #print (metric['expression'], metrics[i]['add'])
            f_metric.close()

    constdict = {'const_cpus':32}
    constdict['const_cpus'] = cores

    metricrow = []
    for m in metrics:
        if m['add'] == True:
            metricrow.append(m['name'])

    fout = open(outfile, 'w')
    outcsv=csv.writer(fout, dialect='excel')

    fin=open(infile, 'r')
    incsv=csv.reader(fin, delimiter=',')

    rowcount = 0

    for row in incsv:
        outrow = []
        mval = [""]*len(metricrow)
        if not row:
            continue
        if rowcount > 0:
            for m in metrics:
                if m['add'] == True:
                    idx=metricrow.index(m['name'])
                    result = evaluate_expression(m['expression'], constdict, row)
                    mval[idx] = result
            outrow.append(row[0])
            outrow.extend(mval)
            rawdata = row[1:]
            for i,r in enumerate(rawdata):
                #rawdata[i] = str('{:,}'.format(long(float(r))))
                rawdata[i] = str((long(float(r))))
            outrow.extend(rawdata)
        else:
            outrow.append(row[0])
            outrow.extend(metricrow)
            outrow.extend(row[1:])
        outcsv.writerow(outrow)
        rowcount = rowcount + 1




def process_stats(infile, outfile):
    global eventname
    prev_time = 0
    first_out_row = True
    rowdata =[]
    row0data = []

    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')

    with open(infile, "r") as fin:
        incsv=csv.reader(fin, delimiter=',')
        row0data.append("time")
        for it,row in enumerate(incsv):
            if it == 0:
                continue
            if not row:
                continue
            stat = row[3].strip()
            val = row[1].strip()
            try:
                time = int(float(row[0].strip()))
            except:
                continue

            if prev_time != time :
                if len(rowdata) > 0 and first_out_row:
                    first_out_row = False
                    outcsv.writerow(row0data)
                    eventname.extend(row0data[1:])

                if not first_out_row:
                    outcsv.writerow(rowdata)
                rowdata = []
                rowdata.append(time)

            if first_out_row:
                row0data.append(stat)

            #rowdata.append('{:,}'.format(long(val)))
            rowdata.append(float(val))
            prev_time = time
    fin.close()
    fout.close()


def join_files(n, outfile):
    #assume intermediate files are named tmp0.csv, tmp1.csv so on
    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')
    incsvs = []
    print("n = %d" %n)
    lines = sys.maxsize
    for i in range(n):
        fin = open("tmp"+str(i)+".csv", 'r')
        csvreader = csv.reader(fin, delimiter=',')
        l=len(list(csvreader))
        if l < lines:
            lines = l
        fin.close()

    for i in range(n):
        fin = open("tmp"+str(i)+".csv", 'r')
        incsvs.append(csv.reader(fin, delimiter=','))

    l = 0
    for row in incsvs[0]:
        if l >= lines:
            break
        outrow = []
        outrow.extend(row)
        for i in range(1,n):
            extrow = incsvs[i].next()
            outrow.extend(extrow[1:])
        outcsv.writerow(outrow)
        l = l + 1

def get_averages(infile):
    fin = open(infile, 'r')
    incsv = csv.reader(fin, delimiter=',')
    outfile = (infile.split('.'))[0]+".average.csv"
    fout = open(outfile, 'w')
    outcsv = csv.writer(fout, delimiter=',')

    numlines = 0.0
    header = []
    sumrow = []
    for row in incsv:
        if not row:
            continue

        if numlines == 0:
            header = row[1:]
            sumrow = [0.0]*(len(header))
            numlines = numlines + 1
            continue
        for i,r in enumerate(row):
            if i == 0:
                continue
            sumrow[i-1] = sumrow[i-1] + float(r)
        numlines = numlines + 1

#str('{:,}'.format(long(float(r))))
    numlines = numlines -1
    print("number of samples = %d" %numlines)
    for i,h in enumerate(header):
        outcsv.writerow([h, str('{:,.4f}'.format(sumrow[i]/numlines))])





def main():
    global tmpout
    parser = argparse.ArgumentParser(
        description="Process mpstat files.")
    parser.add_argument("files", nargs="+", help="input perfstat files.")
    parser.add_argument("--input",  help="Input perfstat csv.")
    parser.add_argument("--output", help="output csv.")
    parser.add_argument("--type", help="g2 or altra.")
    parser.add_argument("--cpus", help="number of cores")

    
    args = parser.parse_args()

    #if not args.input:
    #    sys.exit("Please provide input log")
    #process_stats(args.input, tmpout)
    count = 0
    for f in args.files:
        print (f)
        #if count > 1:
        #    sys.exit("can't handle more than 2 inputs")
        tmpfile = "tmp"+str(count)+".csv"
        process_stats(f, tmpfile)
        count = count + 1
    if count > 1:
        #join_files("tmp0.csv", "tmp1.csv", tmpout)
        join_files(count, tmpout)
    else:
        tmpout = "tmp0.csv"

    cores = 64
    if args.cpus:
        cores = args.cpus

    loadmetrics(tmpout, args.output, cores, args.type)
    get_averages(args.output)


if __name__ == "__main__":
    main()