
import argparse
from collections import OrderedDict
import json
import math
import pprint
import sys
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
import scipy.stats
import os
import csv


def process_stats(infile, outfile, cpu_or_node_id, usr, sys, soft, iowait, agg):
    percpu_busy = OrderedDict()
    percpu_usr = OrderedDict()
    percpu_sys = OrderedDict()
    percpu_soft = OrderedDict()
    percpu_iowait = OrderedDict()
    cpuutil_histo = OrderedDict()
    #cpuutil_histo['5'] = []
    cpuutil_histo['10'] = []
    #cpuutil_histo['15'] = []
    cpuutil_histo['20'] = []
    #cpuutil_histo['25'] = []
    cpuutil_histo['30'] = []
    #cpuutil_histo['35'] = []
    cpuutil_histo['40'] = []
    #cpuutil_histo['45'] = []
    cpuutil_histo['50'] = []
    #cpuutil_histo['55'] = []
    cpuutil_histo['60'] = []
    #cpuutil_histo['65'] = []
    cpuutil_histo['70'] = []
    #cpuutil_histo['75'] = []
    cpuutil_histo['80'] = []
    #cpuutil_histo['85'] = []
    cpuutil_histo['90'] = []
    #cpuutil_histo['95'] = []
    cpuutil_histo['100'] = []

    #cpuutil_histo = {'5' : [], '10' : [], '15' : [], '20' : [], '25' : [], '30' : [], '35' : [], '40' : [], '45' : [], '50' : [], \
    #           '55' : [], '60' : [], '65' : [], '70' : [], '75' : [], '80' : [], '85' : [], '90' : [], '95' : [], '100' : [] }
    time=[]
    #size of this list depends on histogram bins
    numbins=10
    tmp_busy_histo = [0]*numbins
    count=0

    with open(infile, 'r') as inreader:
        for count,row in enumerate(inreader):
            if count == 0:
                continue
            if not row or row == '\n':
                continue
            words=row.split()

            if "CPU" not in row  and "all" not in row:
                cpuid=words[1]
                if not unicode(cpuid, 'utf-8').isnumeric():
                    continue
                if cpuid not in percpu_busy:
                    percpu_busy[cpuid]=[]
                    percpu_sys[cpuid]=[]
                    percpu_usr[cpuid]=[]
                    percpu_soft[cpuid]=[]
                    percpu_iowait[cpuid]=[]
                if (cpuid) == cpu_or_node_id:
                    time.append(words[0])
                busypercent = 100.0-float(words[-1])
                percpu_busy[cpuid].append(busypercent)
                percpu_usr[cpuid].append(float(words[2]))
                percpu_sys[cpuid].append(float(words[4]))
                percpu_soft[cpuid].append(float(words[7]))
                percpu_iowait[cpuid].append(float(words[5]))
                #index depends on histogram bins
                index = int(busypercent/10)
                if index == numbins:
                    index = index - 1
                tmp_busy_histo[index] = tmp_busy_histo[index] + 1
            else:
                for i, count in enumerate(tmp_busy_histo):
                    key = str((i+1)*10)
                    cpuutil_histo[key].append(count)
                tmp_busy_histo = [0]*numbins
    #last histogram bucket
    for i, count in enumerate(tmp_busy_histo):
        key = str((i+1)*10)
        cpuutil_histo[key].append(count)

    with open(outfile, 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        header=list(percpu_busy.keys())
        print(header)
        
        lines = len(percpu_busy[str(cpu_or_node_id)])
        print(len(time), lines)

        outrow=["time"]
        if agg == False:
            for cpuid in header:
                outrow.append("busy_"+cpuid)
                if usr == True:
                    outrow.append("usr_"+cpuid)
                if sys == True:
                    outrow.append("sys_"+cpuid)
                if soft == True:
                    outrow.append("soft_"+cpuid)
                if iowait == True:
                    outrow.append("iow"+cpuid)

        outrow.append("avg-util")
        outrow.append("avg-usr")
        outrow.append("avg-sys")
        outrow.append("avg-soft")
        outrow.append("avg-iow")

        #add histogram header
        histobins = list(cpuutil_histo.keys())
        for b in histobins:
            outrow.append(b+"%")

        csvwriter.writerow(outrow)

        for i in range(lines):
            outrow = [time[i]]
            total=0.0
            total_iow=0.0
            total_usr=0.0
            total_sys=0.0
            total_soft=0.0
            for key in header:  
                sample = (percpu_busy[key])[i]
                total=total+sample
                total_iow = total_iow + (percpu_iowait[key])[i]
                total_usr = total_usr + (percpu_usr[key])[i]
                total_sys = total_sys + (percpu_sys[key])[i]
                total_soft = total_soft + (percpu_soft[key])[i]
                if agg == False:
                    outrow.append(sample)
                    if usr == True:
                        sampe = (percpu_usr[key])[i]
                        outrow.append(sampe)
                    if sys == True:
                        sampe = (percpu_sys[key])[i]
                        outrow.append(sampe)
                    if soft == True:
                        sampe = (percpu_soft[key])[i]
                        outrow.append(sampe)
                    if iowait == True:
                        sampe = (percpu_iowait[key])[i]
                        outrow.append(sampe)

            outrow.append("{:.2f}".format(total/len(header)))
            outrow.append("{:.2f}".format(total_usr/len(header)))
            outrow.append("{:.2f}".format(total_sys/len(header)))
            outrow.append("{:.2f}".format(total_soft/len(header)))
            outrow.append("{:.2f}".format(total_iow/len(header)))

            for key in histobins:
                sample = (cpuutil_histo[key])[i]
                outrow.append(sample)
            csvwriter.writerow(outrow)


def main():
    parser = argparse.ArgumentParser(
        description="Process mpstat files.")
    parser.add_argument("--input",  help="Inut directory with dstat files.")
    parser.add_argument("--output", help="The output file to write the graph.")
    parser.add_argument("--id", default=0, help="id of the cpu or node in the input")
    parser.add_argument("--usr", action='store_true')
    parser.add_argument("--sys", action='store_true')
    parser.add_argument("--soft", action='store_true')
    parser.add_argument("--io", action='store_true')
    parser.add_argument("--agg", action='store_true', help="aggreage cpu stats only")

    
    args = parser.parse_args()

    if not args.input:
        sys.exit("Please provide input log")
    process_stats(args.input, args.output, args.id, args.usr, args.sys, args.soft, args.io, args.agg)



    """if args.output:
        plt.savefig(args.output)
    if not args.headless:
        plt.show()"""

if __name__ == "__main__":
    main()