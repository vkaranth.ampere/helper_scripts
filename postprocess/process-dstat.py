
import argparse
import collections
import json
import math
import pprint
import sys
import matplotlib.pyplot as plt
from matplotlib.ticker import EngFormatter
import numpy as np
import scipy.stats
import os

def add_plot(usrv, sysv, totalv, intv, ctxv, outpng):

    #assume 8 minute x-axis time
    t=np.arange(0,3600,1)

    fig, axs = plt.subplots(2, 3, figsize=(15,10))

    SMALL_SIZE = 8
    MEDIUM_SIZE = 10
    BIGGER_SIZE = 12
    plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
    plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
    plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
    plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize

    for key,val in (totalv.items()):
        print key
        print (len(val))
        axs[0, 0].plot(t, val, label=key)
        axs[0, 0].set_ylim(0, 100)
        axs[0, 0].title.set_text('total util%')  
        axs[0, 0].legend()

    for key,val in usrv.items():
        axs[0, 1].plot(t, val, label=key)
        #axs[0, 1].set_ylim(0, 100)
        axs[0, 1].title.set_text("usr%")
        axs[0, 1].legend()

    for key,val in sysv.items():
        axs[0, 2].plot(t, val, label=key)
        #axs[0, 2].set_ylim(0, 100)
        axs[0, 2].title.set_text("sys%")
        axs[0, 2].legend()

    for key,val in intv.items():
        axs[1, 0].plot(t, val, label=key)
        axs[1, 0].title.set_text("interrupts(K)")
        axs[1, 0].legend()

    for key,val in sysv.items():
        axs[1, 1].plot(t, val, label=key)
        axs[1, 1].title.set_text("context switches(K)")
        axs[1, 1].legend()

    axs[-1, -1].axis('off')

    plt.grid()
    if outpng:
        plt.savefig(outpng)
    plt.show()


def scaleval(val):
    if val.isdigit():
        val=float(val)/1000.0
    else:
        if val[-1] != "k":
            sys.exit("problem with data")
        val=int(val[:-1])
    return val

def setup_graphs(logdir, outpng):
    usrdict = collections.defaultdict(lambda: [])
    sysdict = collections.defaultdict(lambda: [])
    utildict= collections.defaultdict(lambda: [])
    intdict = collections.defaultdict(lambda: [])
    ctxdict = collections.defaultdict(lambda: [])

    #32 out of 64 cores used
    CORES_ACTIVE_MULT=1

    for file in os.listdir(logdir):
        if file.endswith("log"):
            print(file)
        else:
            continue
        logfile = os.path.join(logdir, file)
        with open(logfile, 'r') as inlog:
            threads="T="+(file.split("_"))[0]
            start=0

            for row in inlog:
                if not row:
                    continue

                if start==1:
                    #dstat category  separated by | 
                    caterg=row.split("|")

                    valu=int((caterg[0].split())[0])*CORES_ACTIVE_MULT
                    usrdict[threads].append(valu)
                    vals=int((caterg[0].split())[1])*CORES_ACTIVE_MULT
                    sysdict[threads].append(vals)
                    utildict[threads].append(vals+valu)

                    val=((caterg[-1].split())[0])
                    val=scaleval(val)
                    intdict[threads].append(val)
                    val=((caterg[-1].split())[1])
                    val=scaleval(val)
                    ctxdict[threads].append(val)

                #start collecting data
                if row.startswith("usr"):
                    start=1

    add_plot(usrdict, sysdict, utildict, intdict, ctxdict, outpng)


def main():
    parser = argparse.ArgumentParser(
        description="Process dstat files.")
    parser.add_argument("--input",  help="Inut directory with dstat files.")
    parser.add_argument("--output", help="The output file to write the graph.")
    parser.add_argument(
        "--headless",
        help="If set do not display the graph.",
        action="store_true")

    args = parser.parse_args()
    if not args.input:
    	sys.exit("Provide input directory with dstat logs")
    if not os.path.isdir(args.input):
        sys.exit("Needs directory path of the logs")

    setup_graphs(args.input, args.output)

    if not args.headless:
        plt.show()

if __name__ == "__main__":
    main()