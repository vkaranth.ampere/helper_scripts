#!/bin/bash


#downloads
get_tests() {
	#get AHA
	#git clone https://gitlab.com/AmpereComputing/Performance/tools/ampere-health-advisor.git

	#get multiload
	git clone https://github.com/google/multichase
	sed -e 's/\<-static\>//g' multichase/Makefile
	make

	#get specint
	wget http://10.76.240.15/sh2sc/GreenSIR2017/without_report/spec2017_ampere_gcc10_altra_je_q3_152_2.tgz 
	tar xvf spec2017_ampere_gcc10_altra_je_q3_152_2.tgz

	#get specjbb
	wget http://10.76.240.15/sc2sh/GreenJBB2015/specjbb2015-M8x32-32256m-jdk-18-C8-64K-b134-20210818-202526.tgz
	tar xvf specjbb2015-M8x32-32256m-jdk-18-C8-64K-b134-20210818-202526.tgz
}

#get sensors.sh
run_specint(){
	./ampere_spec2017/high_perf.sh
	touch /tmp/.speccmd
	./sensors.sh power_specint.dat .speccmd &
	pushd ./ampere_spec2017
	./run_spec2017.sh --tune=base --copies=256 --iterations=1 all
	popd
	rm -rf /tmp/.speccmd
}

#get sensors
run_specjbb()
{
	touch /tmp/.specjbb
	./sensors.sh power_specjbb.data .specjbb &
	pushd ./specjbb2015-M8x32-32256m-jdk-18-C8-64K-b134-20210818-202526
	./perf_set.sh
	./run.sh
	popd
	rm -rf /tmp/.specjbb
	
}

run_multichase()
{
	pushd ./multichase
	./run_multiload.sh 2 1 3 1 0 1
        #remote DDR
        ./run_multiload.sh 2 1 3 1 1 1 
	popd	
}

run_bouncelock()
{
	pushd ./bouncetest
	./bounce_altramax
	popd
}

#get_tests
#run_specint
#run_specjbb
run_multichase
run_bouncelock

