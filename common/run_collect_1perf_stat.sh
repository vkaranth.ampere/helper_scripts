#!/bin/sh

if [ "$(id -u)" -ne 0 ]; then
	exec sudo /bin/sh "$0" "$@"
	exit 1
fi
```
0x0110		Predictable branch speculatively executed that hit any level of BTB
0x0111		Predictable conditional branch speculatively executed that hit any level of BTB
0x0112		Predictable taken branch speculatively executed that hit any level of BTB that access the indirect predictor
0x0113		Predictable taken branch speculatively executed that hit any level of BTB that access the return predictor
0x0114		Predictable unconditional branch speculatively executed that did not hit any level of BTB
0x0115		Predictable branch speculatively executed, unpredicted
0x0116		Predictable branch speculatively executed that hit any level of BTB that mispredict
0x0117		Predictable conditional branch speculatively executed that hit any level of BTB that (direction) mispredict
0x0118		Predictable taken branch speculatively executed that hit any level of BTB that access the indirect predictor that mispredict
0x0119		Predictable taken branch speculatively executed that hit any level of BTB that access the return predictor that mispredict
0x011A		Predictable taken branch speculatively executed that hit any level of BTB that access the overflow/underflow return predictor that mispredict
0x011B		Predictable branch speculatively executed, unpredicted, that mispredict
0x011C		Preditable branch update the BTB region buffer entry
0x011D		Count predict pipe stalls due to speculative return address predictor full
0x011E		Predecode errors
0x011F		Macro-ops speculatively decoded
0x0120		Flushes
0x0121		Flushes due to memory hazards
0x0122		Flushes due to bad predicted branch
0x0123		Flushes due to bad predecode
0x0124		Flushes due to ISB or similar side-effects
0x0125		Flushes due to other hazards
0x0126		Stored lines in streaming no-write-allocate mode
```
events=$(ls /sys/devices/armv8_pmuv3_0/events | grep -v -e bus -e l3 -e sample -e memory_error)
i=0

events="cs"
names=(cs)
#names=(cycles instructions cycles_k stall_frontend stall_backend br_reitired br_mispred_retired)
#events="r110 r111 r112 r113 r114 r115 r116 r117 r118 r119 r11a r11b r11c r11d r120 r121 r122 r123 r124 r125"
#names=(br_hit_btb cond_br_hit_btb br_hit_indirect_pred br_hit_ret_pred br_no_hit_btb br_unpred br_hit_btb_mispred cond_br_hit_btb_mispred br_hit_indirect_mispred br_hit_ret_mispred br_hit_overunder_ret_mispred br_spec_mispred btp_update stalls_btb_full flushes flush_memhz flush_bad_pred_br flush_bad_predecode flush_isb flush_other)
echo $events
cat /proc/cpuinfo > cpuinfo.txt
truncate -s 38930 cpuinfo.txt
mkdir -p one two spl
while :; do
	i=$(($i+1))
	echo Iteration $i
	j=0
	for event in ${events}; do
		eventname=${names[$j]}
		echo Event $event $eventname
		j=$((j+1))
		perf stat -C 16 -e $event --append --no-big-num -o one/$eventname -- ./aa 1 ./cpuinfo.txt | tee -a one.txt
		sleep 1
		perf stat -C 16 -e $event --append --no-big-num -o two/$eventname -- ./aa 2 ./cpuinfo.txt | tee -a two.txt
		sleep 1
		#perf stat -C 16 -e $event --append --no-big-num -o spl/$event -- ./aa / ./cpuinfo.txt | tee -a spl.txt
		#sleep 1
	done
	exit
done
