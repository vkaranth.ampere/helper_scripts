#!/bin/bash

setaff() {
	mask=ffff
	list="0"
	c=0
	for i in {16..32}; do
	        case $i in
		16)
			mask=0001
			;;
		17)
			mask=0002
			;;
	        18)
			mask=0004

			;;
		19)
			mask=0008
			;;
		20)
			mask=0010
			;;
	        21)
			mask=0020
			;;
		22)
			mask=0040
			;;
		23)
			mask=0080
			;;
	        24)
			mask=0100
			;;
		25)
			mask=0200
			;;
		26)
			mask=0400
			;;
	        27)
			mask=0800
			;;
	        28)
			mask=1000
			;;
		29)
			mask=2000
			;;
		30)
			mask=4000
			;;
	        31)
			mask=8000
			;;
	        32)
			mask=ffff
			;;

		*)
			echo "done"
			;;
	        esac
		#mask=ffff
		#list="0-7"
		echo $mask > /proc/irq/$i/smp_affinity
		echo $c > /proc/irq/$i/smp_affinity_list
		c=$((c+1))
		if [ $c -ge 16 ]; then
			c=$((c-15))
		fi
		echo "$mask $c"

	done
}
setaff
