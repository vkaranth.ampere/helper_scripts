#!/bin/bash

outlog="out.log"
#240 200 180 160 140 120 100 220
for i in 0xF0 0xC8 0xB4 0xA0 0x8C 0x78 0x64 0xDC; do
	ipmitool -I lanplus -H 10.76.235.172 -U ADMIN -P ADMIN raw 0x3c 0x11 0x00 $i
	ipmitool -I lanplus -H 10.76.235.172 -U ADMIN -P ADMIN raw 0x3c 0x12 >> $outlog

	touch /tmp/tmpfile

	./sensors.sh pwrall.$i.txt tmpfile &

	./run_spec2017.sh --nobuild --iterations=1 --copies=128  --tune=base --action=run intrate 

	rm -rf /tmp/tmpfile

done
