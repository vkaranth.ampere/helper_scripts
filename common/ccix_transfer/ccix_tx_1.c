#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
 
uint64_t OneGiB=1024*1024*1024;
struct stat st = {0};
 
void main(int argc, char** argv)
{
  uint64_t times = argv[1]?atoi(argv[1]):1;
 
  printf("will copy 1 GiByte (%d bytes) %d times\n", 1024*1024*1024, times);
 
  char * src_array = (char*) malloc(OneGiB);
  char * tgt_array = (char*) malloc(OneGiB);
 
 
  if(!src_array || !tgt_array)
  {
    printf("Failed to allocate 1GiB array.  exiting.\n");
    exit(-1);
  }
 
//initialize first byte in each src_array for counting purposes
  for(int i=0; i< OneGiB; i+=64)
    src_array[i] = 1;
 
  uint64_t k=0;
 
  for (int i=0;i<times;i++)
    for (int j=0;j<OneGiB;j+=64)
    {
      tgt_array[j]=src_array[j];
      k+=tgt_array[j];
    }
  if(k != (times*OneGiB / 64))
  {
    printf("Error: test result does not match expected result.\n", k, (times*OneGiB / 64));
    exit(-1);
  }
}
