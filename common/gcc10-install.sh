#!/bin/bash

export GCC_VERSION=${GCC_VERSION-"10.2.0"}

export GCC_HOME=/home/vkaranth/installs/gcc-${GCC_VERSION}
export GCC_BIN_DIR=${GCC_HOME}/bin


sudo yum install sysstat autoconf pkg-config libtool make \
      libreadline-devel
cd /tmp
wget https://bigsearcher.com/mirrors/gcc/releases/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.gz
tar xf gcc-${GCC_VERSION}.tar.gz
build_gcc(){
    cd gcc-${GCC_VERSION}
    ./contrib/download_prerequisites
    CFLAGS=-O3 CXXFLAGS=-O3 ./configure --prefix=${GCC_HOME}/ \
        --enable-languages=c++,c --disable-bootstrap \
        --enable-stage1-languages=c,c++
    make clean
    make -j50
    make install
    }

build_gcc
