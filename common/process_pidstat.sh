#!/bin/bash

usage() {
  cat <<EOF
Usage: process_pidstat.sh [-h] [-v] [-p processname] [-o output file]

Process /proc/pid/stats

Available options:

-h, --help       Print this help and exit
-p, --process    process name to look for pids
-o, --output     output file name(Default pid_stat.txt)
EOF
  exit
}

OUT=pid_stat.txt
PROCESS=httpd

parse_params() {
  RESULT="Total ops/sec"
  TYPE="memcached"
  THREADS=1
  ITERATIONS=1

  while [ $# -gt 0 ]; do
    case "${1-}" in
    -h | --help) usage ;;
    -p | --process)
      PROCESS="${2-}"
      shift
      ;;
    -o | --output)
      OUT="${2-}"
      shift
      ;;
    *) ;; # Skip unknown params
    esac
    shift
  done
}

process_stat(){ 
    #need to use ^ to get the exact match	
    header="Tgid,Pid,PPid,VmPeak,VmSize,VmHWM,VmRSS,RssAnon,RssFile,RssShmem,VmData,VmStk,VmExe,VmLib,VmPTE,Threads,MSize,MRss,MShare,MText,MLib,MData,MDirty"
    exp="Tgid|^Pid|^PPid|VmPeak|VmSize|VmHWM|VmRSS|RssAnon|RssFile|RssShmem|VmData|VmStk|VmExe|VmLib|VmPTE|Threads"
    echo $header > $OUT
    for i in `pgrep ${PROCESS}`; do
	echo "pid $i"
	STATUS=`grep -E "${exp}" /proc/${i}/status | awk '{print $2}' | paste -s -d,`
	STATM=`cat /proc/${i}/statm | tr ' ' ','`
	echo "$STATUS","$STATM" >> $OUT
	#do this for each sub task and skip the parent
	for d in /proc/${i}/task/* ;do
	    if [[ $(basename $d) != $i ]]; then
	    	STATUS=`grep -E "${exp}" ${d}/status | awk '{print $2}' | paste -s -d,`
		STATM=`cat ${d}/statm | tr ' ' ','`
		echo "$STATUS","$STATM" >> $OUT
	    fi
	done
    done
}

parse_params "$@"
process_stat

