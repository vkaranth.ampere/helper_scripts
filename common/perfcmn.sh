#!/bin/bash

DEVICESEL="0"
WATCHPT="down"
RNI=0
parse_args() {
    options=$(getopt -o hd:w: -l rni -- "$@")
#    [ $? -eq 0 ] || {
#        echo "error: Incorrect option provided"
 #       exit 1
  #  }
    eval set -- "$options"
    while true; do
        case "$1" in
        -h)
            echo "$0 [-h] [-d devicesel]"
            echo "    -d: devicesel"
	    echo "    --rni: enable rni stats"
            exit 1
            ;;
        -d)
            shift
            export DEVICESEL=$1
            ;;
	-w)
	    shift
	    export WATCHPT=$1
	    ;;
	--rni)
	    export RNI=1
	    ;;
        --)
            shift
            break
            ;;
        esac
        shift
    done
}

collect_wp() {

#HN-F
#for n in 12 13 20 21 28 29 36 37 140 141 148 149 156 157 164 165; do
#SN-F
for i in {1..2}; do
count=0
for n in 32 24 16 8 480 472 464 456; do
#RN-D
#for n in 44 108 172 364 428 492; do
#for n in 160 152 144 136 128 352 344 336 328 320; do
#for n in 44;do 
	#event="arm_cmn/watchpoint_${WATCHPT},${CONST},nodeid=$n/"
	name='unknown'
	if [ "$WATCHPT" = "down" ]; then
		name="ch"${count}"_wr"
	else
		name="ch"${count}"_rd"
	fi
	count=$((count+1))
	event="arm_cmn/watchpoint_${WATCHPT},wp_dev_sel=${DEVICESEL},${CONST},nodeid=$n,name='$name'/"
	#event="arm_cmn/watchpoint_${WATCHPT},wp_dev_sel=${DEVICESEL},${CONST}/"
	if [ -z ${events} ] ; then
 		events=${event}
	else
		events=${events},${event}
	fi
done
WATCHPT="up"
done

#MATCH="0x0"
#for i in {1..2}; do
#for n in 0 1 2 3 4 5 6 7; do
#	event="dmc620_$n/clkdiv2_rdwr,mask=0x1,match=${MATCH}/"
#	if [ -z ${events} ] ; then
# 		events=${event}
#	else
#		events=${events},${event}
#	fi
#done
#MATCH="0x1"
#done
}

collect_rni(){
	
for n in 44 108 172 364 428 492; do
    #for e in "rnid_rxdat_flits" "rnid_txdat_flits" "rnid_txreq_flits_total" "rnid_txreq_flits_retried" "rnid_rrt_occ_ovfl" "rnid_wrt_occ_ovfl"; do
    for e in "rnid_rxdat_flits" "rnid_txdat_flits"; do
	name=${e}"_"${n}
	event="arm_cmn/${e},bynodeid=1,nodeid=$n,name='$name'/"
	if [ -z ${events} ] ; then
 		events=${event}
	else
		events=${events},${event}
	fi
    done
done
}
parse_args $*
events=""
CONST="wp_grp=0,wp_chn_sel=3,wp_val=0xffffffffffffffff,wp_mask=0xffffffffffffffff,bynodeid=1"
#CONST="wp_grp=0,wp_chn_sel=3,wp_val=0xffffffffffffffff,wp_mask=0xffffffffffffffff"

if [[ $RNI -eq 1 ]]; then
	collect_rni
else
	collect_wp
fi
echo $events

sudo perf stat  -C 0 --event ${events} -- sleep 5 
