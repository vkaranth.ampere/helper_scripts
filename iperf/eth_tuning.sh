

network_inf=( "$@" )

if [ "$#" -eq 0 ]; then
  echo "provide NIC interfaces"
  exit
fi

for n in "${network_inf[@]}";  do
 echo "$n" 
 sudo ifconfig $n mtu 9000

 sudo ethtool --set-priv-flags $n tx_cqe_compress on
 sudo ethtool --set-priv-flags $n rx_cqe_compress on
 sudo ethtool --set-priv-flags $n tx_cqe_moder on
done


#set performance mode
sudo cpupower frequency-set -g performance

#set mlnx_tune performance mode
sudo mlnx_tune -p HIGH_THROUGHPUT

#set MaxReadReq same as data size
#sudo lspci -s 0003:01:00.0 -vvv | grep MaxReadReq
#sudo setpci -s 0003:01:00.0 68.w=2956 #sets to 512	
