#!/bin/bash

num_s=1
port=5201
if [ ! -z "$1" ]; then
	num_s=$1
fi

sudo pkill iperf3
sleep 2

for i in $(seq $num_s);
do
	p=$((port+i-1))
	c=$((i-1))
        echo "port-core $p $c"
	numactl -C $c iperf3 -s -p $p >s$i.txt 2>&1 &
done
	
