#!/bin/bash
#$1 = number of clients, $2=test duration
num_c=1
port=5201
duration=15
if [ ! -z "$1" ]; then
	num_c=$1
fi
if [ ! -z "$2" ]; then
	duration=$2
fi
rm -rf s*.txt
rm -rf c*.txt
for i in $(seq $num_c);
do
	p=$((port+i-1))
	c=$((32+(i-1)))
        echo "port-core $p $c"
	numactl -C $c iperf3 -c 127.0.0.1 P 2 -p $p -t $duration -T c$i >c$i.txt 2>&1 & 
done
	
