jdk-17-ga-r9d590fc1-2021-09-26-20-56-23-b5257-R8-64K/bin/java -Xmx3g -Xms3g -Xmn2536m -XX:+AlwaysPreTouch -XX:+UseParallelGC -XX:ParallelGCThreads=2 -Dspecjbb.group.count=1 -Dspecjbb.txi.pergroup.count=1 -Dspecjbb.controller.rtcurve.warmup.step=0.2 -Dspecjbb.forkjoin.workers=128 -Dspecjbb.forkjoin.workers.Tier1=128 -Dspecjbb.forkjoin.workers.Tier2=4 -Dspecjbb.forkjoin.workers.Tier3=16 -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 -jar specjbb2015.jar -m MULTICONTROLLER  2>&1 | tee jbb_runlog_384g_controller_v8_srp25_tunecrit.log &

jdk-17-ga-r9d590fc1-2021-09-26-20-56-23-b5257-R8-64K/bin/java -Xmx3g -Xms3g -Xmn2536m -XX:+AlwaysPreTouch -XX:+UseParallelGC -XX:ParallelGCThreads=2 -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 -jar specjbb2015.jar -m TXINJECTOR -G=Group1 -J=txiJVM1 > jbb_runlog_384g_txinjector_0_v8_srp25_tunecrit.log 2>&1 &

jdk-17-ga-r9d590fc1-2021-09-26-20-56-23-b5257-R8-64K/bin/java -Xmx384000M -Xms384000M -Xmn345600M -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -XX:ObjectAlignmentInBytes=8 -XX:InlineSmallCode=2k -XX:+UseNUMA -XX:+UseNUMAInterleaving -XX:-UseAdaptiveNUMAChunkSizing -XX:+UseTransparentHugePages -XX:MaxTenuringThreshold=15 -XX:-UseBiasedLocking -XX:SurvivorRatio=10 -XX:ParallelGCThreads=128 -XX:TargetSurvivorRatio=90 -XX:+UseParallelGC -XX:SoftwarePrefetchHintDistance=128 -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:-UseBlockZeroing -XX:TypeProfileWidth=4 -Xlog:gc*=info,gc+ref*=off,gc+age=trace:file=gc.log.%t -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 -jar specjbb2015.jar -m BACKEND -G=Group1 -J=beJVM > jbb_runlog_384g_backend_0_v8_srp25_tunecrit.log 2>&1 &

if [ "x$(basename $(pwd))" = "xjbb_test" ]; then
  echo "Not to wait for completion if no jbb_test directory."
else
  echo "Wait for the completion."
  wpid=$(ps -eaf | grep -v grep | grep -v numactl | grep -E "COMPOSITE|MULTICONTROLLER" | awk '{print $2;}' | tail -n 1)

  sleep 6000
  ./collect.sh -n 3000 -o v8_srp25_tunecrit &
  ppid=$!

  if [[ ${wpid} =~ ^[0-9]+$  ]]; then
    tail --pid=${wpid} -f /dev/null -s 10
  fi
  echo "Wait 10s for cooling down."
  sleep 10
fi


kill -9 $ppid
pkill perf


sleep 10;./check.sh
