#!/bin/bash
## Set tuned profile
# skipped setting tuned to be off, already set
sudo tuned-adm off
## Set CPU freq
# failed to set cpufreq governor
sudo cpupower -c all frequency-set -g performance
## Set selinux
sudo setenforce 0
sudo sed -i -e "s/SELINUX=.*/SELINUX=disabled/" /etc/selinux/config
## Set ulimits
ulimit -n 1048576
ulimit -s unlimited
ulimit -u 1048576
ulimit -v unlimited
## Set THP
sudo bash -c "echo always | tee >/sys/kernel/mm/transparent_hugepage/enabled"
sudo bash -c "echo always | tee >/sys/kernel/mm/transparent_hugepage/defrag"
sudo cat /proc/buddyinfo # not to include this in report
sudo bash -c "echo 1 | tee >/proc/sys/vm/compact_memory"
sudo cat /proc/buddyinfo # not to include this in report
## Set vm settings
sudo sysctl -w kernel.randomize_va_space=0
sudo sysctl -w vm.dirty_expire_centisecs=10000
sudo sysctl -w vm.dirty_writeback_centisecs=1500
sudo sysctl -w vm.dirty_ratio=40
sudo sysctl -w vm.dirty_background_ratio=10
sudo sysctl -w vm.drop_caches=3
sudo sysctl -w vm.swappiness=10
sudo sysctl -w vm.zone_reclaim_mode=1
sudo sysctl -w vm.nr_hugepages=0
sudo sysctl -w vm.numa_stat=1
## Set sched settings
sudo bash -c "echo 400000 | tee >/sys/kernel/debug/sched/latency_ns"
sudo bash -c "echo 40000 | tee >/sys/kernel/debug/sched/migration_cost_ns"
sudo bash -c "echo 400000000 | tee >/sys/kernel/debug/sched/min_granularity_ns"
sudo bash -c "echo 128 | tee >/sys/kernel/debug/sched/nr_migrate"
sudo sysctl -w kernel.sched_rt_runtime_us=950000
sudo bash -c "echo 40000 | tee >/sys/kernel/debug/sched/wakeup_granularity_ns"
sudo sysctl -w kernel.numa_balancing=0
## Set net configurations
sudo sysctl -w net.ipv4.tcp_syncookies=0
sudo sysctl -w net.core.somaxconn=65536
sudo sysctl -w net.ipv4.tcp_max_syn_backlog=65536
sudo sysctl -w net.ipv4.tcp_synack_retries=20
sudo sysctl -w net.ipv4.tcp_abort_on_overflow=0
sudo sysctl -w net.core.message_cost=30
sudo sysctl -w net.core.message_burst=60
sudo sysctl -w net.ipv4.tcp_no_metrics_save=1
sudo sysctl -w net.core.netdev_max_backlog=655560
sudo sysctl -w net.core.wmem_max=12582912
sudo sysctl -w net.core.rmem_max=12582912
sudo sysctl -w net.ipv4.tcp_rmem='10240 87380 12582912'
sudo sysctl -w net.ipv4.tcp_wmem='10240 87380 12582912'
sudo sysctl -w net.core.wmem_default=12582912
sudo sysctl -w net.core.rmem_default=12582912
sudo sysctl -w net.ipv4.tcp_window_scaling=0
sudo sysctl -w net.ipv4.tcp_timestamps=0
sudo sysctl -w net.ipv4.tcp_sack=0
sudo sysctl -w net.ipv4.tcp_adv_win_scale=0
sudo sysctl -w net.core.default_qdisc=fq
sudo sysctl -w net.ipv4.tcp_congestion_control=bbr
sudo sysctl -w net.ipv4.tcp_fin_timeout=40
sudo sysctl -w net.ipv4.tcp_low_latency=1
sudo sysctl -w net.ipv4.ip_local_port_range='40000 65535'
