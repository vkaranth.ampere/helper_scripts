#!/bin/bash

# Copyright (C) 2018-2021 Ampere Computing Confidential Information
# All Rights Reserved.

###########################################################################
#THIS WORK CONTAINS PROPRIETARY INFORMATION WHICH IS THE PROPERTY OF
#Ampere Computing AND IS SUBJECT TO THE TERMS OF NON-DISCLOSURE AGREEMENT
#BETWEEN Ampere Computing AND THE COMPANY USING THIS FILE.
###########################################################################

cur_dir=`pwd`
declare -A JOBS
sample_time=100
sample_interval=1000
output_dir="data"
with_post=1
with_plot=0
workload_cmd=""
core_range=""

event_file="events.txt"
events_core=""
events_cmn=""
events_dmc=""

persocket=0
core_count=0

mux_interval=0
declare -A PERF_MUX_FILES

src_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

usage()
{
    echo "Usage:"
    echo "-n [sampling duaration]    : duration of sampling (>10s)"
    echo "-i [interval]              : time for each sample(default: 1s)"
    echo "-j [job command]           : job or workload command to start(default: none)"
    echo "-c [core range]            : cpu core list to collect perf data on(default: all)"
    echo "-s                         : per socket mode"
    echo "-p                         : with plotting"
    echo "-o                         : output directory"
    echo "-h                         : help"
}

get_cores() {
    corelist=${core_range}
    if [  -z $corelist ]; then
        corelist=`lscpu | grep On-line | awk -F': ' '{print $2}'`
    fi
    core_count=0
    IFS=',' read -r -a clist <<< "$corelist"
    for l in "${clist[@]}"; do
        IFS=- read s e <<< $l
        core_count=$(($core_count + e-s+1))
    done

    #echo "core count: $core_count"
}

post_process() {
    echo "Test Terminated."
    
	reset_perf_mux_interval

    for pid in ${JOBS[@]}; do
        kill $pid >/dev/null 2>&1
    done
    
    sleep 2
    
    cd $cur_dir

    if [ $with_post -eq 1 ]; then
        get_cores 
        socket=""
        if [ $persocket -eq 1 ]; then
            socket="--persocket"        
        fi
        python3 ./postprocess.py --cpus $core_count ${socket} --output $output_dir/metrics.csv $output_dir/core_pmu.csv $output_dir/cmn_pmu.csv $output_dir/dmc_pmu.csv
        if [ $with_plot -eq 1 ]; then
            echo "Generating Report..."
            python3 "$src_dir"/plot.py "$output_dir"
        fi
    fi

    exit 0
}

trap post_process SIGINT

get_events() {
    perflist=`perf list`
    support_core="0"
    support_cmn=`echo ${perflist} | grep 'arm_cmn' | wc -l`
    support_dmc=`echo ${perflist} | grep 'dmc620_' | wc -l`
    while IFS= read -r line; 
    do
        #echo $line
        if [ -z "$line" ]; then continue; fi
        if [[ "$line" == "#"* ]]; then continue; fi
        if [[ "$line" == ";" ]]; then break; fi
        if [[ "$line" == "ARM_CMN"* ]] ; then   
            if [[ $support_cmn != "0" ]] ; then
                events_cmn="$events_cmn""$line",
            fi
        elif [[ "$line" == "dmc620_"* ]]; then
            if [[ $support_dmc != "0" ]] ; then
                events_dmc="$events_dmc""$line",
            fi
        else
            pname=`cut -d'|' -f1 <<< ${line} | xargs`
            #extract hex representation if present
            rval=""
            if [[ "$line" =~ .*"|".* ]]; then
                rval=`cut -d'|' -f2 <<< ${line} | xargs`
            fi

            #remove :k :u extensions before checking the name in perf list
            name=`cut -d':' -f1 <<< ${pname}`
            support_core=`echo ${perflist} | grep -sw ${name} | wc -l`

            if [[ $support_core != "0" ]]; then
                events_core="$events_core""$pname",
            elif [ ! -z "$rval" ]; then
                events_core="$events_core""$rval", 
            fi
        fi
    done < "$event_file"
    #remove the last ","
    if [ ! -z ${events_core} ]; then events_core=${events_core::-1} ; fi
    if [ ! -z ${events_dmc} ] ; then events_dmc=${events_dmc::-1} ; fi
    if [ ! -z ${events_cmn} ]; then 
        events_cmn=${events_cmn::-1} ; 
        # replace the CMN name
		events_cmn=`echo $events_cmn | sed 's/ARM_CMN_0/'${ARM_CMN_0}'/g; s/ARM_CMN_1/'${ARM_CMN_1}'/g'`
    fi 
}

# default interval may be too large to fit multiple events in one sampling interval. set to 4ms
set_perf_mux_interval() {

    PERF_MUX_FILES=$(find /sys/devices/*pmu*/perf_event_mux_interval_ms)
    PERF_MUX_FILES="$PERF_MUX_FILES $(find /sys/devices/arm_cmn*/perf_event_mux_interval_ms 2>/dev/null)"
    PERF_MUX_FILES="$PERF_MUX_FILES $(find /sys/devices/dmc*/perf_event_mux_interval_ms 2>/dev/null)"

    for f in ${PERF_MUX_FILES[@]}; do
        if [ $mux_interval -eq 0 ]; then mux_interval=`cat $f`; fi
        echo 4 > $f
    done
}

# reset perf muxing interval to default value
reset_perf_mux_interval() {
    for f in ${PERF_MUX_FILES[@]}; do
        if [ $mux_interval -ne 0 ]; then echo $mux_interval > $f; fi
    done
}

print_progress_bar() {
    iter=`expr $1 / 10`
    
    for ((k = 0; k <= 10 ; k++))
    do
        echo -n "Waiting for PMU sampling to complete($1 seconds)... [ "
        for ((i = 0 ; i <= k; i++)); do echo -n "#"; done
        for ((j = i ; j <= 10 ; j++)); do echo -n " "; done
        v=$((k * 10))
        echo -n " ] "
        echo -n "$v %" $'\r'
        sleep $iter
    done
    echo
}

collect_pmu() {
    #JOBS=""
    persocket_cmd=""
    agg_cmd=" -a "
    if [ $persocket -eq 1 ]; then
        persocket_cmd=" --per-socket "
    fi
    if [ ! -z $core_range ]; then
        agg_cmd=" -C ${core_range} " 
    fi
    CONSTCMD="perf stat -I $sample_interval -x, "

    # collecting all events in one command keeps timestamp the same for all events. 
#    all_events=${events_core}
#    if [ ! -z ${events_cmn} ];then
#        all_events=${all_events},${events_cmn}
#    fi
#    if [ ! -z ${events_dmc} ];then
#        all_events=${all_events},${events_dmc}
#    fi
#    CMD="${CONSTCMD} ${agg_cmd} ${persocket_cmd} -e ${all_events} -o $output_dir/core_pmu.csv ${workload_cmd} > /dev/null 2>&1 &"
#    eval "$CMD"
#    JOBS="$JOBS $!"
#
    if [ ! -z ${events_core} ]; then
        CMD="${CONSTCMD} ${agg_cmd} ${persocket_cmd} -e ${events_core} -o $output_dir/core_pmu.csv ${workload_cmd} &"
        eval "$CMD"
        JOBS="$JOBS $!"
	disown
    fi
    if [ ! -z ${events_cmn} ];then
        CMD="${CONSTCMD} -C 0 -e ${events_cmn} -o $output_dir/cmn_pmu.csv &"
        eval "$CMD"
        JOBS="$JOBS $!"
	disown
    fi
    if [ ! -z ${events_dmc} ]; then
        CMD="${CONSTCMD} -C 0 -e ${events_dmc} -o $output_dir/dmc_pmu.csv &"
        eval "$CMD"
        JOBS="$JOBS $!"
	disown
    fi 
}

while getopts ":n:i:t:o:j:c:psh" opt; do
    case $opt in
        n )
            sample_time="$OPTARG"
            ;;
        i )
            sample_interval="$((OPTARG * 1000))"
            ;;
        j )
            workload_cmd="$OPTARG"
            ;;
        c )
            core_range="$OPTARG"
            ;;
        p )
            with_plot=1
            ;;
        s )
            persocket=1
            ;;
        o )
            output_dir="$OPTARG"
            ;;
        h )
            usage
            exit 1
                ;;
        * )
            usage
            exit 1
            ;;
    esac
done

if [ `whoami` != 'root' ]; then echo "Must run with root!!!"; exit 1; fi

perf --help > /dev/null 2>&1
if [ ! $? -eq 0 ];then echo "perf tool does not exist."; exit 1; fi

if ! command -v python3 > /dev/null 2>&1
then
    echo "python3 does not exist. Will skip the post processing. "
    with_post=0
    with_plot=0
fi

if [ ! -f $event_file ] ; then echo "Events file not found!" ; exit 1; fi

if [ $sample_time -lt 10 ]; then echo "Sample time is too short!" ; exit 1; fi

if [ ! -z $output_dir ]; then
    mkdir -p $output_dir 2>/dev/null
fi

rm -rf $output_dir/*

NUM_OF_SOCKETS=`lscpu |grep Socket|awk '{print $2}'`
NUM_OF_NODES=`lscpu | grep -E "NUMA node[0-9]+" | wc -l`
NODE_PER_SOCKET=`expr $NUM_OF_NODES / $NUM_OF_SOCKETS`

# get arm_cmn name
if [ "$NUM_OF_SOCKETS" == "1" ]; then
    ARM_CMN_0=`perf list |grep 'watchpoint_up' |awk -F'/' 'NR==1{gsub(/ /,"",$1);print $1}'`
elif [ "$NUM_OF_SOCKETS" == "2" ]; then
    ARM_CMN_0=`perf list |grep 'watchpoint_up' |awk -F'/' 'NR==1{gsub(/ /,"",$1);print $1}'`
    ARM_CMN_1=`perf list |grep 'watchpoint_up' |awk -F'/' 'NR==2{gsub(/ /,"",$1);print $1}'`
else
    echo "Unexpected socket number ($NUM_OF_SOCKETS) !!"; exit 1
fi

get_events

set_perf_mux_interval

collect_pmu

if [ -z ${workload_cmd} ] ; then
    print_progress_bar $sample_time
    echo "Sample completed!"
else
    echo "waiting for workload to complete"
    for pid in ${JOBS[@]}; do
       wait $pid
       echo "Workload completed!"
       break
    done
fi

post_process

echo "All Done!"
exit 0

