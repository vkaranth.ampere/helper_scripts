#!/bin/bash
sudo bash -c "echo never | tee >/sys/kernel/mm/transparent_hugepage/enabled"
sudo bash -c "echo never | tee >/sys/kernel/mm/transparent_hugepage/defrag"
sudo bash -c "echo 100000000 | tee >/sys/kernel/debug/sched/latency_ns"
sudo bash -c "echo 80000 | tee >/sys/kernel/debug/sched/migration_cost_ns"
sudo bash -c "echo 600000 | tee >/sys/kernel/debug/sched/min_granularity_ns"
sudo bash -c "echo 100000 | tee >/sys/kernel/debug/sched/wakeup_granularity_ns"
sudo sysctl -w net.ipv4.tcp_rmem='10240	87380	12582912'
sudo sysctl -w net.ipv4.tcp_wmem='10240	87380	12582912'
sudo sysctl -w net.ipv4.ip_local_port_range='40000	65535'
