#!/bin/bash

#results of validation iterations 
RESULT_FILE=result_validation.txt

#number of iterations to run
ITERATION=2

function usage() {
	echo "usage $(basename $0) [-h][-r result_file][-i iterations]"
	echo "[-h] help"
	echo "[-r <result_file>] specify result file"
	echo "[-i <num iterations>] number of validation iterations"
}

function parse_args() {
while [ $# -gt 0 ];
do
    case "${1-}" in
        -h|--help) usage; exit 0 ;;
	-r) RESULT_FILE=${2}; shift ;; 
        -i) ITERATION=${2}; shift 2 ;;
        --) shift; break ;;
        *) usage; exit 0 ;;
    esac
    shift
done
}

#parse arguments
parse_args "$@"

# Below values need to be changed according to the experiment "tuned" knobs. Default values are listed below
# This is the list of all the knobs tested so far , not necessarily all in one experiment
#
##   TIER1_WORKER=80
##   TIER2_WORKER=16
##   TIER3_WORKER=16
##   MAP_POOLSIZE=80
##   FORKJOIN_WORKER=80
##   THRD_SATURATE=96
##   
##   XMS=31
##   XMX=31
##   SURVIVOR_RATIO=27
##   TARGET_SURVIVOR_RATIO=95
##   
##   ALLOC_PREFETCH_LINE=4
##   ALLOC_PREFETCH_DIS=512
##   INLINE_CODE=2048
##   TYPE_PROFILE_WIDTH=4
##   
##   GCTHREADS=80
##   GCTHREADS_FE=2
#
# While validating a given experiment, change the knobs that were part of that experiment to "tuned" values
# And keep the default values for other knobs 

TIER1_WORKER=80
TIER2_WORKER=16
TIER3_WORKER=16
MAP_POOLSIZE=80
FORKJOIN_WORKER=80
THRD_SATURATE=96

XMS=31
XMX=31
SURVIVOR_RATIO=27
TARGET_SURVIVOR_RATIO=95

ALLOC_PREFETCH_LINE=4
ALLOC_PREFETCH_DIS=512
INLINE_CODE=2048
TYPE_PROFILE_WIDTH=4

GCTHREADS=80
GCTHREADS_FE=2

XMN=$(( XMS - 2 ))

# formulate JVM Override flag
JVMOverride="-XX:AllocatePrefetchDistance=${ALLOC_PREFETCH_DIS} -XX:AllocatePrefetchLines=${ALLOC_PREFETCH_LINE} -XX:InlineSmallCode=${INLINE_CODE} -XX:TypeProfileWidth=${TYPE_PROFILE_WIDTH}"

# formulate Jbb Override flag
JbbOverride="-Dspecjbb.customerDriver.threads.saturate=${THRD_SATURATE} -Dspecjbb.forkjoin.workers.Tier1=${TIER1_WORKER} -Dspecjbb.forkjoin.workers.Tier2=${TIER2_WORKER} -Dspecjbb.forkjoin.workers.Tier3=${TIER3_WORKER} -Dspecjbb.mapreducer.pool.size=${MAP_POOLSIZE} -Dspecjbb.forkjoin.workers=${FORKJOIN_WORKER}"

# formulate memopt override flag
MemOverride="-Xmx${XMX}g -Xms${XMS}g -Xmn${XMN}g -XX:SurvivorRatio=${SURVIVOR_RATIO} -XX:TargetSurvivorRatio=${TARGET_SURVIVOR_RATIO} -XX:+UseCompressedOops -XX:+UseCompressedClassPointers"

# formulate gcopt override flag
GCOverride="-XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS}"
GCFEOverride="-XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE}"

# save the knobs used in the ouput result file
echo "jvm override = " $JVMOverride > $RESULT_FILE
echo "JbbOverride = " $JbbOverride >> $RESULT_FILE
echo "MemOverride = "$MemOverride >> $RESULT_FILE
echo "GcOverride = "$GCOverride >> $RESULT_FILE
echo "GcFEOverride = "$GCFEOverride >> $RESULT_FILE

#run the validation step
Count=$ITERATION
echo "$RESULT_FILE"
while [ $Count -gt 0 ]; do
  Count=$(( Count - 1 ))

  pkill java
  sleep 2
  GCOptsOverride=${GCOverride} GCOptsFEOverride=${GCFEOverride} AdvancedJVMOptsOverride=${JVMOverride} AdvancedJbbOptsOverride=${JbbOverride} MemOptsOverride=${MemOverride} ./run_jbb2015_fast_v6.sh -m M4:C0-19:20-39:40-59:60-79 -r ${RESULT_FILE}
done


