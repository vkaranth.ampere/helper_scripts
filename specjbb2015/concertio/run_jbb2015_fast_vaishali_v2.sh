#!/bin/bash
# Ampere Computing Copyright 2020, 2021

VERSION="1.0"
export JAVA_HOME=/home/tblizzard/studio/fastjbb/AmpereOpenJDK/jdk-15.0.1-ga-r4fd68cdd-2020-11-23-02-19-58-b3332-C8-64K

function usage_short
{
  echo "Usage: $(basename $0) [-h][-d][-f][-r][-i <preset-ir>][-m <jbb-mode>][-t <settle-time>][--forkjoin-worker][--tier1-worker][--tier2-worker][--tier3-worker][--threads-saturate][--client-poolsize][--map-poolsize]"
  echo "    [-h|--help]"
  echo "    [-d|--dryrun] only print out the cmdline"
  echo "    [-f|--force-c2] force c2 compilation, default tiered compilation"
  echo "    [-r <result file> "
  echo "    [-i|--preset-ir <preset-ir-value>] default 100000"
  echo "    [-m|--jbb-mode <the-run-mode-of-specjbb>] default C"
  echo "    [-t|--settle-time <settle-time-in-seconds>] default 30"
  echo "    [--forkjoin-worker <worker>] default 80"
  echo "    [--tier1-worker <worker>] default 80"
  echo "    [--tier2-worker <worker>] default 1"
  echo "    [--tier3-worker <worker>] default 16"
  echo "    [--threads-saturate <threads>] default 96"
  echo "    [--map-poolsize <size>] default 80"
  echo "    [--client-poolsize <size>] default 256"
}

function usage
{
  echo "This tool is to help run FastJBB on Ampere Computing systems"
  echo "Version $VERSION (20210108), JDK 11+ required"
  usage_short
  echo ""
  echo "Prerequisites:"
  echo "    (1) JAVA_HOME should be set properly to a path of valid JDK11+"
  echo "    (2) ./specjbb2015-fast.jar is needed at the current directory"
  echo ""
  echo "Examples for run modes"
  echo "    (1) C1:Cm-n     : run Composite mode on cores m to n"
  echo "    (2) M2:Cm-n:j-k : run MultiJVM mode on cores m to n for group 1, and i to j for group 2"
  echo "    (3) M1:Nu-v     : run MultiJVM mode on nodes u to v with the single group"
  echo ""
  echo "The list of vars can be defined from runner to override the execution arguments:"
  echo "    (1) MemOptsOverride         : JVM heap configs overridden"
  echo "    (2) AdvancedJVMOptsOverride : JVM advanced options overridden"
  echo "    (3) AdvancedJbbOptsOverride : SPECjbb advanced options overridden"
  echo "    (4) TaskConfigOverride      : numactl options overridden"
  echo ""
  echo "Examples:"
  echo "    (1) $(basename $0) -i 100000 -t 30"
  echo "        Run with with preset IR 100000 jOPS and settle time 30s"
  echo "    (2) MemOptsOverride='-Xms100g -Xmx100g -Xmn96g -XX:SurvivorRatio=30 -XX:TargetSurvivorRatio=95' \\"
  echo "        AdvancedJVMOptsOverride='-XX:-DisableExplicitGC -XX:AllocatePrefetchDistance=512 \\"
  echo "                                 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4' \\"
  echo "        AdvancedJbbOptsOverride='-Dspecjbb.customerDriver.threads.saturate=96 -Dspecjbb.comm.connect.client.pool.size=256 \\"
  echo "                                 -Dspecjbb.forkjoin.workers.Tier1=80 -Dspecjbb.forkjoin.workers.Tier2=1 \\"
  echo "                                 -Dspecjbb.forkjoin.workers.Tier3=16 -Dspecjbb.mapreducer.pool.size=80' \\"
  echo "        $(basename $0) -i 100000 -t 240"
  echo "        Run with three options overridden for more configurable execution"
}

NumCores=$(nproc --all)
PresetIR=100000
SettleTime=30
DryRun=0
ForceC2=0

function getPhyPkgId() {
  ppid=$(cat /sys/devices/system/cpu/cpu${1}/topology/physical_package_id 2>/dev/null)
  if [ $? -ne 0 ] || [ -z "${ppid}" ] ; then echo -1; else echo ${ppid}; fi
}

function is2p() {
  pkgId0=$(getPhyPkgId 0)
  pkgId1=$(getPhyPkgId $((NumCores-1)))
  [ "x${pkgId0}" != "x${pkgId1}" ] # && echo "2p" || echo "1p"
  # if [ -n "$(echo ${1} | grep -E "Silver")" ]; then [ -n "$(echo ${1} | grep -E "31")" ];
  # else [ -n "$(echo ${1} | grep -E "159|95|223|255")" ]; fi # Altra:160, KP920:96, TX2:224, EYPC2-7742:256
}

# 4NPS Quad, 2NPS Hemi, 1NPS Mono
cpuinfo=$(echo $(lscpu | grep CPU))
if $(is2p "${cpuinfo}"); then
  NumOfSockets=2;
  if [[ "x${cpuinfo}" =~ "node7" ]]; then NodesPerSocket=4;
  elif [[ "x${cpuinfo}" =~ "node3" ]]; then NodesPerSocket=2;
  elif [[ "x${cpuinfo}" =~ "node1" ]]; then NodesPerSocket=1; fi
else NumOfSockets=1;
  if [[ "x${cpuinfo}" =~ "node3" ]]; then NodesPerSocket=4;
  elif [[ "x${cpuinfo}" =~ "node1" ]]; then NodesPerSocket=2;
  elif [[ "x${cpuinfo}" =~ "node0" ]]; then NodesPerSocket=1; fi
fi

NumNodes=$((NumOfSockets*NodesPerSocket))
# By default run on socket 0 only
CoresPerSocket=$((NumCores/NumOfSockets))
# By default 1P
UsedCores=${CoresPerSocket}
JbbMode="C1:C0-$((UsedCores-1))"
Tier1Worker=80
Tier2Worker=1
Tier3Worker=16
ThreadsSaturate=96
ClientPoolSize=256
ForkJoinWorker=80
MapReducePoolSize=80
ResultFile=""

setMode=0
ARGS=`getopt -o "hdfi:r:m:t:" -l "help,dryrun,force-c2,preset-ir:,jbb-mode:,settle-time:,tier1-worker:,tier2-worker:,tier3-worker:,threads-saturate:,client-poolsize:,forkjoin-worker:,map-poolsize:" -n "$0" -- "$@"`
while [ -n "${1}" ];
do
    case "${1}" in
        -h|--help) usage; exit 0 ;;
        -d|--dryrun) DryRun=1; shift ;;
        -f|--force-c2) ForceC2=1; shift ;;
	-r) ResultFile=${2}; shift 2;;
        -i|--preset-ir) PresetIR=${2}; shift 2 ;;
        -m|--jbb-mode) JbbMode=${2}; setMode=1; shift 2 ;;
        -t|--settle-time) SettleTime=${2}; shift 2 ;;
        --forkjoin-worker) ForkJoinWorker=${2}; shift 2 ;;
        --tier1-worker) Tier1Worker=${2}; shift 2 ;;
        --tier2-worker) Tier2Worker=${2}; shift 2 ;;
        --tier3-worker) Tier3Worker=${2}; shift 2 ;;
        --threads-saturate) ThreadsSaturate=${2}; shift 2 ;;
        --map-poolsize) MapReducePoolSize=${2}; shift 2 ;;
         
        --) shift; break ;;
        *) usage_short; exit 0 ;;
    esac
done

if [ -z "$JAVA_HOME" ] || [ ! -f $JAVA_HOME/bin/java ]; then
  echo "Error: JAVA_HOME was not well configured: ($JAVA_HOME)" >&2
  exit 1
fi
export PATH=$JAVA_HOME/bin:$PATH

nnodes=$(numactl -H | grep ava)
nnodes=${nnodes#*: }
nnodes=${nnodes% nodes*}
echo "nodes: ${nnodes}"
if [ -z "${nnodes}" ]; then
  echo "Error: numactl returns wrong state " >&2
  exit 1
fi

if ! [ -f ./specjbb2015-fast.jar ]; then
  echo "Error: Need to run with specjbb2015-fast.jar" >&2
  exit 1
fi

if [ ${setMode} -eq 0 ] || [ "x${JbbMode}" = "xC" ] ; then JbbMode="C1:C0-$((UsedCores-1))"; fi
if ! [[ ${JbbMode} =~ ^[CM][0-9]+:[CN][0-9\:\,\-]+$ ]]; then echo "Error: Wrong JbbMode: ${JbbMode}"; exit 1; fi

# JbbMode = ${JM1}${JM2}:${JM3}${JM4}
# Composite or MultiJVM or Distributed
JM1=${JbbMode:0:1}; ate=1; GroupCfg=${JbbMode:1}; GroupCnt=${GroupCfg%%:*}; CntLen=${#GroupCnt};
# 1..99
JM2=${JbbMode:1:${CntLen}}; ate=$((ate+CntLen)); ate=$((ate+1))
# C:Cores or N:Nodes
JM3=${JbbMode:${ate}:1}; ate=$((ate+1)); JM3=${JM3:-C}
# Indices of cores or nodes for running
JM4=${JbbMode:${ate}}
CheckRange() {
  IFS='[:]' read -r -a configs <<< "${JM4}"
  ConfigCnt=${#configs[@]}
  if [ ${GroupCnt} -ne ${ConfigCnt} ]; then
    if [ "x${JM3}" = "xC" ]; then
      if [ ${ConfigCnt} -gt 1 ]; then echo "Error, wrong jbb-mode: ${JbbMode}"; exit 1; fi
      # Allow auto split for:
      # M1 or M1:C - M1:C0-79
      # M2:C160    - M2:C0-79:80-159
      if [ ${ConfigCnt} -eq 1 ]; then UsedCores=${configs[0]}; fi
      CoresPerGrp=$((UsedCores/GroupCnt)) # mostly >1
      for((n=0;n<${GroupCnt};n++)); do
        ConfigStr+="$((CoresPerGrp*n))-$((CoresPerGrp*(n+1)-1)):"
      done
    else
      # Allow M1:N but disallow M2:N0
      if [ ${ConfigCnt} -ge 1 ]; then echo "Error, wrong jbb-mode: ${JbbMode}"; exit 1; fi
      for((n=0;n<${GroupCnt};n++)); do
        ConfigStr+="$((NumNodes*n/GroupCnt))-$((NumNodes*(n+1)/GroupCnt-1)):"
      done
    fi
    JM4=${ConfigStr%:}
  fi
}
CheckRange

echo "Args: jbb-mode: ${JbbMode}, preset-ir: ${PresetIR}, settle-time: ${SettleTime} sec, "
echo "      force-c2: ${ForceC2}, dry-run: ${DryRun}"
echo ""
java -Xshare:off -version 2>&1 | tee jbb_cmdline.txt
echo ""

CompOpt=
if [ ${ForceC2} -eq 1 ]; then
  CompOpts="-Xcomp -XX:-TieredCompilation"
fi

SettleTime=$(($SettleTime * 1000))
PresetDuration=$(($SettleTime * 2))

MemOpts="-Xmx31g -Xms31g -Xmn29g -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers "
if [ -n "${MemOptsOverride}" ]; then
  MemOpts="${MemOptsOverride}"
fi

AArch64Opts="-XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals "
if [ -n "$(arch)" ] && [[ "$(arch)" =~ "x86" ]]; then
  AArch64Opts=
fi
if [ -n "${AArch64OptsOverride}" ]; then
  AArch64Opts="${AArch64OptsOverride}"
fi

XmxArg=${MemOpts}; XmxArg=${XmxArg#*Xmx}; XmxArg=${XmxArg%% *}; echo "XmxArg=${XmxArg}" >> ./jbb_bldinfo.txt

if [ "x${XmxArg:0:1}" = "x3" ]; then
  MemOptsCT="-Xms28g -Xmx28g -Xmn24g -XX:SurvivorRatio=4 "
  MemOptsTX="-Xms9g -Xmx9g -Xmn8g"
else
  MemOptsCT="-Xms3g -Xmx3g -Xmn2g"
  MemOptsTX="-Xms4g -Xmx4g -Xmn3g"
fi
MemOptsBE=${MemOpts}

AdvancedJVMOpts="-XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4 -XX:+PrintFlagsFinal ${AdvancedJVMOptsOverride} "
AdvancedJbbOpts="-Dspecjbb.comm.connect.client.pool.size=${ClientPoolSize} -Dspecjbb.mapreducer.pool.size=${MapReducePoolSize} -Dspecjbb.customerDriver.threads.saturate=${ThreadsSaturate} \
-Dspecjbb.forkjoin.workers=${ForkJoinWorker} -Dspecjbb.forkjoin.workers.Tier1=${Tier1Worker} \
-Dspecjbb.forkjoin.workers.Tier2=${Tier2Worker} -Dspecjbb.forkjoin.workers.Tier3=${Tier3Worker} ${AdvancedJbbOptsOverride} "
AdvancedJbbOptsIN="-Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 "
AdvancedJbbOptsCT="${AdvancedJbbOptsIN} ${AdvancedJbbOpts} -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 "
AdvancedJbbOptsTX="${AdvancedJbbOptsIN} "
AdvancedJbbOptsBE="${AdvancedJbbOptsIN} "
echo "-----------Advanced jbb options: $AdvancedJbbOpts"
echo "-----------Advanced jbb options CT: $AdvancedJbbOptsCT"
BasicOpts="-showversion -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints \
           -XX:-UsePerfData -XX:+UseLargePages -XX:+UseTransparentHugePages "
LogOpts="-Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc.log:time,uptime,level,tags:filecount=0 "
AdvancedOpts="${AArch64Opts} ${AdvancedJVMOpts} ${AdvancedJbbOpts} ${LogOpts} "
AdvancedOptsCT="${AdvancedJbbOptsCT} "
AdvancedOptsTX="${AdvancedJbbOptsTX} "
AdvancedOptsBE="${AdvancedJVMOpts} ${AArch64Opts} ${AdvancedJbbOptsBE} ${LogOpts} "

# $((8+(${UsedCores}-8)*5/8))
GCOpts="-XX:+UseParallelGC -XX:ParallelGCThreads=${UsedCores} "
GCOptsFE="-XX:+UseParallelGC -XX:ParallelGCThreads=2 "
GCOptsBE="${GCOpts}"

FastOpts="-Dspecjbb.controller.type=PRESET -Dspecjbb.controller.rampup.steps=1 -Dspecjbb.controller.preset.ir=${PresetIR} -Dspecjbb.controller.preset.duration=${PresetDuration} \
          -Dspecjbb.controller.profile.duration.min=0 -Dspecjbb.controller.settle.time.max=${SettleTime} -Dspecjbb.controller.validation.skip=true "
FastJar="-jar ./specjbb2015-fast.jar "

UsedSockets=1
Found2P() { if [ ${UsedSockets} -eq 1 ]; then UsedSockets=2; UsedCores=${NumCores}; fi }

# NumOfSockets=2
# CoresPerSocket=80
FindBindNodes() {
  IFS='[,:]' read -a inputRange <<< "${1}"
  cpuNodes=()
  if [ "${JM3}" = "N" ]; then
    nodeRanges=(${inputRange[*]})
    for range in ${nodeRanges[@]}; do
      rangeSeq="$(seq "${range%-*}" "${range#*-}")"
      nodes=(${rangeSeq})
      cpuNodes+=(${nodes[*]})
    done
  else
    cpuList=()
    cpuRanges=(${inputRange[*]})
    for range in ${cpuRanges[@]}; do
      rangeSeq="$(seq "${range%-*}" "${range#*-}")"
      cpus=(${rangeSeq})
      cpuList+=(${cpus[*]})
    done
    # create core->node map
    while read nodeToken nodeNumber cpuToken cpuArray ; do
      for cpu in ${cpuArray} ; do
        cpuToNodeMap[${cpu}]=${nodeNumber}
      done
    done < <(numactl --hardware | grep -E '^node [0-9]+ cpus: ')
    # find nodes according to cores
    for arg in "${cpuList[@]}" ; do
        cpuNodes+=(${cpuToNodeMap[${arg}]})
    done
  fi

  # sort and commbine
  IFS=$'\n' cpuNodes=($(sort -n <<<"${cpuNodes[*]}"));
  IFS=$'\n' cpuNodes=($(uniq <<<"${cpuNodes[*]}"));
  cpuNodes="${cpuNodes[*]}"
  cpuNodes=(${cpuNodes})

  idx=0;separater=;nodeList=();
  nodeList[0]=${cpuNodes[0]}
  for ((i=1;i<${#cpuNodes[@]};i++)); do
    if ((cpuNodes[i] > (cpuNodes[i-1]+1))); then
      separater=","
      nodeList[$((++idx))]="${separater}"
      nodeList[$((++idx))]="${cpuNodes[$i]}"
    else
      if [ "x${separater}" = "x-" ]; then
        # continue previous (m)-(n) to be (m)-(n+1)
        nodeList[$((idx))]="${cpuNodes[$i]}"
      else
        separater="-"
        nodeList[$((++idx))]="${separater}"
        nodeList[$((++idx))]="${cpuNodes[$i]}"
      fi
    fi
  done
  nodeList=$( IFS=''; echo "${nodeList[*]}" )
  echo "${nodeList}"

  # help test is2p
  IFS='[,\-]' read -a nodeList <<< "${nodeList}"
  for node in ${nodeList[@]}; do if ((node >= (NumNodes/2))); then Found2P; break; fi; done
}

CombineRanges() {
  IFS='[,:]' read -a inputRanges <<< "${1}"
  rangeList=()
  for range in ${inputRanges[@]}; do
      rangeSeq="$(seq "${range%-*}" "${range#*-}")"
      ranges=(${rangeSeq})
      rangeList+=(${ranges[*]})
  done

  singleRanges=(${rangeList[*]})
  IFS=$'\n' singleRanges=($(sort -n <<<"${singleRanges[*]}"));
  IFS=$'\n' singleRanges=($(uniq <<<"${singleRanges[*]}"));
  singleRanges="${singleRanges[*]}"
  singleRanges=(${singleRanges})

  idx=0;separater=;
  combinedRanges[0]=${singleRanges[0]}
  for ((i=1;i<${#singleRanges[@]};i++)); do
      if ((singleRanges[i] > (singleRanges[i-1]+1))); then
          separater=","
          combinedRanges[$((++idx))]="${separater}"
          combinedRanges[$((++idx))]="${singleRanges[$i]}"
      else
          if [ "x${separater}" = "x-" ]; then
              # continue previous (m)-(n) to be (m)-(n+1)
              combinedRanges[$((idx))]="${singleRanges[$i]}"
          else
              separater="-"
              combinedRanges[$((++idx))]="${separater}"
              combinedRanges[$((++idx))]="${singleRanges[$i]}"
          fi
      fi
  done

  combinedRanges=$( IFS=''; echo "${combinedRanges[*]}" )
  echo "${combinedRanges}"
}

BindPolicy() {
  if [ "${JM3}" = "C" ]; then echo "--physcpubind=${1}"; else echo "--cpunodebind=${1}"; fi
}
MemPolicy() {
  if [[ "x${1}" =~ [,-] ]]; then echo "--interleave=${1}"; else echo "--membind=${1}"; fi
}

GroupCnt=${JM2}
GroupTop=$((GroupCnt-1))
bindRangeAll=${JM4}; bindRangeAll=${bindRangeAll//[:]/,}
# A dumny call to update UsedSockets and UseCores, otherwise embedded callings will not do real updating
FindBindNodes ${bindRangeAll}
IFS='[:]' read -r -a ranges <<< ${JM4}
nodeRangeAll=$(FindBindNodes "${bindRangeAll}")
bindRangeAll=$(CombineRanges "${bindRangeAll}")
if [ "${JM1}" = "C" ]; then
  TaskConfig="$(BindPolicy ${JM4}) $(MemPolicy ${nodeRangeAll})"
  if [ -n "${TaskConfigOverride}" ]; then TaskConfig="${TaskConfigOverride}"; fi

#---vaishali: commented AdvancedOptsCT because these are cmd line args now----
  # Update UsedCores relevant settings
#  AdvancedOpts=${AdvancedOpts/workers=+([0-9]) /workers=${UsedCores} }
#  AdvancedOpts=${AdvancedOpts/Tier1=+([0-9]) /Tier1=${UsedCores} }
  GCOpts="-XX:+UseParallelGC -XX:ParallelGCThreads=${UsedCores} "

  # Assemble command lines
  RunCmdC="numactl ${TaskConfig} java ${CompOpts} ${MemOpts} ${BasicOpts} ${GCOpts} ${AdvancedOpts} ${FastOpts} ${FastJar} -m COMPOSITE"
  RunCmdC="$(echo ${RunCmdC} | tr -s ' ')"
elif [ "${JM1}" = "M" ]; then
  # Prepare task configs
  declare -a TaskConfigMTX
  declare -a TaskConfigMBE
  if [ "${JM3}" = "C" ]; then
    TaskConfigMC="$(BindPolicy ${bindRangeAll}) $(MemPolicy ${nodeRangeAll})"
    for i in $(seq 0 ${GroupTop}); do
      nodeRange=$(FindBindNodes ${ranges[${i}]})
      TaskConfigMTX[${i}]="$(BindPolicy ${ranges[${i}]}) $(MemPolicy ${nodeRange})";
      TaskConfigMBE[${i}]="$(BindPolicy ${ranges[${i}]}) $(MemPolicy ${nodeRange})";
    done
  else
    TaskConfigMC="$(BindPolicy ${bindRangeAll}) $(MemPolicy ${nodeRangeAll})"
    for i in $(seq 0 ${GroupTop}); do
      TaskConfigMTX[${i}]="$(BindPolicy ${ranges[${i}]}) $(MemPolicy ${ranges[${i}]})";
      TaskConfigMBE[${i}]="$(BindPolicy ${ranges[${i}]}) $(MemPolicy ${ranges[${i}]})";
    done
  fi

  # Update UsedCores relevant settings
  # "+([0-9])" in shell does not work on some systems
#---vaishali: commented AdvancedOptsCT because these are cmd line args now----
#  AdvancedOptsCT=$(echo ${AdvancedOptsCT} | sed -E "s/workers=[0-9]+/workers=${UsedCores}/")
#  AdvancedOptsCT=$(echo ${AdvancedOptsCT} | sed -E "s/Tier1=[0-9]+/Tier1=${UsedCores}/")
#  AdvancedOptsCT=$(echo ${AdvancedOptsCT} | sed -E "s/Tier2=[0-9]+/Tier2=4/")
#  if [ ${UsedSockets} -eq 2 ]; then
#    AdvancedOptsCT=$(echo ${AdvancedOptsCT} | sed -E "s/Tier2=[0-9]+/Tier2=32/")
#    AdvancedOptsCT=$(echo ${AdvancedOptsCT} | sed -E "s/Tier3=[0-9]+/Tier3=32/")
#  fi
  # TODO: overwrite Tiers with AdvancedJbbOptsOverride
  GCOptsBE="-XX:+UseParallelGC -XX:ParallelGCThreads=$((UsedCores/GroupCnt)) "

  # Assemble command lines
  RunCmdMC="numactl ${TaskConfigMC} java ${MemOptsCT} ${BasicOpts} ${GCOptsFE} ${AdvancedOptsCT} -Dspecjbb.group.count=${GroupCnt} ${FastOpts} ${FastJar} -m MULTICONTROLLER"
  RunCmdMC="$(echo ${RunCmdMC} | tr -s ' ')"
  for i in $(seq 0 ${GroupTop}); do
    RunCmdMTX[${i}]="numactl ${TaskConfigMTX[${i}]} java ${MemOptsTX} ${BasicOpts} ${GCOptsFE} ${AdvancedOptsTX} ${FastJar} -m TXINJECTOR -G=Group${i} -J=JvmTX${i}"
    RunCmdMBE[${i}]="numactl ${TaskConfigMBE[${i}]} java ${MemOptsBE} ${BasicOpts} ${GCOptsBE} ${AdvancedOptsBE} ${FastJar} -m BACKEND -G=Group${i} -J=JvmBE${i}"
    RunCmdMTX[${i}]="$(echo ${RunCmdMTX[${i}]} | tr -s ' ')"
    RunCmdMBE[${i}]="$(echo ${RunCmdMBE[${i}]} | tr -s ' ')"
  done
else
  echo "Error, jbb-mode not supported: ${JbbMode}"
  exit 1
fi

if [ -n "${RunCmdC}" ]; then
  echo "${RunCmdC}" | tee -a jbb_cmdline.txt
else
  echo "${RunCmdMC}" | tee -a jbb_cmdline.txt
  for i in $(seq 0 ${GroupTop}); do
    echo "------------"; echo "${RunCmdMTX[${i}]}" | tee -a jbb_cmdline.txt
    echo "------------"; echo "${RunCmdMBE[${i}]}" | tee -a jbb_cmdline.txt
  done
fi

errpid=0
cpid=0
declare -a tpid bpid
jbbrun() {
  RunCmd="${1}"
  RunLog="${2}"
  AgentType="${3}"
  AgentIndex="${4}"

  pid1=$(pgrep -n java)
  if [ "x${AgentType}" = "xc" ]; then
    eval "time \time ${RunCmd} 2>&1 | tee ${RunLog} &"
  else
    eval "\time ${RunCmd} > ${RunLog} 2>&1 &"
  fi
  sleep 2
  pid2=$(pgrep -n java)
  if [ "x${pid1}" != "x${pid2}" ]; then
    if [ "x${AgentType}" = "xc" ]; then
      cpid=${pid2}
    elif [ "x${AgentType}" = "xt" ]; then
      tpid[${AgentIndex}]=${pid2}
    elif [ "x${AgentType}" = "xb" ]; then
      bpid[${AgentIndex}]=${pid2}
    fi
  fi
}

checkpid() {
  if [ "x$(ps ${1} >/dev/null 2>&1 && echo 1 || echo 0)" = "x0" ]; then
    echo "${2}:${1}"
    errpid=1
  fi
}

if [ ${DryRun} -eq 0 ]; then
  if [ -n "${RunCmdC}" ]; then
    jbbrun "${RunCmdC}" "jbb_runlog0_controller.log" c
  else
    jbbrun "${RunCmdMC}" "jbb_runlog0_controller.log" c
    for i in $(seq 0 ${GroupTop}); do
      jbbrun "${RunCmdMTX[${i}]}" "jbb_runlog0_txinjector_${i}.log" t ${i}
      jbbrun "${RunCmdMBE[${i}]}" "jbb_runlog0_backend_${i}.log" b ${i}
    done
  fi

  if [ ${cpid} -ne 0 ]; then timeout 10s tail --pid=${cpid} -f /dev/null; fi
  # check pids
  if [ -n "${RunCmdC}" ]; then
    checkpid ${cpid} "[Error]Composite process not found";
    if [ ${errpid} -eq 1 ]; then exit 1; fi
  else
    checkpid ${cpid} "[Error]Controller process not found";
    for i in $(seq 0 ${GroupTop}); do
      checkpid ${tpid[${i}]} "[Error]TxInjector ${i} process not found";
      checkpid ${bpid[${i}]} "[Error]Backend ${i} process not found";
      if [ ${errpid} -eq 1 ]; then
        kill ${cpid}
        exit 1
      fi
    done
  fi
  if [ ${cpid} -ne 0 ]; then tail --pid=${cpid} -f /dev/null; fi
  echo "----------------------"
  if [ -z $ResultFile ]; then
          cat result/*/*/logs/*ter.out | grep SCORE | sort | tail -1 | awk '{print $4}' | tee /tmp/result_fastjbb_fix
  else
	  cat result/*/*/logs/*ter.out | grep SCORE | sort | tail -1 | awk '{print $4}' >> $ResultFile
  fi
fi
