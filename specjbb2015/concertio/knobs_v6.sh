domain:
  common:
    knobs:
      TIER1_WORKER:
        options:
          range:
            min: 20
            max: 200
            step: 10
        default: 80

      TIER2_WORKER:
        options:
          range:
            min: 0
            max: 200
            step: 8
        default: 16

      TIER3_WORKER:
        options:
          range:
            min: 0
            max: 200
            step: 8
        default: 16
      
      MAP_POOLSIZE:
        options:
          range:
            max: 160
            min: 20
            step: 10
        default: 80
        

      FORKJOIN_WORKER: 
        options:
          range:
            max: 200
            min: 60
            step: 10
        default: 80

      THRD_SATURATE: 
        options:
          values: [60, 70, 80, 96, 100, 120]
        default: 96

      XMS:
        options:
          range:
            max: 80
            min: 20
            step: 1
        default: 31 
      
      XMX:
        options:
          range:
            max: 80
            min: 20
            step: 1
        default: 31 
      
      ALLOC_PREFETCH_DIS:
        options:
          values: [32, 64, 128, 192, 512, 1024, 2048, 4096]
        default: 512

      ALLOC_PREFETCH_LINE:
        options:
          range:
            max: 16
            min: 1
            step: 1
        default: 4

      INLINE_CODE:
        options:
          values: [64, 256, 512, 1024, 2048, 4096]
        default: 2048

      TYPE_PROFILE_WIDTH:
        options:
          values: [2, 4, 8, 16, 32]
        default: 4

      SURVIVOR_RATIO:
        options:
          range:
            max: 40
            min: 6
            step: 1
        default: 27

      TARGET_SURVIVOR_RATIO:
        options:
          range:
            max: 98
            min: 80
            step: 1
        default: 95
      
      GCTHREADS_FE:
        options:
          range:
            max: 10
            min: 2
            step: 1
        default: 2

      GCTHREADS:
        options:
          range:
            max: 100
            min: 40
            step: 5
        default: 80
        
    metrics:
      result:
        kind: file
        path: /tmp/result_fastjbb_v3
    target: result:max
    
    
global_settings:
  min_baseline_samples: 5
  min_samples_per_config: 5
  project_guid: a3411a60-7360-4aa2-b561-e8e72cea9ce1 #7481f40b-69f7-49f5-bf15-b5e08b54030a
  optimization_strategy_settings:
    evolution:
      full_cycle_coeff: 2  
  
workload:
  kind: sync
  start_command: ./workload_v6.sh
  timeout: 300s

stages:
  validation:
    duration: 15m
    max_samples: 30
    min_samples: 10

