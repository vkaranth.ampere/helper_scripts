#!/bin/bash

RESULT_FILE=/tmp/result_fastjbb_v3

#test with defaults
#TIER1_WORKER=80
#TIER2_WORKER=1
#TIER3_WORKER=16
#MAP_POOLSIZE=80
#FORKJOIN_WORKER=80
#THRD_SATURATE=96
#XMS=31
#XMX=31
#ALLOC_PREFETCH_LINE=4
#ALLOC_PREFETCH_DIS=512
#INLINE_CODE=2048
#TYPE_PROFILE_WIDTH=4

# validate knob inputs
# validate jbb set of knobs
if [ $TIER2_WORKER -gt $TIER1_WORKER ] ; then
    echo "invalid" > ${RESULT_FILE}
fi

# validate memory alloc knobs
if [ $XMX -lt $XMS ] ; then
	echo "invalid" > ${RESULT_FILE}
fi

TotalMemReq=$(( XMS + XMX + XMN ))
if [ $TotalMemReq -gt 200 ] ; then
	echo "invalid" > ${RESULT_FILE}
fi

XMN=$(( XMS - 2 ))

# formulate Jbb Override flag
JbbOverride="-Dspecjbb.customerDriver.threads.saturate=${THRD_SATURATE} -Dspecjbb.forkjoin.workers.Tier1=${TIER1_WORKER} -Dspecjbb.forkjoin.workers.Tier2=${TIER2_WORKER} -Dspecjbb.forkjoin.workers.Tier3=${TIER3_WORKER} -Dspecjbb.mapreducer.pool.size=${MAP_POOLSIZE} -Dspecjbb.forkjoin.workers=${FORKJOIN_WORKER}"

# formulate memopt override flag
MemOverride="-Xmx${XMX}g -Xms${XMS}g -Xmn${XMN}g -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers"

echo "JbbOverride = " $JbbOverride
echo "MemOverride = "$MemOverride

pkill java
sleep 2
AdvancedJbbOptsOverride=${JbbOverride} MemOptsOverride=${MemOverride} ./run_jbb2015_fast_v3.sh -m M4:C0-19:20-39:40-59:60-79 

