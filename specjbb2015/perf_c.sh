#!/bin/bash
# Copyright (c) 2020 Ampere Computing.  All rights reserved.
# Author: patrickz

function setsync { sysctl $1; sudo sysctl -w $1=$2; sysctl $1; sysctl -f -p; }

function vm_configure
{
  sudo sync
  setsync kernel.randomize_va_space 0
  setsync vm.dirty_ratio 40
  setsync vm.dirty_background_ratio 10
  setsync vm.drop_caches 3
  setsync vm.swappiness 10
  setsync vm.nr_hugepages 0
  setsync vm.dirty_expire_centisecs 10000
  setsync vm.dirty_writeback_centisecs 1500
}

function sched_configure
{
  sudo sync
  setsync kernel.sched_latency_ns            400000
  setsync kernel.sched_migration_cost_ns     40000
  setsync kernel.sched_min_granularity_ns    400000000
  setsync kernel.sched_nr_migrate            128
  setsync kernel.sched_rt_runtime_us         950000
  setsync kernel.sched_wakeup_granularity_ns 40000
}

if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo $0 $*"
    exit 1
fi

sudo cpupower -c all frequency-set -g performance
sudo tuned-adm profile throughput-performance
sudo setenforce 0
sudo bash -c "echo 0 | tee >/proc/sys/kernel/numa_balancing >(cat)"
sudo bash -c "echo never | tee >/sys/kernel/mm/transparent_hugepage/enabled >(cat)"
#sudo bash -c "echo always | tee >/sys/kernel/mm/transparent_hugepage/enabled >(cat)"
#sudo bash -c "echo always | tee >/sys/kernel/mm/transparent_hugepage/defrag >(cat)"
sudo bash -c "echo 1 | tee >/proc/sys/vm/compact_memory >(cat)"
sched_configure
vm_configure
setsync net.core.somaxconn 65536
setsync net.ipv4.tcp_max_syn_backlog  65536
setsync net.core.netdev_max_backlog 655560
setsync net.ipv4.tcp_no_metrics_save 1
