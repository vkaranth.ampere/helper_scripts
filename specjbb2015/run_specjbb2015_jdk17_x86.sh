#!/bin/bash
# Copyright (c) 2020-2021 Ampere Computing.  All rights reserved.

# Comp 0

CORES=`nproc --all`
WORKERS=$CORES
SATURATE_THRDS=64
OBJECT_ALIGNMENT=32
MEMORY=16
TAG="0"
#JAVA_HOME=/home/vkaranth/specjbb2015/jdk-17.0.4+2-r2680e83f-2022-05-17-05-35-04-b6431-U20-4K
#jdk-17.0.3-ga-r90d4233f-2022-04-27-02-11-38-b6233-U18-4K
JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64

show_help(){
    echo "    -w : forkjoin workers        usage: -w workers"
    echo "    -m : xms/xmn memory in gb    usage: -m memory"
    echo "    -o : object alignment byte   usage: -o bytes (power of 2). default is 32 for 128G"
    echo "    -s : saturate threads        usage: -s threads"
    echo "    -t : result tag              usage: -t tagname"
}

parse() {
    while [ $# -gt 0 ]; do
        case "${1-}" in
	    -h )
                show_help
		exit
		;;
	    -w )
		WORKERS=${2-}
		shift
		;;
	    -m )
	        MEMORY=${2-}
		shift
		;;
	    -s )
	        SATURATE_THRDS=${2-}
		shift
		;;
	    -o )
               OBJECT_ALIGNMENT=${2-}
	       shift
	       ;;
	    -t )
	        TAG=${2-}
		shift
		;;
        esac
	shift
    done
}

run() {

    GC_THRDS=$WORKERS
    XMN=$(( (MEMORY * 94) / 100 ))
    echo "GC=$GC_THRDS xmn=$XMN"

    ${JAVA_HOME}/bin/java -Xms"$MEMORY"g -Xmx"$MEMORY"g -Xmn"$XMN"g -XX:SurvivorRatio=37 -XX:ObjectAlignmentInBytes="$OBJECT_ALIGNMENT" -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -XX:MetaspaceSize=64m -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads="$GC_THRDS" -XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4 -XX:+SegmentedCodeCache -XX:ReservedCodeCacheSize=64M -XX:InitialCodeCacheSize=64M  -Dspecjbb.customerDriver.threads=64 -Dspecjbb.customerDriver.threads.service=64 -Dspecjbb.customerDriver.threads.probe=64 -Dspecjbb.customerDriver.threads.saturate=96 -Dspecjbb.forkjoin.workers=32 -Dspecjbb.forkjoin.workers.Tier1="$WORKERS" -Dspecjbb.forkjoin.workers.Tier2=1 -Dspecjbb.forkjoin.workers.Tier3=8 -Dspecjbb.comm.connect.selector.runner.count=4 -Dspecjbb.controller.type=HBIR_RT -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m COMPOSITE 2>&1 | tee controller_log_${TAG}.log &

##cmd with C1x60g from Patrick
#    ${JAVA_HOME}/bin/java -Xms"$MEMORY"g -Xmx"$MEMORY"g -Xmn"$XMN"g -XX:SurvivorRatio=37 -XX:ObjectAlignmentInBytes="$OBJECT_ALIGNMENT" -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -XX:MetaspaceSize=64m -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads="$GC_THRDS" -XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+AvoidUnalignedAccesses -XX:BlockZeroingLowLimit=64K -XX:+UseBlockZeroing -XX:-UseSIMDForArrayEquals -XX:+UseSIMDForMemoryOps -Dspecjbb.customerDriver.threads=64 -Dspecjbb.customerDriver.threads.service=64 -Dspecjbb.customerDriver.threads.probe=64 -Dspecjbb.customerDriver.threads.saturate="$SATURATE_THRDS" -Dspecjbb.forkjoin.workers="$WORKERS" -Dspecjbb.forkjoin.workers.Tier1="$WORKERS" -Dspecjbb.forkjoin.workers.Tier2=1 -Dspecjbb.forkjoin.workers.Tier3=16 -Dspecjbb.comm.connect.selector.runner.count=4 -Dspecjbb.controller.type=HBIR_RT -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m COMPOSITE 2>&1 | tee controller_log_${TAG}.log &
if [ "x$(basename $(pwd))" = "xjbb_test" ]; then
  echo "Not to wait for completion if no jbb_test directory."
else
  echo "Wait for the completion."
  wpid=$(ps -eaf | grep -v grep | grep -v numactl | grep -E "COMPOSITE|MULTICONTROLLER" | awk '{print $2;}' | tail -n 1)
  if [[ ${wpid} =~ ^[0-9]+$  ]]; then
    tail --pid=${wpid} -f /dev/null -s 10
  fi
  echo "Wait 30s for cooling down."
  sleep 30
fi
sleep 10;./check.sh
}

parse "$@"
echo "workers=$WORKERS mem=$MEMORY saturate_threads=$SATURATE_THRDS"
run 
