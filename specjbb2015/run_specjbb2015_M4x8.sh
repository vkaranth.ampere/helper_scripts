#!/bin/bash

###############################################################################
# Sample script for running SPECjbb2015 in MultiJVM mode.
# 
# This sample script demonstrates running the Controller, TxInjector(s) and 
# Backend(s) in separate JVMs on the same server.
###############################################################################

# Launch command: java [options] -jar specjbb2015.jar [argument] [value] ...
if [ `whoami` != 'root' ]; then echo "must be root"; exit 1; fi

cores=`cat /proc/cpuinfo | grep processor | wc -l`
zones=`numactl -H | grep cpus | wc -l`
let "zonecores = cores / zones"

# Default values
let PARSE_GC=0
let "RAM_MB_PER_CORE=3000"
GRAAL_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCI -XX:+UseJVMCICompiler"
JAVA_HOME=/home/ubuntu/specjbb/jdk-17.0.4+2-r2680e83f-2022-05-17-05-35-04-b6431-U20-4K
JAVA=$JAVA_HOME/bin/java
echo $JAVA
if [ ! -e $JAVA ]; then
  yum -y install java-11-openjdk
fi

#MULTI_JVM_OVERRIDES="-Dspecjbb.customer.RemoteCustomerShare=0 -Dspecjbb.sm.replenish.localPercent=100"
MULTI_JVM_OVERRIDES=""

parse_gc() {

  if [ -f ./consume_gc_log.pex ]
  then
    for f in $result/gc/gc.log.*
    do
      ./consume_gc_log.pex -f $f > post_$f
    done
  else
    echo "*****************************************************"
    echo "consume_gc_log.pex not found."
    echo "to obtain it, build it with pants from source:"
    echo " ./pants binary vm-team/src/main/python/consume_gc_log"
    echo "then upload it to the your SpecJBB directory"
    echo "*****************************************************"
  fi
}


display_help() {
  echo " Optional arguments" 
  echo "    -b : backend size        usage: -b [backend size]"
  echo "    -g : group count         usage: -g [group count]"
  echo "    -c : use C2 / No GRAAL   usage: -c <no argument>"
  echo "    -d : Result directory    usage: -d [directory]  "
  echo "    -l : Parse GC logs       usage: -l <no argument>"
  echo "    -m : Std remote work %   usage: -m <no argument>"
  echo "    -r : ram mb per core     usage: -r [mb per core]"
  echo "    -h : help "
}

while getopts ":b:r:g:d:cmhl" opt; do
  case $opt in
    h )
      display_help
      exit 1
      ;;
    b )
      echo "Backend size is $OPTARG" >&2
      let BACKEND_SIZE=$OPTARG
      ;;
    g )
      echo "Group count is $OPTARG" >&2
      let GROUP_COUNT=$OPTARG
      ;;
    d )
      echo "Result directory set to: $OPTARG" >&2
      RESULT_DIR=$OPTARG
      ;;
    l )
      echo "Parsing GC logs" >&2
      let PARSE_GC=1
      ;;
    r )
      echo "Ram MB per core is $OPTARG" >&2
      let RAM_MB_PER_CORE=$OPTARG
      ;;
    c )
      echo "Using C2 instead of GRAAL" >&2
      let GRAAL_OPTS=""
      ;;
    m )
      echo "Using standard Multi-JVM behavior" >&2
      let MULTI_JVM_OVERRIDES=""
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

# Handle four cases:
# 1. Group and Backend size set --> Do nothing
# 2. Group count is set, Backend isn't
# 3. Backend is set, Group count isn't
# 4. Neither set.

#1
echo "$GROUP_COUNT=GROUP_COUNT"
echo "$BACKEND_SIZE=BACKEND_SIZE"

if [ -v GROUP_COUNT ] && [ ! -v BACKEND_SIZE ]; then
  let "BACKEND_SIZE=$cores / $GROUP_COUNT"
#2
elif [ -v BACKEND_SIZE ] && [ ! -v GROUP_COUNT ]; then
  let "GROUP_COUNT=$cores / $BACKEND_SIZE"
#3
elif [ ! -v GROUP_COUNT ] && [ ! -v BACKEND_SIZE ]; then
  # The default behavior is to set the number of groups
  # such that each group has 8 threads
  let "GROUP_COUNT=$cores / 8"
  let "GROUP_COUNT=$GROUP_COUNT-$GROUP_COUNT%2"
  let "BACKEND_SIZE=$cores / $GROUP_COUNT"
fi


# Number of TxInjector JVMs to expect in each Group
TI_JVM_COUNT=1

# Number of Groups (TxInjectors mapped to Backend) to expect
let "WORKER_SIZE=2*$BACKEND_SIZE"
let "GC_SIZE=($cores/$GROUP_COUNT)"
let "MR_SIZE=$GROUP_COUNT"

echo "GC_SIZE=$GC_SIZE MR_SIZE=$MR_SIZE GROUP_COUNT=$GROUP_COUNT WORKER_SIZE=$WORKER_SIZE"
echo "$MULTI_JVM_OVERRIDES"

# for the backend
let "maxheap = RAM_MB_PER_CORE * GC_SIZE"
maxmeg="${maxheap}M"
let "new = (maxheap*94)/100"
maxnew="${new}M"

# for the TxI
tximeg="3g"
txinew="2536m"

# for the controller
contmeg="3g"
contnew="2536m"

echo "max heap=$maxheap max new=$maxnew"

TI_JVM_COUNT=1


# Benchmark options for Controller / TxInjector JVM / Backend
# Please use -Dproperty=value to override the default and property file value
# Please add -Dspecjbb.controller.host=$CTRL_IP (this host IP) to the benchmark options for the all components
# and -Dspecjbb.time.server=true to the benchmark options for Controller 
# when launching MultiJVM mode in virtual environment with Time Server located on the native host.

ARGS_C=" -Xms3g -Xmx3g -Xmn2g -XX:+UseParallelGC -XX:ParallelGCThreads=2 -XX:CICompilerCount=2 -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -Dspecjbb.customerDriver.threads=64 -Dspecjbb.customerDriver.threads.service=64 -Dspecjbb.customerDriver.threads.probe=64 -Dspecjbb.customerDriver.threads.saturate=96 -Dspecjbb.forkjoin.workers=32 -Dspecjbb.forkjoin.workers.Tier1=32 -Dspecjbb.forkjoin.workers.Tier2=4 -Dspecjbb.forkjoin.workers.Tier3=16 -Dspecjbb.controller.maxir.maxFailedPoints=1 -Dspecjbb.comm.connect.selector.runner.count=4 -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.group.count=8 -Dspecjbb.controller.type=HBIR_RT -Dspecjbb.controller.port=24000 "

ARGS_B=" -Xms24g -Xmx24g -Xmn21g -XX:SurvivorRatio=19 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -XX:MetaspaceSize=64m -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads=12 -XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+AvoidUnalignedAccesses -XX:BlockZeroingLowLimit=64K -XX:+UseBlockZeroing -XX:-UseSIMDForArrayEquals -XX:+UseSIMDForMemoryOps -Dspecjbb.comm.connect.selector.runner.count=3 -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.client.pool.size=160 -Dspecjbb.comm.connect.worker.pool.max=60 -Dspecjbb.comm.connect.worker.pool.min=60 -Dspecjbb.controller.port=24000 "

SPEC_OPTS_C="-Dspecjbb.group.count=$GROUP_COUNT -Dspecjbb.txi.pergroup.count=$TI_JVM_COUNT -Dspecjbb.controller.rtcurve.warmup.step=0.2 -Dspecjbb.forkjoin.workers=$WORKER_SIZE -Dspecjbb.mapreducer.pool.size=8  $MULTI_JVM_OVERRIDES"

ARGS_T=" -Xms3g -Xmx3g -Xmn2g -XX:+UseParallelGC -XX:ParallelGCThreads=2 -XX:CICompilerCount=2 -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -Dspecjbb.comm.connect.selector.runner.count=3 -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.client.pool.size=160 -Dspecjbb.comm.connect.worker.pool.max=60 -Dspecjbb.comm.connect.worker.pool.min=60 -Dspecjbb.controller.port=24000 "

SPEC_OPTS_TI=""
SPEC_OPTS_BE="$MULTI_JVM_OVERRIDES"

# Java options for Controller / TxInjector / Backend JVM
JAVA_OPTS_C="-Xmx2g -Xms2g -Xmn1536m -XX:+AlwaysPreTouch -XX:ParallelGCThreads=2"
JAVA_OPTS_TI="-Xmx2g -Xms2g -Xmn1536m -XX:+AlwaysPreTouch -XX:ParallelGCThreads=2"
JAVA_OPTS_BE="-Xmx$maxmeg -Xms$maxmeg -Xmn$maxnew -XX:ParallelGCThreads=$GC_SIZE -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:MaxTenuringThreshold=15 -XX:-UseBiasedLocking -XX:SurvivorRatio=10 -XX:TargetSurvivorRatio=90 -XX:TargetSurvivorRatio=90 -XX:+UseParallelOldGC -XX:+PrintGCDetails"
######GCP#######
#GCP TX options
# numactl --cpunodebind 0 --membind 0 java -Xms2g -Xmx2g -Xmn1536m -XX:+AlwaysPreTouch -XX:ParallelGCThreads=2 -jar specjbb2015.jar -m txinjector -G GRP4 -J JVM1
#GCP BE options
# nohup numactl --cpunodebind 0 --membind 0 java -XX:ParallelGCThreads=12 -Xms18000m -Xmx18000m -Xmn16920m  -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:MaxTenuringThreshold=15 -XX:-UseBiasedLocking -XX:SurvivorRatio=10 -XX:TargetSurvivorRatio=90 -XX:TargetSurvivorRatio=90 -XX:+UseParallelOldGC -XX:+PrintGCDetails  -jar specjbb2015.jar -m backend -G GRP4 -J JVM2
#GCP Controller
#java -Xms2g -Xmx2g -Xmn1536m -XX:+AlwaysPreTouch -XX:ParallelGCThreads=2 -Dspecjbb.controller.rtcurve.warmup.step=0.5 -Dspecjbb.mapreducer.pool.size=8  -Dspecjbb.forkjoin.workers=24  -Dspecjbb.group.count=4 -jar specjbb2015.jar -m multicontroller -p config/specjbb2015.props
###########

# Optional arguments for multiController / TxInjector / Backend mode 
# For more info please use: java -jar specjbb2015.jar -m <mode> -h
MODE_ARGS_C=""
MODE_ARGS_TI=""
MODE_ARGS_BE=""

# Number of successive runs
NUM_OF_RUNS=1

###############################################################################
# This benchmark requires a JDK7 compliant Java VM.  If such a JVM is not on
# your path already you must set the JAVA environment variable to point to
# where the 'java' executable can be found.
###############################################################################

#if [ "$JAVA_HOME" == "" ]; then
#  JAVA_HOME="/usr/lib/jvm/jre-1.8.0/"
#fi


which $JAVA > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "ERROR: Could not find a 'java' executable. Please set the JAVA environment variable or update the PATH."
    exit 1
fi

trap ctrl_c SIGINT

function ctrl_c() {
  killall java
  sleep 1
  exit 1
}

UNAME=`uname -a`


for ((n=1; $n<=$NUM_OF_RUNS; n=$n+1)); do

# Create result directory
if [ -v RESULT_DIR  ]
then
  result=$RESULT_DIR
else
  timestamp=$(date '+%y-%m-%d_%H%M%S')
  result=./$timestamp
fi

  mkdir $result

  # Copy current config to the result directory
  cp -r config $result

  cd $result

  echo "Run $n: $timestamp"
  echo "Launching SPECjbb2015 in MultiJVM mode..."
  echo

  echo "Start Controller JVM"
  $JAVA $ARGS_C -jar ../specjbb2015.jar -m MULTICONTROLLER $MODE_ARGS_C 2>controller.log > controller.out &

  CTRL_PID=$!
  echo "Controller PID = $CTRL_PID"

  sleep 3

  for ((gnum=1; $gnum<$GROUP_COUNT+1; gnum=$gnum+1)); do

    GROUPID=Group$gnum
    let "NODEID= ($gnum % $zones)"

    echo -e "\nStarting JVMs from $GROUPID:"

    for ((jnum=1; $jnum<$TI_JVM_COUNT+1; jnum=$jnum+1)); do

        JVMID=txiJVM$jnum
        TI_NAME=$GROUPID.TxInjector.$JVMID

        echo "    Start $TI_NAME"

	echo "$ARGS_T -jar ../specjbb2015.jar -m TXINJECTOR -G=$GROUPID -J=$JVMID $MODE_ARGS_TI"

        $JAVA $ARGS_T -jar ../specjbb2015.jar -m TXINJECTOR -G=$GROUPID -J=$JVMID $MODE_ARGS_TI > $TI_NAME.log 2>&1 &
        echo -e "\t$TI_NAME PID = $!"
        sleep 1
    done

    JVMID=beJVM
    BE_NAME=$GROUPID.Backend.$JVMID
    #JAVA_OPTS_BE="$JAVA_OPTS_BE -Xlog:gc*=info,gc+ref*=off,gc+age=trace:file=gc.log.%t" 

    echo "    Start $BE_NAME"

    echo "numactl --cpunodebind $NODEID --membind $NODEID $JAVA $JAVA_OPTS_BE $GRAAL_OPTS $SPEC_OPTS_BE -jar ../specjbb2015.jar -m BACKEND -G=$GROUPID -J=$JVMID $MODE_ARGS_BE "
    $JAVA $ARGS_B -jar ../specjbb2015.jar -m BACKEND -G=$GROUPID -J=$JVMID $MODE_ARGS_BE > $BE_NAME.log 2>&1 &
    echo -e "\t$BE_NAME PID = $!"
    sleep 1

  done
  echo
  echo "SPECjbb2015 is running..."
  echo "Please monitor $result/controller.out for progress"
  echo "^^^ does this look right?"
  echo "Sit tight.  This normally takes about 2 hours."
  echo "If you want to check the progress, tail the files that the benchmark mentioned in the output above."

  wait $CTRL_PID
  echo
  echo "Controller has stopped"

  echo "SPECjbb2015 has finished"
  echo
  
  cd ..

  mkdir $result/gc
  mv $result/gc.log* $result/gc

  # Print GC Log
  if [ $PARSE_GC == 1 ]
  then
   parse_gc
  fi

  grep "RUN RESULT" $result/controller.out | tee -a $result/twitter-systeminfo
  $JAVA -version 2>> $result/twitter-systeminfo 
  numactl -H >> $result/twitter-systeminfo
  cpupower frequency-info >> $result/twitter-systeminfo
  uname -a >> $result/twitter-systeminfo
  set >> $result/twitter-systeminfo
  dmidecode >> $result/twitter-systeminfo
done

exit 0
