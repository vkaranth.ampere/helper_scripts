#!/bin/bash
# Copyright (c) 2020-2021 Ampere Computing.  All rights reserved.

# Comp 0

CORES=`nproc --all`
WORKERS=$CORES
SATURATE_THRDS=64
MEMORY=16

show_help(){
    echo "    -w : forkjoin workers        usage: -w workers"
    echo "    -m : xms/xmn memory in gb    usage: -m memory"
    echo "    -s : saturate threads        usage: -s threads"
}

parse() {
    while [ $# -gt 0 ]; do
        case "${1-}" in
	    -h )
                show_help
		exit
		;;
	    -w )
		WORKERS=${2-}
		shift
		;;
	    -m )
	        MEMORY=${2-}
		shift
		;;
	    -s )
	        SATURATE_THRDS=${2-}
		shift
		;;
        esac
	shift
    done
}

run() {

    GC_THRDS=$WORKERS
    XMN=$(( (MEMORY * 94) / 100 ))
    echo "GC=$GC_THRDS xmn=$XMN"

numactl --cpunodebind=0 --membind=0 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms"$MEMORY"g -Xmx"$MEMORY"g -Xmn"$XMN"g -XX:SurvivorRatio=39 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads="$GC_THRDS" -XX:AllocatePrefetchDistance=512 -XX:AllocatePrefetchLines=4 -XX:InlineSmallCode=2k -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:ObjectAlignmentInBytes=32 -Dspecjbb.customerDriver.threads=64 -Dspecjbb.customerDriver.threads.service=64 -Dspecjbb.customerDriver.threads.probe=64 -Dspecjbb.customerDriver.threads.saturate="$SATURATE_THRDS" -Dspecjbb.forkjoin.workers="$WORKERS" -Dspecjbb.forkjoin.workers.Tier1="$WORKERS" -Dspecjbb.forkjoin.workers.Tier2=1 -Dspecjbb.forkjoin.workers.Tier3=16 -Dspecjbb.controller.validation.skip=true -XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -Dspecjbb.controller.type=HBIR_RT -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m COMPOSITE 2>&1 | tee jbb_runlog0_controller.log &
if [ "x$(basename $(pwd))" = "xjbb_test" ]; then
  echo "Not to wait for completion if no jbb_test directory."
else
  echo "Wait for the completion."
  wpid=$(ps -eaf | grep -v grep | grep -v numactl | grep -E "COMPOSITE|MULTICONTROLLER" | awk '{print $2;}' | tail -n 1)
  if [[ ${wpid} =~ ^[0-9]+$  ]]; then
    tail --pid=${wpid} -f /dev/null -s 10
  fi
  echo "Wait 30s for cooling down."
  sleep 30
fi
sleep 10;./check.sh
}

parse "$@"
echo "workers=$WORKERS mem=$MEMORY saturate_threads=$SATURATE_THRDS"
run 
