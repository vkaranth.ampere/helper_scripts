#!/bin/bash
#
#set -x
tune(){
    cpupower frequency-set -g performance
    tuned-adm profile throughput-performance
    echo 400000000 > /proc/sys/kernel/sched_min_granularity_ns
    echo 40000 > /proc/sys/kernel/sched_wakeup_granularity_ns
    echo 40000 > /proc/sys/kernel/sched_migration_cost_ns
    echo 128 > /proc/sys/kernel/sched_nr_migrate
    echo 950000 > /proc/sys/kernel/sched_rt_runtime_us
    echo 400000 > /proc/sys/kernel/sched_latency_ns
    echo 10000 > /proc/sys/vm/dirty_expire_centisecs
    echo 1500 > /proc/sys/vm/dirty_writeback_centisecs
    echo 40 > /proc/sys/vm/dirty_ratio
    echo 10 > /proc/sys/vm/dirty_background_ratio
    echo 10 > /proc/sys/vm/swappiness
    echo 0 > /proc/sys/kernel/numa_balancing
    echo always > /sys/kernel/mm/transparent_hugepage/defrag
    echo always > /sys/kernel/mm/transparent_hugepage/enabled
}

CORES=`nproc --all`
WORKERS=$CORES

# Number of successive runs
NUM_OF_RUNS=3

SATURATE_THRDS=64
MEMORY=16
TAG="default"
TUNE=""
JAVAVER="jdk1.8.0_341"
#jdk1.8.0_361
#jdk-17.0.4.1
PRESET_MODE=0
PRESET_ARGS="500;600000"
STATS=0
COMPRESSED_OOPS=0
JARFILE="specjbb2015.jar"

show_help(){
    echo "    -n : num runs                usage: -n number of runs"
    echo "    -v : jar filename            usage: -v jar version"
    echo "    -c : enable compressed oops  usage: -c enables compressed oops"
    echo "    -k : tune kernel param       usage: -k 1"
    echo "    -t : result tag              usage: -t tagname"
    echo "    -j : JDK version             usage: -j version"
    echo "    -p : preset mode             usage: -p fixedRT;duration"
    echo "    -s : stats                   usage: -s enables stat collection with mpstat"
 
}

parse() {
    while [ $# -gt 0 ]; do
        case "${1-}" in
            -h )
                show_help
                exit
                ;;
            -n )
                NUM_OF_RUNS=${2-}
                shift
                ;;
            -s )
               STATS=1
               ;;
            -p )
                PRESET_MODE=1
                PRESET_ARGS=${2-}
                shift
                ;;
            -m )
                MEMORY=${2-}
                shift
                ;;
            -s )
                SATURATE_THRDS=${2-}
                shift
                ;;
            -j )
                JAVAVER=${2-}
                shift
                ;;
            -v )
		JARFILE=${2-}
                shift
                ;;
            -c )
               COMPRESSED_OOPS=1
               ;;
            -k )
               TUNE=${2-}
               shift
               ;;
            -t )
                TAG=${2-}
                shift
                ;;
        esac
        shift
    done
}

parse "$@"

if [ ! -z $TUNE ] ;then
   tune
fi 

pkill java
###############################################################################
## Sample script for running SPECjbb2015 in Composite mode.
##
## This sample script demonstrates launching the Controller, TxInjector and
## Backend in a single JVM.
###############################################################################

# Launch command: java [options] -jar specjbb2015.jar [argument] [value] ...

# Benchmark options (-Dproperty=value to override the default and property file value)
# Please add -Dspecjbb.controller.host=$CTRL_IP (this host IP) and -Dspecjbb.time.server=true
# when launching Composite mode in virtual environment with Time Server located on the native host.
# AMD Used the following - some are non-compliant
SPEC_MODE="HBIR_RT"
EXTRA_ARGS=
if [ $PRESET_MODE -eq 1 ]; then
    SPEC_MODE="PRESET"
    IFS=';' read -r -a array <<< "$PRESET_ARGS"
    PRESET_IR="${array[0]}"
    PRESET_DUR="${array[1]}"
    EXTRA_ARGS="-Dspecjbb.controller.preset.ir=${PRESET_IR} -Dspecjbb.controller.preset.duration=${PRESET_DUR}"
fi
if [ $COMPRESSED_OOPS -eq 1 ]; then
    EXTRA_ARGS="${EXTRA_ARGS} -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -XX:ObjectAlignmentInBytes=32"
fi
SPEC_OPTS="-Dspecjbb.comm.connect.timeouts.connect=700000 -Dspecjbb.comm.connect.timeouts.read=700000 -Dspecjbb.comm.connect.timeouts.write=700000 -Dspecjbb.customerDriver.threads.probe=80 -Dspecjbb.forkjoin.workers.Tier1=32 -Dspecjbb.forkjoin.workers.Tier2=16 -Dspecjbb.forkjoin.workers.Tier3=8 -Dspecjbb.heartbeat.period=100000 -Dspecjbb.heartbeat.threshold=1000000 -Dspecjbb.controller.type=${SPEC_MODE} ${EXTRA_ARGS}"

echo "spec options: $SPEC_OPTS"

# Java options for Composite JVM

JAVA_OPTS="-server -XX:+UseParallelGC -XX:+AlwaysPreTouch -XX:+UseLargePages -Xms112g -Xmx112g -Xmn102g -XX:MaxTenuringThreshold=1 -XX:+ScavengeBeforeFullGC -XX:-UseAdaptiveSizePolicy  -XX:ParallelGCThreads=16 -XX:-UseBiasedLocking -XX:SurvivorRatio=30 -XX:TargetSurvivorRatio=95 -XX:+UseNUMA -XX:+UseTransparentHugePages"

# Optional arguments for the Composite mode (-l <num>, -p <file>, -skipReport, etc.)
MODE_ARGS=""


###############################################################################
# This benchmark requires a JDK7 compliant Java VM.  If such a JVM is not on
# your path already you must set the JAVA environment variable to point to
# where the 'java' executable can be found.
###############################################################################

JAVA=/home/opc/JDK/"$JAVAVER"/bin/java

#COLLECT="/opt/oracle/developerstudio12.6/bin/collect -j on -F on -o AMD_tuning_exp.er"
COLLECT=

which $JAVA > /dev/null 2>&1
if [ $? -ne 0 ]; then
    echo "ERROR: Could not find a 'java' executable. Please set the JAVA environment variable or update the PATH."
    exit 1
else
    echo "JAVA version $JAVA found"
fi

#vmstat -t 10 > vmstat.out &
#mpstat 30 > mpstat.out &


#bash ~/server_stats.sh

for ((n=1; $n<=$NUM_OF_RUNS; n=$n+1)); do

  # Create result directory
  timestamp=$(date '+%y-%m-%d_%H%M%S')
  result=./${TAG}_"$timestamp"
  mkdir $result

  # Copy current config to the result directory
  cp -r config $result
        cp $0 $result
  cd $result

  echo "Run $n: $timestamp"
  echo "Launching SPECjbb2015 in Composite mode..."
  echo

  if [ $STATS -eq 1 ]; then
      echo " stats collection enabled"
      mpstat 1 > mpstat.out &
  fi
  echo "Start Composite JVM"
  echo "numactl --cpunodebind=0 --localalloc $COLLECT $JAVA $JAVA_OPTS  -Xloggc:be_gc_composite.log $SPEC_OPTS -jar /home/opc/benchmarks/specjbb2015/${JARFILE} -ikv -m COMPOSITE $MODE_ARGS"
  numactl --cpunodebind=0 --localalloc $COLLECT $JAVA $JAVA_OPTS  -Xloggc:be_gc_composite.log $SPEC_OPTS -jar /home/opc/benchmarks/specjbb2015/${JARFILE} -ikv -m COMPOSITE $MODE_ARGS 2>composite.log > composite.out &

    COMPOSITE_PID=$!
    echo "Composite JVM PID = $COMPOSITE_PID"

  sleep 3

  echo
  echo "SPECjbb2015 is running..."
  echo "Please monitor $PWD/composite.out for progress"

  sleep 60
  pmap -p $COMPOSITE_PID >  $PWD/pmap$COMPOSITE_PID.out
  pkill pmap

  wait $COMPOSITE_PID
  if [ $STATS -eq 1 ]; then
     pkill mpstat
  fi 
  echo
  echo "Composite JVM has stopped"

  echo "SPECjbb2015 has finished"
  echo

 cd ..

done

#pkill vmstat
#mv vmstat.out $result
cp nohup.out $result
#pkill mpstat
#mv mpstat.out $result
#for i in top sar vmstat mpstat iostat pidstat free numastat ipcs pmap mem_collect.sh
#do
#sudo pkill $i
#done

#mv ~/server_stats $result

exit 0
