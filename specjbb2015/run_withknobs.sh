#!/bin/bash
# Copyright (c) 2020-2021 Ampere Computing.  All rights reserved.

# set the result file
OUTPUT="output.txt"

#change these knobs 
#===============

#default values

## TIER1_WORKER=80
## TIER2_WORKER=16
## TIER3_WORKER=16
## MAP_POOLSIZE=80
## FORKJOIN_WORKER=80
## THRD_SATURATE=96
## 
## XMS=31
## XMX=31
## SURVIVOR_RATIO=27
## TARGET_SURVIVOR_RATIO=95
## 
## ALLOC_PREFETCH_LINE=4
## ALLOC_PREFETCH_DIS=512
## INLINE_CODE=2048
## TYPE_PROFILE_WIDTH=4
## 
## GCTHREADS=80
## GCTHREADS_FE=2
## 

TIER1_WORKER=80
TIER2_WORKER=16
TIER3_WORKER=16
MAP_POOLSIZE=80
FORKJOIN_WORKER=80
THRD_SATURATE=96

XMS=31
XMX=31
SURVIVOR_RATIO=27
TARGET_SURVIVOR_RATIO=95

ALLOC_PREFETCH_LINE=4
ALLOC_PREFETCH_DIS=512
INLINE_CODE=2048
TYPE_PROFILE_WIDTH=4

GCTHREADS=80
GCTHREADS_FE=2
#==================
XMN=$(( XMS - 2 ))

RES="res"
mkdir -p $RES

function run_specjbb() {
# CTRL 0
numactl --cpunodebind=0-3 --interleave=0-3 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms${XMS}g -Xmx${XMX}g -Xmn${XMN}g -XX:SurvivorRatio=4 -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:CICompilerCount=2 -XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE} -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_ctrl0.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.maxir.maxFailedPoints=1 -Dspecjbb.customerDriver.threads=64 -Dspecjbb.customerDriver.threads.probe=64 -Dspecjbb.customerDriver.threads.saturate=${THRD_SATURATE} -Dspecjbb.customerDriver.threads.service=64 -Dspecjbb.forkjoin.workers=${FORKJOIN_WORKER} -Dspecjbb.forkjoin.workers.Tier1=${TIER1_WORKER} -Dspecjbb.forkjoin.workers.Tier2=${TIER2_WORKER} -Dspecjbb.forkjoin.workers.Tier3=${TIER3_WORKER} -Dspecjbb.mapreducer.pool.size=${MAP_POOLSIZE} -Dspecjbb.controller.handshake.timeout=800000 -Dspecjbb.heartbeat.threshold=600000 -Dspecjbb.group.count=4 -Dspecjbb.controller.type=HBIR_RT -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m MULTICONTROLLER 2>&1 | tee $RES/jbb_runlog0_controller.log &
# Group 0.0 BE 0
numactl --cpunodebind=0 --membind=0 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms32256m -Xmx32256m -Xmn29696m -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads=20 -XX:AllocatePrefetchDistance=${ALLOC_PREFETCH_DIS} -XX:AllocatePrefetchLines=${ALLOC_PREFETCH_LINE} -XX:InlineSmallCode=${INLINE_CODE} -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+UseNUMA -XX:+UseNUMAInterleaving -XX:-UseAdaptiveNUMAChunkSizing -XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_be_0.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m BACKEND -G=Group0 -J=beJVM0 > $RES/jbb_runlog0_backend_0.log 2>&1 &
# Group 0.1 BE 1
numactl --cpunodebind=1 --membind=1 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms32256m -Xmx32256m -Xmn29696m -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads=20 -XX:AllocatePrefetchDistance=${ALLOC_PREFETCH_DIS} -XX:AllocatePrefetchLines=${ALLOC_PREFETCH_LINE} -XX:InlineSmallCode=${INLINE_CODE} -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+UseNUMA -XX:+UseNUMAInterleaving -XX:-UseAdaptiveNUMAChunkSizing -XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_be_1.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m BACKEND -G=Group1 -J=beJVM1 > $RES/jbb_runlog0_backend_1.log 2>&1 &
# Group 0.2 BE 2
numactl --cpunodebind=2 --membind=2 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms32256m -Xmx32256m -Xmn29696m -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads=20 -XX:AllocatePrefetchDistance=${ALLOC_PREFETCH_DIS} -XX:AllocatePrefetchLines=${ALLOC_PREFETCH_LINE} -XX:InlineSmallCode=${INLINE_CODE} -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+UseNUMA -XX:+UseNUMAInterleaving -XX:-UseAdaptiveNUMAChunkSizing -XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_be_2.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m BACKEND -G=Group2 -J=beJVM2 > $RES/jbb_runlog0_backend_2.log 2>&1 &
# Group 0.3 BE 3
numactl --cpunodebind=3 --membind=3 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms32256m -Xmx32256m -Xmn29696m -XX:SurvivorRatio=27 -XX:TargetSurvivorRatio=95 -XX:+UseCompressedOops -XX:+UseCompressedClassPointers -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:+UseParallelGC -XX:ParallelGCThreads=20 -XX:AllocatePrefetchDistance=${ALLOC_PREFETCH_DIS} -XX:AllocatePrefetchLines=${ALLOC_PREFETCH_LINE} -XX:InlineSmallCode=${INLINE_CODE} -XX:TypeProfileWidth=4 -XX:SoftwarePrefetchHintDistance=128 -XX:+UseNUMA -XX:+UseNUMAInterleaving -XX:-UseAdaptiveNUMAChunkSizing -XX:+AvoidUnalignedAccesses -XX:-UseBlockZeroing -XX:+UseSIMDForMemoryOps -XX:-UseSIMDForArrayEquals -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_be_3.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m BACKEND -G=Group3 -J=beJVM3 > $RES/jbb_runlog0_backend_3.log 2>&1 &
# Group 0.0 TXI 0
numactl --cpunodebind=0 --membind=0 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms3g -Xmx3g -Xmn2g -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:CICompilerCount=2 -XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE} -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_txi_0.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m TXINJECTOR -G=Group0 -J=txiJVM0 > $RES/jbb_runlog0_txinjector_0.log 2>&1 &
# Group 0.1 TXI 1
numactl --cpunodebind=1 --membind=1 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms3g -Xmx3g -Xmn2g -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:CICompilerCount=2 -XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE} -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_txi_1.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m TXINJECTOR -G=Group1 -J=txiJVM1 > $RES/jbb_runlog0_txinjector_1.log 2>&1 &
# Group 0.2 TXI 2
numactl --cpunodebind=2 --membind=2 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms3g -Xmx3g -Xmn2g -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:CICompilerCount=2 -XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE} -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_txi_2.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m TXINJECTOR -G=Group2 -J=txiJVM2 > $RES/jbb_runlog0_txinjector_2.log 2>&1 &
# Group 0.3 TXI 3
numactl --cpunodebind=3 --membind=3 jdk-15+36-r4a588d89-2020-12-10-01-56-32-b3477-R8-64K/bin/java -Xms3g -Xmx3g -Xmn2g -server -XX:+AlwaysPreTouch -XX:-UseAdaptiveSizePolicy -XX:-UseCountedLoopSafepoints -XX:-UsePerfData -XX:+PrintFlagsFinal -XX:+UseTransparentHugePages -XX:CICompilerCount=2 -XX:+UseParallelGC -XX:ParallelGCThreads=${GCTHREADS_FE} -Dspecjbb.comm.connect.timeouts.connect=600000 -Dspecjbb.comm.connect.timeouts.read=600000 -Dspecjbb.comm.connect.timeouts.write=600000 -Dspecjbb.comm.connect.selector.runner.count=5 -Xlog:gc,gc+heap+coops,gc+start,gc+phases,gc+heap=debug,gc+metaspace,gc+cpu=info,gc+promotion=info:file=gc_txi_3.log:time,uptime,level,tags:filecount=0 -Dspecjbb.controller.port=24000 -jar specjbb2015.jar -m TXINJECTOR -G=Group3 -J=txiJVM3 > $RES/jbb_runlog0_txinjector_3.log 2>&1 &
if [ "x$(basename $(pwd))" = "xjbb_test" ]; then
  echo "Not to wait for completion if no jbb_test directory."
else
  echo "Wait for the completion."
  wpid=$(ps -eaf | grep -v grep | grep -v numactl | grep -E "COMPOSITE|MULTICONTROLLER" | awk '{print $2;}' | tail -n 1)
  if [[ ${wpid} =~ ^[0-9]+$  ]]; then
    tail --pid=${wpid} -f /dev/null -s 10
  fi
  echo "Wait 30s for cooling down."
  sleep 30
fi
sleep 10;./check.sh

}


# run iterations of specjbb
for i in {1..5}; do
  run_specjbb
  cat result/*/*/logs/*ter.out | grep max-jOPS | sort | tail -1 | cut -d',' -f3 >> $OUTPUT
done
