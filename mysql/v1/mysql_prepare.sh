#!/bin/bash
## xiaohua.zeng@amperecomputing.com

source ./config.rc
workload=oltp_point_select

#1.initialize db
#2.start mysqld
#3.create sbtest dabatase
#4.test data prepare
#5.clean the test data

if [ ! -d $DATA_DIR ] ; then
    mkdir -p $DATA_DIR

fi
export LD_LIBRARY_PATH=$MYSQL_INSTALL_DIR/lib
export LUA_PATH=$SYSBENCH_INSTALL_DIR/share/sysbench/?.lua

echo never > /sys/kernel/mm/transparent_hugepage/enabled

umount $DATA_DIR
#mount -t tmpfs -o size=100G,mpol=bind:0 tmpfs $DATA_DIR
mount -t tmpfs -o size=100G tmpfs $DATA_DIR

ninst=$INSTANCE

for i in $(seq 1 $ninst) ; do

    # prepare the conf file for each instance
    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port   
    mysql_datadir=$DATA_DIR/data$mysql_port/dbs$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp

    rm -rf $mysql_basedir

    mkdir -p $mysql_basedir $mysql_datadir $mysql_tmpdir

    echo "Creating a mysqld instance at ${mysql_basedir}..."
  
    # prepare the conf file for instance
    sed -e 's/%PORT%/'$mysql_port'/g' \
            -e 's,%DATA_ROOT%,'$DATA_DIR',g' \
            mysql_conf > \
            $mysql_basedir/my.cnf

    # initialize instance
    $NUMACTL $MYSQL_INSTALL_DIR/bin/mysqld --defaults-file=$mysql_basedir/my.cnf --skip-grant-tables --user=root --initialize >> $mysql_basedir/mysql_install_db.log 2>&1 &

    wait

done


for i in $(seq 1 $ninst) ; do

    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port
    mysql_datadir=$DATA_DIR/data$mysql_port/dbs$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    
    # start the server instance
    echo "Starting mysqld instances on port $mysql_port..."
    LD_PRELOAD=$JEMALLOC $NUMACTL $MYSQL_INSTALL_DIR/bin/mysqld --defaults-file=$mysql_basedir/my.cnf --skip-grant-tables --user=root >>$mysql_basedir/mysqld_startup.log &

done

# wait for the server stable
sleep 20

job_ids=""
for i in $(seq 1 $ninst) ; do

    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port
    mysql_datadir=$DATA_DIR/data$mysql_port/dbs$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    mysql_sock=$mysql_tmpdir/mysql.sock
    
    echo "Generate data for instance $mysql_port for benchmark..."

    $MYSQL_INSTALL_DIR/bin/mysql -S $mysql_sock -u root -e "create database sbtest" 
    $SYSBENCH_INSTALL_DIR/bin/sysbench $SYSBENCH_INSTALL_DIR/share/sysbench/${workload}.lua --table-size=$TABLESIZ --tables=$TABLECNT --threads=$TABLECNT --mysql-user=root --mysql-port=$mysql_port  --mysql-socket=$mysql_sock prepare >> $mysql_basedir/data_prepare.log 2>&1 &
    job_ids="$job_ids $!"
done

echo "Waiting for the data generating..."
wait $job_ids

for i in $(seq 1 $ninst) ; do
    mysql_port=$(($PORT + i))
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    mysql_sock=$mysql_tmpdir/mysql.sock

    echo "Shuting down the instance at port $mysql_port..."
    $MYSQL_INSTALL_DIR/bin/mysqladmin --socket=$mysql_sock shutdown 

done

echo "Data Generation Done."


