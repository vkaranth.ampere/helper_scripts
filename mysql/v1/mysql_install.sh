#!/bin/bash

source ./config.rc

##sudo yum install gcc gcc-c++  openssl-devel ncurses-devel rpcgen automake libtool -y
##problems with cmake
sudo yum install gcc gcc-c++ cmake openssl-devel ncurses-devel rpcgen automake libtool -y
curl -LOJ https://github.com/Kitware/CMake/releases/download/v3.20.3/cmake-3.20.3-linux-aarch64.tar.gz
tar -xzf cmake-3.20.3-linux-aarch64.tar.gz

## install mysql
echo "Installing mysql @ $MYSQL_INSTALL_DIR"
wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.20.tar.gz
tar zxvf mysql-8.0.20.tar.gz
mkdir BOOST
mkdir build; cd build
#
##download boost
wget https://boostorg.jfrog.io/artifactory/main/release/1.70.0/source/boost_1_70_0.tar.gz
tar xvf boost_1_70_0.tar.gz
#
##source /opt/rh/gcc-toolset-10/enable
OPT_FLAGS="-g -O3 -fno-omit-frame-pointer -march=native"
#../cmake-3.20.3-linux-aarch64/bin/cmake ../mysql-8.0.20 -DDOWNLOAD_BOOST=1 -DWITH_BOOST=../BOOST -DCMAKE_CXX_FLAGS="$OPT_FLAGS" -DCMAKE_C_FLAGS="$OPT_FLAGS" -DWITH_UNIT_TESTS=OFF -DCMAKE_INSTALL_PREFIX=$MYSQL_INSTALL_DIR
../cmake-3.20.3-linux-aarch64/bin/cmake ../mysql-8.0.20  -DWITH_BOOST=../boost_1_70_0 -DCMAKE_CXX_FLAGS="$OPT_FLAGS" -DCMAKE_C_FLAGS="$OPT_FLAGS" -DWITH_UNIT_TESTS=OFF -DCMAKE_INSTALL_PREFIX=$MYSQL_INSTALL_DIR
make -j`nproc`
sudo make install
cd ..

# install sysbench
echo "Installing sysbench @ $SYSBENCH_INSTALL_DIR"
wget https://github.com/akopytov/sysbench/archive/1.0.20.tar.gz
tar zxvf 1.0.20.tar.gz
cd sysbench-1.0.20
./autogen.sh
./configure --with-mysql-includes=${MYSQL_INSTALL_DIR}/include/ --with-mysql-libs=${MYSQL_INSTALL_DIR}/lib/ --prefix=$SYSBENCH_INSTALL_DIR
make
sudo make install
#
#

