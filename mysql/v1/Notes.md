### Install mysql and sysbench
```
./mysql_install.sh
```
MySQL version: 8.0.20  
sysbench version: 1.0.20  
Default installation location: /usr/local/mysql-8.0.20-lse  

### Prepare the data
```
./mysql_prepare.sh
```
Default data location: /run/user/mysql (mounted as tmpfs bind to node 0)   
It may fail if run on 1 numa node system, then please remove "mpol=bind:0" from the script.
```
mount -t tmpfs -o size=100G,mpol=bind:0 tmpfs $DATA_DIR
```

### Start the server and run sysbench
```
export LD_LIBRARY_PATH=/usr/local/mysql-8.0.20-lse/lib
numactl -C 10-11 -m 0 /usr/local/mysql-8.0.20-lse/bin/mysqld --defaults-file=/run/user/mysql/data3001/my.cnf --skip-grant-tables --user=root &
sleep 10
numactl -C 20-21 -m 0 /usr/local/sysbench-1.0.20/bin/sysbench /usr/local/sysbench-1.0.20/share/sysbench/oltp_point_select.lua --table-size=10000000 --tables=8 --mysql-user=root --mysql-db=sbtest --db-driver=mysql --mysql-ignore-errors=1062 --threads=16 --events=0 --time=600 --report-interval=10 --mysql-port=3001 --mysql-socket=/run/user/mysql/data3001/tmp/mysql.sock run
```
mysqld is affinity to core 10,11;  
sysbench is affinity to core 20,21  

### Stop the server and clean the data
```
/usr/local/mysql-8.0.20-lse/bin/mysqladmin --socket=/run/user/mysql/data3001/tmp/mysql.sock shutdown

umount /run/user/mysql
```

### Capture the perf events
