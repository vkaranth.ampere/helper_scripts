#source ./config.rc
export MYSQL_INSTALL_DIR=/usr/local/bin/mysql-8.0.20-lse
export SYSBENCH_INSTALL_DIR=/usr/local/bin/sysbench-1.0.20
export DATA_DIR=/data
export PORT=3002

export LD_LIBRARY_PATH=${MYSQL_INSTALL_DIR}/lib:/usr/local/bin/gcc-10.2.0/lib64/




group1() {
cores=$1 
sudo perf stat -C $cores -e '{cycles,cycles:k,instructions,instructions:k,stalled-cycles-frontend,stalled-cycles-backend,cs,faults}',\
'{L1-icache-loads,L1-icache-load-misses,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/}',\
'{armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\}',\
'{armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/}' \
-x, -o stat1.csv -I 1000 &
}


group2() {
cores=$1
sudo perf stat -C $cores -e '{armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/}',\
'{armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/}',\
'{l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/}',\
'{armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/}' \
-x, -o stat2.csv -I 1000 &
}

RESULT="mysql_result.log"
MIGRATE=0
DURATION=300
NODEID=3
NUMACTL=""

start_server(){
if [ $NODEID -eq 0 ] ; then
	NUMACTL="numactl -C 0-1 -m 0 "
elif [ $NODEID -eq 1 ] ; then
	NUMACTL="numactl -C 80-81 -m 1 "
fi

#numactl -C 0-1 -m 0 ${MYSQL_INSTALL_DIR}/bin/mysqld --defaults-file=${MYSQL_INSTALL_DIR}/run/my.cnf --initialize
$NUMACTL ${MYSQL_INSTALL_DIR}/bin/mysqld --defaults-file=${DATA_DIR}/data$PORT/my.cnf --skip-grant-tables --user=root &
sleep 5

if [ $MIGRATE -eq 1 ] ; then
     echo "mysqld pid"
     MPID=`pgrep mysqld`	
     echo $MPID
     if [ $NODEID -eq 0 ] ; then
     	migratepages $MPID 1 0      
     elif [ $NODEID -eq 1 ] ; then
        migratepages $MPID 0 1 
     fi 
fi
sleep 10
}

run_benchmark(){
if [ $NODEID -eq 0 ] ; then
	NUMACTL="numactl -C 40-41 -m 0 "
elif [ $NODEID -eq 1 ] ; then
	NUMACTL="numactl -C 120-121 -m 1 "
fi
$NUMACTL ${SYSBENCH_INSTALL_DIR}/bin/sysbench ${SYSBENCH_INSTALL_DIR}/share/sysbench/oltp_point_select.lua --table-size=10000000 --tables=8 --mysql-user=root --mysql-db=sbtest --db-driver=mysql --mysql-ignore-errors=1062 --threads=16 --events=0 --time=${DURATION} --report-interval=10 --mysql-port=$PORT --mysql-socket=${DATA_DIR}/data$PORT/tmp/mysql.sock run > ${RESULT} 2>&1 &

pid=$!

sleep 60
#group1 0-1
#sleep 30
#sudo pkill -x perf
#
#sleep 5
#group2 0-1
#sleep 30
#sudo pkill -x perf
echo "waiting to finish"
wait $pid

${MYSQL_INSTALL_DIR}/bin/mysqladmin --socket=${DATA_DIR}/data$PORT/tmp/mysql.sock shutdown
}

parse_args() {
    options=$(getopt -o ht:r:n: -l migrate,node: -- "$@")
#    [ $? -eq 0 ] || {
#        echo "error: Incorrect option provided"
 #       exit 1
  #  }
    eval set -- "$options"
    while true; do
        case "$1" in
        -h)
            echo "$0 [-h] [-r resfile] [-t time] [--migrate]"
            echo "    -r: result filename(default mysql_result.log)"
            echo "    -t: benchmark time(default 300seconds)"
	    echo "    -n|node: node id to run (default no binding)"
            echo "    --migrate: migrate shared libraries(default disabled)"
            exit
            ;;
        -r)
            shift
            export RESULT=$1
            ;;
        -t)
            shift
            export DURATION=$1
            ;;
	-n|node)
            shift
	    export NODEID=$1
	    ;;
	--migrate)
	    export MIGRATE=1
	    ;;
	--)
	    shift
	    break
	    ;;
	esac
	shift
    done
}

parse_args $*
start_server
run_benchmark

