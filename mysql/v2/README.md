# mysql_benchmark

The testsuite for mysql benchmark.

## Getting started

### MySQL Server Side
- Install mysql, refer to the mysql_install.sh;
- Install tools  
CentOS:  
```
sudo yum install sysstat numaclt -y
```
Ubuntu:  
```
sudo apt install sysstat numaclt -y

```


### Sysbench client 
CentOS:  
```
sudo yum -y install make automake libtool pkgconfig libaio-devel
sudo yum -y install mariadb-devel openssl-devel
```

Ubuntu:  
```
sudo apt -y install make automake libtool pkg-config libaio-dev
sudo apt -y install libmysqlclient-dev libssl-dev
```

Build:  
```
wget https://github.com/akopytov/sysbench/archive/refs/heads/master.zip
unzip master
cd sysbench-master

./autogen.sh
./configure
make -j`nproc`
sudo make install
```


