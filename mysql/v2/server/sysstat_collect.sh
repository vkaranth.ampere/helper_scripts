#!/bin/bash
workload=$1
config=$2
interval=$3

source ./config.rc

if [ "$interval" == "" ]; then interval=1; fi

DATASTAMP=`date +%Y_%m_%d_%H%M%S`
RESULT_DIR=$HOMEDIR/mysql_stats/mysql_${workload}

if [ ! -d ${RESULT_DIR} ]
then
    mkdir -p ${RESULT_DIR}
    touch ${RESULT_DIR}/summary.txt
fi

stat_ids=""

# Collect stats
iostat -xmt $interval >$RESULT_DIR/iostat_${config}.out &
stat_ids="$stat_ids $!"
vmstat -t $interval >$RESULT_DIR/vmstat_${config}.out & 
stat_ids="$stat_ids $!"
sar -n DEV $interval >$RESULT_DIR/sar_${config}.out &
stat_ids="$stat_ids $!"

echo $stat_ids > stat_ids

exit 0
