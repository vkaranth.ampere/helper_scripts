#!/bin/bash

source ./config.rc

if [ ! -d $DATA_DIR ] ; then
    mkdir -p $DATA_DIR

fi

#echo never > /sys/kernel/mm/transparent_hugepage/enabled

ninst=$INSTANCE

for i in $(seq 1 $ninst) ; do

    # prepare the conf file for each instance
    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port   
    mysql_datadir=$DATA_DIR/data$mysql_port/dbs$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp

    rm -rf $mysql_basedir

    mkdir -p $mysql_basedir $mysql_datadir $mysql_tmpdir

    echo "Creating a mysqld instance at ${mysql_basedir}..."
  
    # prepare the conf file for instance
    sed -e 's/%PORT%/'$mysql_port'/g' \
            -e 's,%DATA_ROOT%,'$DATA_DIR',g' \
            mysql_conf.tmp > \
            $mysql_basedir/my.cnf

    # initialize instance
    $NUMACTL_SRV $MYSQL_INSTALL_DIR/bin/mysqld --defaults-file=$mysql_basedir/my.cnf --skip-grant-tables --user=root --initialize >> $mysql_basedir/mysql_install_db.log 2>&1 &

    wait

    echo "Starting mysqld instances on port $mysql_port..."
    LD_PRELOAD=$JEMALLOC $NUMACTL_SRV $MYSQL_INSTALL_DIR/bin/mysqld --defaults-file=$mysql_basedir/my.cnf --user=root &
done

sleep 10

for i in $(seq 1 $ninst) ; do

    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port
    mysql_datadir=$DATA_DIR/data$mysql_port/dbs$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp

    password_line=$(grep "A temporary password is generated" ${mysql_tmpdir}/error.log)
    OLD_PASSWORD=${password_line##*: } 

    $MYSQL_CLIENT --socket=${mysql_tmpdir}/mysql.sock --connect-expired-password -uroot -p"${OLD_PASSWORD}" -e "ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';"
    $MYSQL_CLIENT --socket=${mysql_tmpdir}/mysql.sock -uroot -p'123456' -e "GRANT ALL ON *.* TO 'root'@'%';"
    $MYSQL_CLIENT --socket=${mysql_tmpdir}/mysql.sock -uroot -p'123456' -e "create database sbtest"
    $MYSQL_CLIENT --socket=${mysql_tmpdir}/mysql.sock -uroot -p'123456' -e "CREATE USER 'sbtest'@'%' IDENTIFIED WITH mysql_native_password BY '123456';"
    $MYSQL_CLIENT --socket=${mysql_tmpdir}/mysql.sock -uroot -p'123456' -e "GRANT ALL ON *.* TO 'sbtest'@'%';"

done

for i in $(seq 1 $ninst) ; do
    mysql_port=$(($PORT + i))
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    mysql_sock=$mysql_tmpdir/mysql.sock

    echo "Shuting down the instance at port $mysql_port..."
    $MYSQL_INSTALL_DIR/bin/mysqladmin --socket=$mysql_sock -uroot -p123456 shutdown

done

