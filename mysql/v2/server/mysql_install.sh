#!/bin/bash

CURDIR=`pwd`
source ./config.rc
sudo yum install yum-utils -y
sudo yum-config-manager --enable PowerTools
sudo yum groupinstall "Development Tools" -y
sudo yum install gcc gcc-c++ cmake openssl-devel ncurses-devel rpcgen libtirpc-devel automake libtool libarchive ncurses-devel libtirpc-devel rpcgen -y
#wget https://vault.centos.org/centos/8/PowerTools/aarch64/os/Packages/rpcgen-1.3.1-4.el8.aarch64.rpm
#sudo rpm -ivh 

sudo yum install gcc-toolset-10-gcc gcc-toolset-10-gcc-c++ gcc-toolset-10-binutils gcc-toolset-10-annobin -y
sudo yum install numactl sysstat -y

# Ubuntu
# sudo apt install pkg-config gcc g++  cmake libssl-dev libncurses-dev libntirpc-dev libudev-dev bison sysstat gcc-10 g++-10 -y

# install mysql
rm -rf build 
rm -rf BOOST
rm -rf mysql-8.0.27.tar.gz
rm -rf mysql-8.0.27

echo "Installing mysql @ $MYSQL_INSTALL_DIR"
wget https://cdn.mysql.com/archives/mysql-8.0/mysql-8.0.27.tar.gz
tar zxvf mysql-8.0.27.tar.gz
#mkdir BOOST; cd BOOST; wget https://sourceforge.net/projects/boost/files/boost/1.73.0/boost_1_73_0.tar.gz ; cd ${CURDIR}
mkdir build; cd build

#OPT_FLAGS="-g -O3 -fno-omit-frame-pointer -march=armv8.2-a -DNDEBUG"
OPT_FLAGS="-g -O3 -fno-omit-frame-pointer -march=native"

#compile with default GCC
#CC=/usr/bin/gcc-10 CXX=/usr/bin/g++-10 cmake ../mysql-8.0.27 -DDOWNLOAD_BOOST=1 -DWITH_BOOST=../BOOST -DCMAKE_C_FLAGS_RELWITHDEBINFO="$OPT_FLAGS" -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="$OPT_FLAGS" -DCMAKE_BUILD_TYPE=RelWithDebInfo  -DWITH_UNIT_TESTS=OFF -DCMAKE_INSTALL_PREFIX=$MYSQL_INSTALL_DIR

#building wiht gcc-10
source /opt/rh/gcc-toolset-10/enable
cmake ../mysql-8.0.27 -DDOWNLOAD_BOOST=1 -DWITH_BOOST=../BOOST -DCMAKE_C_FLAGS="$OPT_FLAGS" -DCMAKE_CXX_FLAGS="$OPT_FLAGS" -DCMAKE_BUILD_TYPE=Release  -DWITH_UNIT_TESTS=OFF -DCMAKE_INSTALL_PREFIX=/home/vkaranth/installs/mysql-8.0.27

make -j`nproc`
sudo make install
cd ..
#rm -rf build mysql-8.0.27.tar.gz mysql-8.0.27 BOOST

echo "MySQL Installation Done."
