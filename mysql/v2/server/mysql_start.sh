#!/bin/bash

source ./config.rc

#echo never > /sys/kernel/mm/transparent_hugepage/enabled

ninst=$INSTANCE

for i in $(seq 1 $ninst) ; do

    mysql_port=$(($PORT + i))
    mysql_basedir=$DATA_DIR/data$mysql_port
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    
    # start the server instance
    echo "Starting mysqld instances on port $mysql_port..."
    LD_PRELOAD=$JEMALLOC $NUMACTL_SRV $MYSQL_INSTALL_DIR/bin/mysqld --defaults-file=$mysql_basedir/my.cnf  --user=root >>$mysql_basedir/mysqld_startup.log &

    sleep 5

done

