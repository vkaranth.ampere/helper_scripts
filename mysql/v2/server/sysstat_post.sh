#!/bin/bash

###### CentOS

# iostat
# Device            r/s     w/s     rMB/s     wMB/s   rrqm/s   wrqm/s  %rrqm  %wrqm r_await w_await aqu-sz rareq-sz wareq-sz  svctm  %util
# vdb            736.00    0.00      5.75      0.00     0.00     0.00   0.00   0.00    0.33    0.00   0.00     8.00     0.00   1.36 100.00

# vmstat
# r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st                 CST
# 2  0      0 3064960 508608 814336    0    0   736     0 41195 43164  8  2 89  0  0 2022-04-27 12:27:10

# sar
# 12:20:14 PM     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s   %ifutil
# 12:20:15 PM      eth0   7200.00   7201.00    632.77   1849.04      0.00      0.00      0.00      0.00


####### Ubuntu
# iostat
# Device            r/s     rMB/s   rrqm/s  %rrqm r_await rareq-sz     w/s     wMB/s   wrqm/s  %wrqm w_await wareq-sz     d/s     dMB/s   drqm/s  %drqm d_await dareq-sz  aqu-sz  %util
# sdb           1356.13     16.05     0.03   0.00    1.57    12.12 2787.94     54.69    84.96   2.96    0.49    20.09    0.00      0.00     0.00   0.00    0.00     0.00    3.50  63.11

# vmstat
# r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st                 UTC
# 8  6      0 3300728 1088836 3682172    0    0     0 219468 42244 67115  3  2 83 12  0 2022-04-17 16:08:22

# sar
# 16:08:05        IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s   %ifutil
# 16:08:06         eth0   1135.00   1808.00    113.73   2195.75      0.00      0.00      0.00      0.02

source ./config.rc

CentOS=0
if command -v yum &> /dev/null
then
    CentOS=1
fi


RESULT_DIR=$1

printf "%-10s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s\n" "thread" "CPU_Usr(%)" "CPU_Sys(%)" "CPU_IO(%)" "CPU_Util(%)" "IO_R(MB/s)" "IO_W(MB/s)" "Net_Rx(KB/s" "Net_Tx(KB/s)"> $RESULT_DIR/summary.txt

for thread_num in ${THREAD_ARRY[@]};
do
    LOG=$RESULT_DIR/vmstat_thread${thread_num}.out
    total_vmstat=`awk '{print $15}' $LOG |sed -n '/^$/d;/^[0-9]/p'|wc -l`
    CPU_AVG_USR=`awk '{print $13}' $LOG |sed -n '/^$/d;/^[0-9]/p' | tail -n $(($total_vmstat - 1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`
    CPU_AVG_SYS=`awk '{print $14}' $LOG |sed -n '/^$/d;/^[0-9]/p' | tail -n $(($total_vmstat - 1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`
    CPU_AVG_UTIL=`awk '{print $15}' $LOG |sed -n '/^$/d;/^[0-9]/p' | tail -n $(($total_vmstat - 1)) |awk '{sum += $1;count++} END {printf "%4.2f",100 - sum / count}'`
    CPU_AVG_IO=`awk '{print $16}' $LOG |sed -n '/^$/d;/^[0-9]/p' | tail -n $(($total_vmstat - 1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`

    if [ $CentOS -eq 1 ] ; then
        LOG=$RESULT_DIR/iostat_thread${thread_num}.out
        total_iostat=`cat $LOG | grep $DISK | wc -l`
        IO_R=`cat $LOG | grep $DISK | awk '{print $4}' | tail -n $((total_iostat -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`
        IO_W=`cat $LOG | grep $DISK | awk '{print $5}' | tail -n $((total_iostat -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`
	
        LOG=$RESULT_DIR/sar_thread${thread_num}.out
        total_sar=`cat $LOG | grep $NET | wc -l`
        rx_unit=`cat $LOG | grep IFACE | head -n1 | awk '{print $6}'`
        tx_unit=`cat $LOG | grep IFACE | head -n1 | awk '{print $7}'`
        NET_RX=`cat $LOG | grep $NET | awk '{print $6}' | tail -n $((total_sar -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}' `
        NET_TX=`cat $LOG | grep $NET | awk '{print $7}' | tail -n $((total_sar -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}' `
    else
        LOG=$RESULT_DIR/iostat_thread${thread_num}.out
        total_iostat=`cat $LOG | grep $DISK | wc -l`
        IO_R=`cat $LOG | grep $DISK | awk '{print $3}' | tail -n $((total_iostat -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`
        IO_W=`cat $LOG | grep $DISK | awk '{print $9}' | tail -n $((total_iostat -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}'`

        LOG=$RESULT_DIR/sar_thread${thread_num}.out
        total_sar=`cat $LOG | grep $NET | wc -l`
        rx_unit=`cat $LOG | grep IFACE | head -n1 | awk '{print $5}'`
        tx_unit=`cat $LOG | grep IFACE | head -n1 | awk '{print $6}'`
        NET_RX=`cat $LOG | grep $NET | awk '{print $5}' | tail -n $((total_sar -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}' `
        NET_TX=`cat $LOG | grep $NET | awk '{print $6}' | tail -n $((total_sar -1)) |awk '{sum += $1;count++} END {printf "%4.2f",sum / count}' `
    fi
    printf "%-10s %-20s %-20s %-20s %-20s %-20s %-20s %-20s %-20s\n" "$thread_num" "$CPU_AVG_USR" "$CPU_AVG_SYS" "$CPU_AVG_IO" "$CPU_AVG_UTIL" "$IO_R" "$IO_W" "$NET_RX" "$NET_TX" >>$RESULT_DIR/summary.txt

done

