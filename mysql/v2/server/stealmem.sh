sysctl vm.drop_caches=3;
numastat -m -z; free;
mkdir -p /mnt/numa-fill;
mount -t tmpfs none /mnt/numa-fill;
mount -o remount,mpol=bind:0 /mnt/numa-fill;
#create 51GB/48GiB file
dd if=/dev/zero of=/mnt/numa-fill/f1.txt count=80000000;
# run 1 more than you have blocks of 48GiB. Last one will fail, but fill as much as it can
sysctl vm.drop_caches=3;
for i in `seq 1 2 3 4`; do echo $i; cp /mnt/numa-fill/f1.txt /mnt/numa-fill/fff$i.txt; done;
numastat -m -z; 
free;
