#!/usr/bin/env bash

size=170

echo "Creating ${size}GB tmpfs ramdisk at /mnt/ramdisk..."

umount /mnt/ramdisk
mkdir -p /mnt/ramdisk
mount -t tmpfs -o size=${size}g tmpfs /mnt/ramdisk
mkdir -p /mnt/ramdisk/mysql
ln -fs /mnt/ramdisk/mysql

echo "Done!"

