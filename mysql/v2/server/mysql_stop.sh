#!/bin/bash

source ./config.rc

ninst=$INSTANCE

for i in $(seq 1 $ninst) ; do
    mysql_port=$(($PORT + i))
    mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
    mysql_sock=$mysql_tmpdir/mysql.sock

    echo "Shuting down the instance at port $mysql_port..."
    $MYSQL_INSTALL_DIR/bin/mysqladmin --socket=$mysql_sock -uroot -p$PASSWD shutdown 

done


