#Configuration setup in config.rc
#Change the ip address, path of installation, thread range, NIC interface and disk

#Install mysql for ubuntu
#MySQL will be installed in the path provided by config.rc. Can be compiled with default gcc or gcc-10. Edit the script accordingly
./mysql_install.sh 

#Setup the disk(s) for the database
#Format the disk, replace the sdb1 with the name of the disk
sudo parted /dev/sdb --script mklabel gpt mkpart xfspart xfs 0% 100%
sudo mkfs.xfs /dev/sdb1
sudo partprobe /dev/sdb1

sudo mkdir -p /mnt/disks/mysql_data
#mount the disk, change the name of the device before mounting
sudo mount /dev/sdb /mnt/disks/mysql_data

#Apply tuning setting. Edit NIC interface names as per the VM. Also the HW Queue settings for the experiments 
sudo ./mysql_tune.sh

#Init mysql
sudo ./mysql_init.sh

#Start mysql
sudo ./mysql_start.sh

