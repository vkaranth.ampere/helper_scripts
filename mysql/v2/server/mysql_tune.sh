
echo 1 > /proc/sys/vm/swappiness

#test with never (was default)
echo always > /sys/kernel/mm/transparent_hugepage/enabled

INTERFACE=enp1s0f1
SERVER_CPUS_IRQ=0-15
#change the HW Q settings
sudo ethtool -L $INTERFACE combined 16

#disable irq balance
sudo systemctl stop irqbalance


systemctl stop irqbalance.service
tuned-adm profile throughput-performance
echo always > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag
echo 0 > /sys/kernel/mm/transparent_hugepage/khugepaged/defrag
echo 4096 > /proc/sys/net/core/somaxconn
echo 4096 > /proc/sys/net/ipv4/tcp_max_syn_backlog
echo 50 > /proc/sys/net/core/busy_read
echo 50 > /proc/sys/net/core/busy_poll
echo 8000 > /proc/sys/net/core/netdev_budget_usecs
echo 1200 > /proc/sys/net/core/netdev_budget
echo 5000 > /proc/sys/kernel/sched_wakeup_granularity_ns

for interface in ${INTERFACE1}  ; do
    ethtool -C ${interface} adaptive-rx off adaptive-tx off ;:
    ethtool -K ${interface} lro on ;:
    ethtool -K ${interface} gro on
    ethtool -C ${interface} rx-usecs 1 tx-usecs 1 rx-frames 256 tx-frames 256;:
done


./set_irq_affinity_cpulist_mellanox.sh ${SERVER_CPUS_IRQ} ${INTERFACE} > /dev/null
