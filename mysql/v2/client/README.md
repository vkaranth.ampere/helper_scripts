#Configure paths and host ip in config.rc

#Install sysbench, default location is ~/installs/sysbench
./sysbench_install.sh

#prepare the database
./mysql_prepare.sh

#run the benchmark. Set the benchmarks and sysbench thread list in config.rc
./mysql_bench.sh
