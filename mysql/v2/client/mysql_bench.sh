#!/bin/bash

source ./config.rc

DATASTAMP=`date +%Y_%m_%d_%H%M%S`

ninst=$INSTANCE

export LUA_PATH=$SYSBENCH_INSTALL_DIR/share/sysbench/?.lua
export LD_LIBRARY_PATH=$MYSQL_INSTALL_DIR/lib

ulimit -Hn 1048576
ulimit -n 1048576
ulimit -s unlimited

#TODO: capture Ctlr-C and clean up the monitoring on the server
clean_up() {
    echo "Test Terminated."
    ## kill sysbench
    kill -9 `pidof sysbench`
    ## kill monitor
    if [ "${SERVER_STATS}" == "yes" ]
    then
        $REMOTE_SSH "${STATS_DIR}/sysstat_kill.sh"
    fi
}

trap clean_up SIGINT

if [ "${SERVER_STATS}" == "yes" ]
then
    echo "Test the remote server access:"
    $REMOTE_SSH ls 
fi

#warm the buffer bool using oltp_point_select
if [ "$PREWARM" == "yes" ]
then
    job_ids=""
    for i in $(seq 1 $ninst) ; do

        mysql_port=$(($PORT + i))
        mysql_tmpdir=$DATA_DIR/data$mysql_port/tmp
        mysql_sock=$mysql_tmpdir/mysql.sock

        echo "Warming for instance $mysql_port warming..."

        thread_num=64
        $NUMACTL_CLI ${SYSBENCH_INSTALL_DIR}/bin/sysbench $SYSBENCH_INSTALL_DIR/share/sysbench/oltp_point_select.lua --table-size=$TABLESIZ --tables=$TABLECNT --mysql-user=sbtest --mysql-password=123456 --mysql-host=${MYSQL_HOST}  --mysql-db=sbtest  --db-driver=mysql --mysql-ignore-errors=1062 --threads=$thread_num --events=0 --time=$WARMTIME --report-interval=10 --mysql-port=$mysql_port --skip_trx run >> prewarm.log 2>&1 &
        job_ids="$job_ids $!"
    done

    echo "Waiting for the data warming..."
    wait $job_ids
fi

for workload in ${WORKLOADS[@]};
do

    RESULT_DIR=`pwd`/${workload}_ins${ninst}_${tag}_${DATASTAMP}
    if [ ! -d ${RESULT_DIR} ]
    then
        mkdir ${RESULT_DIR}
        touch ${RESULT_DIR}/summary.txt
        cp ./config.rc ${RESULT_DIR}
    fi
    
    printf "*** MySQL Instance = $ninst ***\n" >>$RESULT_DIR/summary.txt
    #printf "%-10s %-10s %-10s %-10s %-20s %-20s %-20s %-20s %-20s %-20s %-20s\n" "thread" "TPS" "QPS" "resp_time" "resp_time_95%" "CPU_Usr" "CPU_Sys" "CPU_IO" "CPU_Util" "IO_R(MB/s)" "IO_W(MB/s)">>$RESULT_DIR/summary.txt
    printf "%-10s %-10s %-10s %-10s %-20s\n" "thread" "TPS" "QPS" "resp_time" "resp_time_95%">>$RESULT_DIR/summary.txt

    
    for thread_num in ${THREAD_ARRY[@]};
    do

        echo " ==> Test with $thread_num client thread"
        job_ids=""
        for inst in $(seq 1 $ninst) ; do

            mysql_port=$(($PORT + inst))

            LOG="mysql_${workload}_thread${thread_num}_instance$inst.log"
            
            echo "----------------thread=$thread_num---------------------" >> $RESULT_DIR/$LOG

            # Run Workloads
            echo "  --Start test for instance $inst..."
            $NUMACTL_CLI ${SYSBENCH_INSTALL_DIR}/bin/sysbench $SYSBENCH_INSTALL_DIR/share/sysbench/${workload}.lua --table-size=$TABLESIZ --tables=$TABLECNT --mysql-user=sbtest --mysql-password=123456 --mysql-host=${MYSQL_HOST}  --mysql-db=sbtest  --db-driver=mysql --mysql-ignore-errors=1062,1213 --threads=$thread_num --events=0 --time=$RUNTIME --warmup-time=100 --report-interval=10 --mysql-port=$mysql_port --skip_trx  run >> $RESULT_DIR/$LOG 2>&1 &
            job_ids="$job_ids $!"
        done
        
        # Start the status monitor on the server      
        if [ "${SERVER_STATS}" == "yes" ]
        then
           $REMOTE_SSH  "${STATS_DIR}/sysstat_collect.sh ${workload}_${tag}_${DATASTAMP} thread${thread_num} 1" &
        fi

        wait $job_ids
    

        echo " <== All jod done for $thread_num client thread."

        # Stop the status monitor       
        if [ "${SERVER_STATS}" == "yes" ]
        then
            $REMOTE_SSH  "${STATS_DIR}/sysstat_kill.sh"
        fi
    
        TPS=0
        QPS=0
        RESP_TIME=0
        RESP_TIME_95=0
        for inst in $(seq 1 $ninst) ; do

            LOG="mysql_${workload}_thread${thread_num}_instance$inst.log"

            echo "----------------END thread=$thread_num---------------------" >>  $RESULT_DIR/$LOG
            
            ltps=`sed -n '/thread='"$thread_num"'[^0-9]/,/END thread='"$thread_num"'[^0-9]/p' $RESULT_DIR/$LOG | grep "transactions" | awk -F ':' '{print $2}'|awk -F '(' '{printf "%4.2f",$2}'`
            TPS=`echo $TPS $ltps | awk '{printf "%4.2f", $1+$2}'`
           
            lqps=`sed -n '/thread='"$thread_num"'[^0-9]/,/END thread='"$thread_num"'[^0-9]/p' $RESULT_DIR/$LOG | grep "queries:" | awk -F ':' '{print $2}'|awk -F '(' '{printf "%4.2f",$2}'`
            QPS=`echo $QPS $lqps | awk '{printf "%4.2f", $1+$2}'`


            lrt=`sed -n '/thread='"$thread_num"'[^0-9]/,/END thread='"$thread_num"'[^0-9]/p' $RESULT_DIR/$LOG | sed -n '/Latency (ms)/,/Threads fairness/p'| grep "avg" | awk -F ':' '{printf "%4.2f",$2}'`
            RESP_TIME=`echo $RESP_TIME $lrt | awk '{printf "%4.2f", $1+$2}'`

            lrt95=`sed -n '/thread='"$thread_num"'[^0-9]/,/END thread='"$thread_num"'[^0-9]/p' $RESULT_DIR/$LOG | sed -n '/Latency (ms)/,/Threads fairness/p'| grep "95th percentile" | awk -F ':' '{printf "%4.2f",$2}'`
            RESP_TIME_95=`echo $RESP_TIME_95 $lrt95 | awk '{printf "%4.2f", $1+$2}'`

        done

        # Average the response time
        RESP_TIME=`echo $RESP_TIME $ninst | awk '{printf "%4.2f", $1/$2}'`
        RESP_TIME_95=`echo $RESP_TIME_95 $ninst | awk '{printf "%4.2f", $1/$2}'`

        #printf "%-10s %-10s %-10s %-10s %-20s %-20s %-20s %-20s %-20s %-20s %-20s\n" "$thread_num" "$TPS" "$QPS" "$RESP_TIME" "$RESP_TIME_95" "$CPU_AVG_USR" "$CPU_AVG_SYS" "$CPU_AVG_IO" "$CPU_AVG_UTIL" "$IO_R" "$IO_W" >>$RESULT_DIR/summary.txt
        printf "%-10s %-10s %-10s %-10s %-20s\n" "$thread_num" "$TPS" "$QPS" "$RESP_TIME" "$RESP_TIME_95" >>$RESULT_DIR/summary.txt

        # Sleep for clean up
        sleep 5
    done
    
    echo -e "\n" >>$RESULT_DIR/summary.txt
done 

