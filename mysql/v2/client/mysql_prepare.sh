#!/bin/bash

source ./config.rc
workload=oltp_point_select

export LUA_PATH=$SYSBENCH_INSTALL_DIR/share/sysbench/?.lua
export LD_LIBRARY_PATH=$MYSQL_INSTALL_DIR/lib
ninst=$INSTANCE

job_ids=""
for i in $(seq 1 $ninst) ; do

    mysql_port=$(($PORT + i))
    
    echo "Generate data for instance $mysql_port for benchmark..."

    if [ "${TABLE_COMPRESSION}" == "on" ]
    then
        $SYSBENCH_INSTALL_DIR/bin/sysbench $SYSBENCH_INSTALL_DIR/share/sysbench/${workload}.lua --table-size=$TABLESIZ --tables=$TABLECNT --threads=$TABLECNT --mysql-user=sbtest --mysql-password=123456 --mysql-host=${MYSQL_HOST}  --mysql-port=$mysql_port --create_table_options="ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8"  prepare >> ./data_prepare.log 2>&1 &
    else
        $SYSBENCH_INSTALL_DIR/bin/sysbench $SYSBENCH_INSTALL_DIR/share/sysbench/${workload}.lua --table-size=$TABLESIZ --tables=$TABLECNT --threads=$TABLECNT --mysql-user=sbtest --mysql-password=123456 --mysql-host=${MYSQL_HOST}  --mysql-port=$mysql_port  prepare >> ./data_prepare.log 2>&1 &
    fi

    job_ids="$job_ids $!"
done

echo "Waiting for the data generating..."
wait $job_ids

echo "Data Generation Done."
