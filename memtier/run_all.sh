#!/bin/bash
#threads=("4" "8" "16" "32")
dataset=(1024 10240 102400 1024000)
tpt=""
for i in ${dataset[*]}; do
	sudo bash ./run-benchmark.sh -y redis -t 32 -d $i -n 32 --perf "all" > redis_$i.txt
	sleep 5
	tpt+=(`cat redis_$i.txt | grep 'total throughput' | awk '{print $3}'`)
	echo "done $i threads redis"
done

for i in ${tpt[*]}; do
	echo $i
done
