#!/bin/bash

GCC_BIN=/usr/local/bin/gcc
MEMCACHE_VER=1.6.9
MEMCACHE_INSTALL=/home/vkaranth/installs/memcached
if [ ! -f ${MEMCACHE_VER}.tar.gz ]; then
	wget https://github.com/memcached/memcached/archive/${MEMCACHE_VER}.tar.gz 
fi
sudo yum install -y autoconf automake pcre-dev libevent-dev zlib-dev linux-headers openssl-dev
tar xzf ${MEMCACHE_VER}.tar.gz &&\
    cd memcached-${MEMCACHE_VER} && \
    ./autogen.sh && \
    CC=${GCC_BIN} CFLAGS="-mcpu=neoverse-n1" ./configure --enable-arm-crc32 --prefix=${MEMCACHE_INSTALL} && \
    make clean && \
    make -j && \
    make install
cd ..

REDIS_VER=6.0.11
REDIS_INSTALL=/home/vkaranth/installs/redis
if [ ! -f ${REDIS_VER}.tar.gz ]; then
	wget https://github.com/redis/redis/archive/${REDIS_VER}.tar.gz .
fi
tar xzf ${REDIS_VER}.tar.gz && \
    cd redis-${REDIS_VER} && \
    make distclean && \
    CC=${GCC_BIN} CFLAGS="-mcpu=neoverse-n1" make -j && \
    make PREFIX=${REDIS_INSTALL} install
cd ..

MEMTIER_VER=1.3.0
MEMTIER_INSTALL=/home/vkaranth/installs/memtier
if [ ! -f ${MEMTIER_VER}.tar.gz ]; then
	wget https://github.com/RedisLabs/memtier_benchmark/archive/${MEMTIER_VER}.tar.gz .
fi
tar xzf ${MEMTIER_VER}.tar.gz && \
    cd memtier_benchmark-${MEMTIER_VER} && \
    autoreconf -ivf && \
    CC=${GCC_BIN} CFLAGS="-mcpu=neoverse-n1" ./configure --prefix=${MEMTIER_INSTALL} && \
     make clean && \
     make -j && \
     make install
cd ..
