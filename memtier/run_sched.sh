#!/bin/bash

#defaults
#kernel.sched_latency_ns = 24000000
#kernel.sched_wakeup_granularity_ns = 4000000

wakeup_ns=("11222222" "11444444" "11666666" "11888888" "12000000" "13000000")
tpt=""

tag="redis_1t_1k"
cmd="-y redis -t 1 -n 1 -d 1024"

run() {
for i in ${wakeup_ns[*]}; do
	sudo sysctl -w kernel.sched_wakeup_granularity_ns=$i

	#sudo bash ./run-benchmark.sh -y redis -t $i -n $i --perf > redis_$i.txt
	echo "cmd  $cmd"
	sudo bash ./run-benchmark.sh ${cmd} -g "$tag"_"$i" > logs/"$tag"_"$i".txt
	sleep 5
	tpt+=(`cat logs/"$tag"_"$i".txt | grep 'total throughput' | awk '{print $3}'`)
	echo "done $i setting for redis"
done

for i in "${!tpt[@]}"; do
#for i in ${tpt[*]}; do
        if [ $i -eq 0 ]; then
		echo "wakeup_ns    tpt"
		continue
	fi
	echo "${wakeup_ns[$((i-1))]}    ${tpt[$i]}"
done

echo "resetting to defaults"
sudo sysctl -w kernel.sched_wakeup_granularity_ns=4000000
}

tag="redis_1t_1k"
cmd="-y redis -t 1 -n 1 -d 1024"
run

tag="redis_1t_1k_iso"
cmd="-y redis -t 1 -n 1 -d 1024 -i"
run

tag="redis_1t_1m"
cmd="-y redis -t 1 -n 1 -d 1048576"
run 

tag="redis_1t_1m_iso"
cmd="-y redis -t 1 -n 1 -d 1048576 -i"
run

