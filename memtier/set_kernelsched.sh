#!/bin/bash
set_ksched_param(){
echo "set kernelsched values"
sysctl -w kernel.sched_latency_ns=24000000
sysctl -w kernel.sched_migration_cost_ns=500000
#experimental
#sysctl -w kernel.sched_min_granularity_ns=200000000
#centos default
sysctl -w kernel.sched_min_granularity_ns=10000000
sysctl -w kernel.sched_nr_migrate=32
sysctl -w kernel.sched_rr_timeslice_ms=100
sysctl -w kernel.sched_rt_period_us=1000000
sysctl -w kernel.sched_rt_runtime_us=950000
sysctl -w kernel.sched_schedstats=0
sysctl -w kernel.sched_time_avg_ms=1000
sysctl -w kernel.sched_tunable_scaling=1
#experimental
#sysctl -w kernel.sched_wakeup_granularity_ns=30000000
sysctl -w kernel.sched_wakeup_granularity_ns=15000000

sysctl -w kernel.numa_balancing=0
echo never > /sys/kernel/mm/transparent_hugepage/enabled
sync;
}

reset_ksched_param(){
sysctl -w kernel.sched_latency_ns=24000000			
sysctl -w kernel.sched_migration_cost_ns=500000			
sysctl -w kernel.sched_min_granularity_ns=3000000			
sysctl -w kernel.sched_nr_migrate=32			
sysctl -w kernel.sched_rr_timeslice_ms=100			
sysctl -w kernel.sched_rt_period_us=1000000			
sysctl -w kernel.sched_rt_runtime_us=950000			
sysctl -w kernel.sched_schedstats=0			
sysctl -w kernel.sched_tunable_scaling=1			
sysctl -w kernel.sched_wakeup_granularity_ns=4000000			
sync;
}


disable_netfilter(){
	echo "disabling netfiltering config params"
	sysctl -w net.netfilter.nf_conntrack_acct=0
	sysctl -w net.netfilter.nf_conntrack_checksum=0
	sysctl -w net.netfilter.nf_conntrack_events=0
        sysctl -w net.netfilter.nf_conntrack_expect_max=1
	sysctl -w net.netfilter.nf_conntrack_timestamp=0
	sync;
}

tune_ivp4(){
sysctl -w net.ipv4.conf.all.accept_redirects=1
sysctl -w net.ipv4.conf.all.forwarding=0
sysctl -w net.ipv4.conf.default.forwarding=0
sysctl -w net.ipv4.ip_forward=0
}


set_ksched_param
disable_netfilter
tune_ivp4
