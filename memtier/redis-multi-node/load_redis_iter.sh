
#!/bin/bash

d=32
c=2
t=1
p=16
resdir=redis_numactl

load() {
portstart=6394
WAIT=""
for i in {0..15}; do
	port=$(( portstart - i ))
	#memtier_benchmark --protocol=redis --server 10.0.2.8 --port="$p" -c 1 -t 4 --pipeline 100 --data-size=32 --ratio='1:9' --key-pattern='R:R' --test-time=30 --out-file=out"$i".txt &
	memtier_benchmark --server 10.0.2.8 --port "$port" --protocol redis --clients "$c" --threads "$t" --ratio 1:9 --data-size 32 --pipeline "$p" --key-minimum 1 --key-maximum 10000000 --key-pattern R:R --run-count 1 --test-time 30 --out-file "$resdir"/"$1"/memtier_"$i".txt --print-percentile 50,90,95,99,99.9 --random-data &
	WAIT="$WAIT""$! "
done
wait $WAIT
}


for p in 50 32 64 100; do
    for c in 1 2 4; do
        for t in 1 2 4; do
            if [[ "$c" -eq 4  && "$t" -eq 4 ]]; then
                   continue
            fi
            res=redis_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir -p "$resdir"/"$res"

            #run the workload
	    load "$res"

        done
    done
done
