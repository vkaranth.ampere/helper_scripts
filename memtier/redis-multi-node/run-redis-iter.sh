#!/bin/bash

out=redis_azure.out
for p in 25 32 50 64; do
	for c in 1 2; do
		for t in 1 2 4; do
			printf 'redis_c%d_t%d_p%d\t' "$c" "$t" "$p" >> $out
			./run-redis.sh --load --server 10.0.0.33 --pipeline $p --clients $c --threads $t --numruns 7 2>&1 | tee /dev/stderr | tail -1 >> $out
		done
	done
done
