#!/bin/bash
port=6379
for i in {0..15}; do
	p=$(( port + i ))
	numactl -C "$i" /opt/pkb/redis/src/redis-server --port "$p" --protected-mode no --ignore-warnings ARM64-COW-BUG --save  --io-threads 4 --maxmemory-policy noeviction & 
done
