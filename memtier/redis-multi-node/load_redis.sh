#!/bin/bash
port=6379
for i in {0..15}; do
	p=$(( port + i ))
	memtier_benchmark --protocol=redis --server localhost --port="$p" -c 1 -t 1 --pipeline 100 --data-size=32 --key-minimum=1 --key-maximum=10000000 --ratio=1:0 --requests=allkeys
done
