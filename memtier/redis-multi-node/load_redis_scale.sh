
#!/bin/bash

d=32
c=2
t=2
p=100
resdir=temp

load() {
	portstart=6394
	WAIT=""
	count=$2
	i=0
	while [ $i -lt $count ]; do
		port=$(( portstart - i ))
		out="$1"/memtier_"$i".txt
		memtier_benchmark --server 10.0.2.8 --port "$port" --protocol redis --clients "$c" --threads "$t" --ratio 1:9 --data-size 32 --pipeline "$p" --key-minimum 1 --key-maximum 10000000 --key-pattern R:R --run-count 1 --test-time 30 --out-file "$out" --print-percentile 50,90,95,99,99.9 --random-data --hide-histogram &
		WAIT="$WAIT""$! "
		echo $i $port $out
		i=$((i+1))
	done
	wait $WAIT
}


for i in {1..16}; do
	resdir=scale_2/redis_"$i"
	mkdir -p "$resdir"
	load "$resdir" $i
	sleep 2
done


