#!/bin/sh
set -eu

MEMCACHE_INSTALL=/home/vkaranth/installs/memcached
MEMTIER_INSTALL=/home/vkaranth/installs/memtier
REDIS_INSTALL=/home/vkaranth/installs/redis

CUR_DIR=`pwd`
PTYPE=""

usage() {
  cat <<EOF
Usage: run-benchmark.sh [-h] [-v] [-y benchmark_type] [-t threads] [-p type] [-d size] [-i] [--duration time] 

Run the memtier benchmark

Available options:

-h, --help       Print this help and exit
-v, --verbose    Print script debug info
-y, --type       Type of benchmark to run (memcached, redis). Optionally a semicolon separated list.
-t, --threads    Amount of threads to use for the benchmark (default: 1). Optionally a semicolon separated list.
-p, --perf       enable perf stat collection core, all
-n, --numactl    use numactl for server and loadgenerator
-d, --dataset    Dataset size to be used. default is 1024
-i, --isolate    run load generator on separate cores
--duration       test duration
-g, --tag        add a tag 
EOF
  exit
}

parse_params() {
  RESULT="Total ops/sec"
  TYPE="memcached"
  ITERATIONS=1
  THREADS=1
  NUMACTL=""
  experimental=0
  PTYPE="none"
  DATASET="1024"
  ISOLATE=0
  TAG=""
  DURATION=30

  while [ $# -gt 0 ]; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    -i | --isolate) ISOLATE=1 ;;
    -g | --tag)
       TAG="${2-}"
       shift
       ;;
    --duration)
       DURATION="${2-}"
       shift
       ;;
    -p | --perf)
       PTYPE=${2-}
       shift
       ;;
    -d | dataset)
       DATASET=${2-}
       echo ${DATASET}
       shift
       ;;
    -n | --numactl)
      cores=${2-}
      cores=$((cores - 1))
      NUMACTL=" numactl -C 0-$cores -m0 "
      if [[ $experimental = 1 ]];then
	      cores=${2-}
	      hcores=$((cores / 2))
	      c1=$((hcores - 1))
	      c2=$((hcores + 39))
	      NUMACTL=" numactl -C 0-$c1,40-$c2 -m 0 "
      fi
      shift 
      ;;
    -y | --type)
      ARG_TYPE="${2-}"
      shift
      ;;
    -t | --threads)
      ARG_THREADS="${2-}"
      shift
      ;;
    *) ;; # Skip unknown params
    esac
    shift
  done
}

DB_PIDS=""
start_server() {
  if [ "$TYPE" = "memcached" ]; then
    # When benchmarking memcached, use half as many more threads as those being
    # used by the server.
    BENCHMARKTHREADS=$(($THREADS+$THREADS/2))
    BENCHMARKPROCESSES=1
    CONCURRENCYTYPE=threads
    PROTOCOL=memcache_binary
    $NUMACTL ${MEMCACHE_INSTALL}/bin/memcached --port "$(( PORT + 1 ))" -u root -t "$THREADS" &
    DB_PIDS="$!"
  else
    BENCHMARKTHREADS=1 
    BENCHMARKPROCESSES="$THREADS"
    CONCURRENCYTYPE=processes
    PROTOCOL=redis
    for P in $(seq 1 $BENCHMARKPROCESSES); do
      $NUMACTL ${REDIS_INSTALL}/bin/redis-server --port "$(( PORT + P ))" &
      if [ -z $DB_PIDS ]; then
	DB_PIDS="$!"
      else
      	DB_PIDS="$DB_PIDS","$!"
      fi
    done
    sleep 10
  fi
  
  echo "Using $THREADS $CONCURRENCYTYPE for the database"
}

benchmark() {
  BENCHMARK_NAME="memtier_$TYPE"
  PARAMS="$CONCURRENCYTYPE=$THREADS"

  echo "Using $BENCHMARKPROCESSES processes and $BENCHMARKTHREADS threads for the benchmark client"
  for I in $(seq 1 $ITERATIONS); do
    WAIT=""

    if [ $PTYPE = "core" ]; then
        echo "collecting perf on $DB_PIDS or cores"
    	sudo perf stat  -o $CUR_DIR/stat_${TYPE}_${THREADS}_${DATASET}_1p.txt -C 1-${THREADS} -D 1000 -e cycles,cycles:k,instructions,instructions:k,stall_frontend,stall_backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x0163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\,\
armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/,\
armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/,\
armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/,\
l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/,\
armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/,\
armv8_pmuv3_0/event=0x056,name=l2d_cache_wb_victim/,\
armv8_pmuv3_0/event=0x057,name=l2d_cache_wb_clean/ \
-- sleep 30 &
    elif [ $PTYPE = "all" ]; then
    	sudo perf stat  -o $CUR_DIR/stat_${TYPE}_${THREADS}_${DATASET}_2p.txt -C 1-${THREADS} -D 1000 -e cycles,cycles:k,instructions,instructions:k,stall_frontend,stall_backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x0163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\,\
armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/,\
armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/,\
armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/,\
l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/,\
armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/,\
armv8_pmuv3_0/event=0x056,name=l2d_cache_wb_victim/,\
armv8_pmuv3_0/event=0x057,name=l2d_cache_wb_clean/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=12,name='slc_access_12'/,arm_cmn/hnf_cache_miss,nodeid=12,name='slc_miss_12'/,\
arm_cmn/hnf_mc_reqs,nodeid=12,name='mc_req_12'/,\
dmc620_0/clkdiv2_rdwr/ \
-- sleep 30 &
    fi
    # override numactl for loadgenerator
    if [ $ISOLATE -eq 1 ]; then
            if [ $BENCHMARKPROCESSES -eq 1 ];then
                    NUMACTL=" numactl -C 1 -m0 "
            else
                    memtier_cores=$(( BENCHMARKPROCESSES + BENCHMARKPROCESSES - 1 ))
                    NUMACTL=" numactl -C $BENCHMARKPROCESSES-$memtier_cores -m0 "

            fi
    fi
    for P in $(seq 1 $BENCHMARKPROCESSES); do
      CMD="$NUMACTL ${MEMTIER_INSTALL}/bin/memtier_benchmark -p "$(( PORT + P ))" -P "$PROTOCOL" -t "$BENCHMARKTHREADS" --test-time $DURATION --pipeline=100 --ratio 1:10 -c 25 -x 1 -d "$DATASET" --key-pattern S:S --json-out-file=$I-$P.json"

      { $CMD 2>&1 | tee /dev/stderr | grep Totals | tail -n 1 | awk '{print $2}' | cut -d . -f 1 > "$P" ; } &
      #{ numactl -C 1-${THREADS} ${MEMTIER_INSTALL}/bin/memtier_benchmark -p "$(( PORT + P ))" -P "$PROTOCOL" -t "$BENCHMARKTHREADS" --test-time $DURATION --pipeline=100 --ratio 1:10 -c 25 -x 1 --data-size-range=10240-1048576 --key-pattern S:S --json-out-file=$I-$P.json 2>&1 | tee /dev/stderr | grep Totals | tail -n 1 | awk '{print $2}' | cut -d . -f 1 > "$P" ; } &
      WAIT="$WAIT""$! "
    done
    
    #start perf
    #"$CUR_DIR"/collect.sh -n 20 -o "$CUR_DIR"/logs/perf_$TAG -c 0-0 &
    wait $WAIT
    SUM=0
    for P in $(cat $(seq 1 $BENCHMARKPROCESSES)); do
      SUM="$(( SUM + P ))"
    done
    echo "total throughput $SUM"
  done
}

cleanup() {
  killall redis-server memcached || true
  sleep 5 # Wait for the servers to actually finish
}

parse_params "$@"

cd /tmp
PORT=7000

echo "arg type $ARG_TYPE"
for TYPE in ${ARG_TYPE//;/ } ; do
  echo "Running for type ${TYPE}"
  for THREADS in ${ARG_THREADS//;/ }; do
    echo "Running for threads ${THREADS} using numactl: ${NUMACTL}"
    start_server
    benchmark
    cleanup
  done
done
