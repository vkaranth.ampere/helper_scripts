#!/bin/sh

# server side: install and warmup: run-redish.sh --install --warmup 
# client side: run-redis.sh --server <ip> --load --clients <c> --threads <t> --pipeline <p> --load 
set -eu


CUR_DIR=`pwd`

usage() {
  cat <<EOF
Usage: run-memcached.sh [-h] 

Run the memtier benchmark for memcached

Available options:

-h, --help       Print this help and exit
--install        Install (run this on server)
--copies         memcached instances default=1 (server or client side)
--memthreads     memcached threads default=16 (server side)
--warmup         Warmup (run this on server)
--bind           bind memcached process to cores (server side - only when memcached is multi-process)
--server         server ip default=localhost (client side arg)
--load           load remote memcached with memtier client (client side)
--pipeline       pipeline depth default=25 (client side)
--clients        num clients default=1 (client side)
--threads        num threads default=1 (client side)
--numruns        run count, default=1 (client side)
--duration       test duration per iteration, default=30s (client side)
EOF
  exit
}

INSTALL=0
WARMUP=0
COPIES=16
MEMTHREADS=4
PORT=11211
WORKDIR="memcached-memtier"
MEMORY=4096
LOAD_KEYMAX=13500000
KEYMAX=14175000
NUMACTL=0
PIPE=25
CLIENTS=1
THREADS=16
SERVER="localhost"
LOAD=0
NUMRUNS=1
DURATION=30
VERSION="1.6.19"

parse_params() {


  while [ $# -gt 0 ]; do
    case "${1-}" in
    -h | --help) usage ;;
    --install)
       INSTALL=1
       ;;
    --warmup)
       WARMUP=1
       ;;
    --load)
       LOAD=1
       ;;
    --bind)
       NUMACTL=1
       ;;
    --copies)
      COPIES=${2-}
      shift
      ;;
    --memthreads)
      MEMTHREADS=${2-}
      shift
      ;;
    --pipeline)
      PIPE=${2-}
      shift
      ;;
    --clients)
      CLIENTS=${2-}
      shift
      ;;
    --threads)
      THREADS=${2-}
      shift
      ;;
    --server)
      SERVER=${2-}
      shift
      ;;
    --numruns)
      NUMRUNS=${2-}
      shift
      ;;
    --duration)
      DURATION=${2-}
      shift
      ;;
 
    *) ;; # Skip unknown params
    esac
    shift
  done
}


install_memcached(){
   cd "$WORKDIR"
   rm -rf "$VERSION".tar.gz

   #sudo apt install wget build-essential libevent-dev numactl -y
   sudo yum groupinstall "Development Tools" -y
   sudo yum install numactl wget libevent-devel -y
   wget https://www.memcached.org/files/memcached-${VERSION}.tar.gz --no-check-certificate
   tar -xvzf memcached-"$VERSION".tar.gz
   cd memcached-"$VERSION"
   ./configure
   make -j8

   #start
   sudo pkill -f "memcached-memtier/memcached-"$VERSION"/memcached"
   sleep 5
   for i in $(seq 1 $COPIES); do
        p=$(( PORT + i - 1))
	pre=""
	if [ $NUMACTL -eq 1 ]; then
	   c=$((i - 1))
	   s=$((c * MEMTHREADS))
           e=$((s + MEMTHREADS - 1))
	   pre="numactl -C $s-$e"
	fi
        "$pre" ./memcached --port "$p" -u $USER -c 32768 -t $MEMTHREADS -m $MEMORY -l 0.0.0.0 -v &
   done
   cd "$CUR_DIR"
}

install_memtier(){
  cd "$WORKDIR"
  #sudo apt install libevent-dev pkg-config zlib1g-dev libssl-dev autoconf automake libpcre3-dev -y   
  sudo yum groupinstall "Development Tools" -y
  sudo yum install libevent-devel libnsl automake autoconf zlib-devel pcre-devel libmemcached-devel libevent-devel openssl-devel -y

  git clone https://github.com/RedisLabs/memtier_benchmark
  cd memtier_benchmark
  git checkout 793d74dbc09395dfc241342d847730a6197d7c0c
  autoreconf -ivf
  ./configure
  make -j8

  cd "$CUR_DIR"
}

warmup(){
   #pid=$(ps -ax | grep memcached | head -n1 | awk '{print $1}')
   #kill -9 $pid
   sudo pkill -f "memcached-memtier/memcached-1.6.9/memcached"
   sleep 5
   WAIT=""
   for i in $(seq 1 $COPIES); do
        p=$(( PORT + i - 1 ))
	pre=""
	if [ $NUMACTL -eq 1 ]; then
	   c=$((i - 1))
	   s=$((c * MEMTHREADS))
           e=$((s + MEMTHREADS - 1))
	   pre="numactl -C $s-$e"
	fi
	echo $pre " " $p
	#restart memcached
	cmd="${pre} ./${WORKDIR}/memcached-${VERSION}/memcached --port "$p" -u $USER -c 32768 -t $MEMTHREADS -m ${MEMORY} -l 0.0.0.0 -v"
	echo $cmd
	${cmd} &
	sleep 3

	#warmup memcached
	cmd="./${WORKDIR}/memtier_benchmark/memtier_benchmark --protocol=memcache_binary --server localhost --port=${p} -c 1 -t 1 --pipeline 100 --data-size=32 --key-minimum=1 --key-maximum=${LOAD_KEYMAX} --ratio=1:0 --requests=allkeys"
	${cmd} &
      
       WAIT="$WAIT""$! "       
   done

}

run_client(){
   if [ ! -f ./${WORKDIR}/memtier_benchmark/memtier_benchmark ]; then
	   mkdir -p $WORKDIR
	   install_memtier
   fi
   WAIT=""
   resdir="memcached_res/memcached_t${THREADS}_c${CLIENTS}_p${PIPE}"
   mkdir -p ${resdir}
   rm -rf ${resdir}/*

   for i in $(seq 1 $COPIES); do
	   p=$(( PORT + i - 1 ))
	   echo $p 
	   
	   cmd="./${WORKDIR}/memtier_benchmark/memtier_benchmark --server ${SERVER} --port ${p} --protocol memcache_binary --clients ${CLIENTS} --threads ${THREADS} --ratio 1:10 --data-size-list 16:56,64:30,128:12,1024:2 --pipeline ${PIPE} --key-minimum 1 --key-maximum ${KEYMAX} --key-pattern S:S --run-count ${NUMRUNS} --test-time ${DURATION} --out-file ${resdir}/memtier_${i}.txt --print-percentile 50,90,95,99,99.9 --random-data"
           ${cmd} &
           WAIT="$WAIT""$! "
   done
   wait $WAIT
   
   total=0
   templat=temp.lat
   for f in ${resdir}/*; do
       #echo `grep Totals ${f} | awk '{print $2}'`
       tpt="$(grep Totals ${f} | tail -1 | awk '{printf("%10d",$2)}')"
       lat="$(grep Totals ${f} | tail -1 | awk '{printf("%.2f",$9)}')"
       total=$((tpt + total))
       echo "tpt=$tpt latency=$lat"
       echo $lat >> $templat
   done
   p99=$(sort -n $templat | tail -1)
   rm -rf $templat
   echo "req/sec= $total  p99lat= $p99"
}


parse_params "$@"

if [ $INSTALL -eq 1 ]; then
	rm -rf $WORKDIR
	mkdir -p $WORKDIR
	install_memcached
	install_memtier
fi
if [ $WARMUP -eq 1 ]; then
	warmup
	echo "done"
fi
if [ $LOAD -eq 1 ]; then
	run_client
fi
