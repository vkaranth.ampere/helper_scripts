#!/bin/bash

out=memcached_gcp_t2d32.out
for p in 32; do
	for c in 1; do
		for t in 32 ; do
			printf 'memcached_c%d_t%d_p%d\t' "$c" "$t" "$p" >> $out
			./run-memcached.sh --load --server 10.128.0.11 --copies 1  --pipeline $p --clients $c --threads $t --numruns 7 2>&1 | tee /dev/stderr | tail -1 >> $out
		done
	done
done
