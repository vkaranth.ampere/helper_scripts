
#!/bin/bash

d=32
c=1
t=16
p=16
resdir=memcached
port=11211
load() {
	memtier_benchmark --server 10.0.2.8 --port "$port" --protocol memcache_binary --clients "$c" --threads "$t" --ratio 1:9 --data-size=32 --pipeline "$p" --key-minimum 1 --key-maximum 10000000 --key-pattern R:R --run-count 1 --test-time 30 --out-file "$resdir"/"$1"/memtier.txt --print-percentile 50,90,95,99,99.9 --random-data &
        wait $!
}


for p in 50 32 64 100; do
    for c in 1 2 4; do
        for t in 12 16 20 24 32; do
            res=memcache_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir -p "$resdir"/"$res"

            #run the workload
	    load "$res"

        done
    done
done
