#!/bin/bash

SERVER="192.168.10.1"
SUT="rtp-epyc-02-7742"
STAT=stat_remote_defT_irq8.txt
USERNAME=root

echo "check irq setting on server"
ssh -t $USERNAME@$SUT "sudo $HOME/apache/config_apache.sh" 
ssh -t $USERNAME@$SUT "$HOME/apache/start_server.sh"
sleep 5

echo "start benchmarking"
numactl -C 0-7 -m0 ab -c 10 -r -n 100000 http://$SERVER/test.html 1>res.txt &
pid=$!

ssh -f vkaranth@$SUT "echo vkaranth | sudo -S $HOME/apache/start_perf.sh 1"
wait $pid

ssh vkaranth@$SUT 'perfpid=`cat $HOME/apache/pid.txt`; echo vkaranth | sudo -S kill -INT $perfpid'

scp vkaranth@$SUT:/home/vkaranth/apache/stat.txt $STAT

