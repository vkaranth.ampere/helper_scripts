#!/bin/bash

p1=3
p2=75
p3=250
p4=25
p5=400
APACHE="apache"
if [ ! -z "$1" ]; then
	p1=$1
fi

if [ ! -z "$2" ]; then
	p2=$2
fi
if [ ! -z "$3" ]; then
	p3=$3
fi
if [ ! -z "$4" ]; then
	p4=$4
fi
if [ ! -z "$5" ]; then
	p5=$5
fi
if [ ! -z "$6" ]; then
	APACHE=$6
fi
echo $p1
echo $p2
echo $p3
echo $p4
echo $p5
echo "changing config for $APACHE"

sed -i "62s/StartServers.*/StartServers ${p1}/" /usr/local/$APACHE/conf/extra/httpd-mpm.conf
sed -i "63s/MinSpareThreads.*/MinSpareThreads ${p2}/" /usr/local/$APACHE/conf/extra/httpd-mpm.conf
sed -i "64s/MaxSpareThreads.*/MaxSpareThreads ${p3}/" /usr/local/$APACHE/conf/extra/httpd-mpm.conf
sed -i "65s/ThreadsPerChild.*/ThreadsPerChild ${p4}/" /usr/local/$APACHE/conf/extra/httpd-mpm.conf
sed -i "66s/MaxRequestWorkers.*/MaxRequestWorkers ${p5}/" /usr/local/$APACHE/conf/extra/httpd-mpm.conf
