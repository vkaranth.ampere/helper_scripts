#!/bin/bash

SERVER=192.168.100.6
SUT=10.76.235.166
STAT=altra_remote_centos_stat_defT_c1.txt
RES=res.txt
CONNS=1
THRDS=""
echo $3
if [ ! -z $1 ]; then
	STAT=$1_stat.txt
	RES=$1_res.txt
fi
if [ ! -z $2 ]; then
	CONNS=$2
fi
if [ ! -z "$3" ]; then
	THRDS="$3"
fi

ssh -t vkaranth@$SUT "sudo $HOME/apache/config_apache.sh $THRDS" 
ssh -t vkaranth@$SUT "$HOME/apache/start_server.sh"
sleep 5

echo "start benchmarking"
numactl -C 0-7 -m0 ab -c $CONNS -r -n 100000 http://$SERVER/test.html 1>$RES &
pid=$!

ssh -f vkaranth@$SUT "$HOME/apache/start_perf.sh"
wait $pid

ssh vkaranth@$SUT 'perfpid=`cat $HOME/apache/pid.txt`; sudo kill -INT $perfpid'

scp vkaranth@$SUT:/home/vkaranth/apache/stat.txt $STAT
mv $STAT results/remote_client/
mv $RES results/remote_client/

