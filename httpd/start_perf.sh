#!/bin/bash

HTTPD_CORE=1
RES_FILE=res.txt
TEST_HOME=/home/vkaranth/apache
STAT_FILE=$TEST_HOME/stat.txt
EPYC=0

if [ ! -z $1 ] ; then
	EPYC=$1
fi

if [[ $EPYC = 1 ]]; then
		sudo perf stat -C $HTTPD_CORE -e cycles,cycles:k,instructions,instructions:k,stalled-cycles-frontend,stalled-cycles-backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
branch-misses,\
branch-load-misses,\
branch-loads,\
iTLB-loads,\
iTLB-load-misses,\
dTLB-loads,\
dTLB-load-misses,\
ic_cache_inval.l2_invalidating_probe,\
ic_cache_fill_l2,\
ic_cache_fill_sys,\
cpu/event=0x64,umask=0x07,name='l2_cache_req_stat.ic_access_in_l2'/,\
cpu/event=0x64,umask=0x01,name='l2_cache_req_stat.ic_fill_miss'/,\
cpu/event=0x64,umask=0x02,name='l2_cache_req_stat.ic_fill_hit_s'/,\
cpu/event=0x64,umask=0x04,name='l2_cache_req_stat.ic_fill_hit_x'/,\
cpu/event=0x64,umask=0x09,name='l2_cache_req_stat.ic_dc_miss_in_l2'/,\
cpu/event=0x64,umask=0xf6,name='l2_cache_req_stat.ic_dc_hit_in_l2/',\
l2_cache_req_stat.ls_rd_blk_c,\
l2_cache_req_stat.ls_rd_blk_cs,\
l2_cache_req_stat.ls_rd_blk_l_hit_s,\
l2_cache_req_stat.ls_rd_blk_l_hit_x,\
l2_cache_req_stat.ls_rd_blk_x,\
l2_request_g1.cacheable_ic_read,\
l2_request_g1.change_to_x,\
l2_request_g1.l2_hw_pf,\
l2_request_g1.ls_rd_blk_c_s,\
l2_request_g1.rd_blk_l,\
l2_request_g1.rd_blk_x,\
l2_request_g1.group2,\
l2_request_g2.group1,\
cpu/event=0x60,umask=0xf9,name='l2_request_g1.all_no_prefetch/',\
l2_pf_hit_l2,\
l2_pf_miss_l2_hit_l3,\
l2_pf_miss_l2_l3,\
l3_comb_clstr_state.other_l3_miss_typs,\
l3_comb_clstr_state.request_miss,\
l3_lookup_state.all_l3_req_typs,\
l3_request_g1.caching_l3_cache_accesses,\
xi_ccx_sdp_req1.all_l3_miss_req_typs \
-o $STAT_FILE &
else
sudo perf stat -C $HTTPD_CORE -e cycles,cycles:k,instructions,instructions:k,stall_frontend,stall_backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x0163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\,\
armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/,\
armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/,\
armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/,\
l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/,\
armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/,\
armv8_pmuv3_0/event=0x056,name=l2d_cache_wb_victim/,\
armv8_pmuv3_0/event=0x057,name=l2d_cache_wb_clean/ \
-o $STAT_FILE &
echo "perf altra"
fi

#arm_cmn/hnf_slc_sf_cache_access,nodeid=12,name='cache_access_12'/,arm_cmn/hnf_cache_miss,nodeid=12,name='cache_miss_12'/,\
#arm_cmn/hnf_mc_reqs,nodeid=12,name='mc_req_12'/ \
pid=$!
echo "perf pid $pid"
echo $pid > $TEST_HOME/pid.txt
