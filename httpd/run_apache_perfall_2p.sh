#!/bin/bash

sudo pkill httpd
sudo pkill apache2
sleep 5
echo "restart server"
sudo numactl -C 1 -m0 /usr/local/apache/bin/httpd
sleep 5

for i in `pgrep httpd`; do
	migratepages $i 1 0 
        echo "migrated $i"
done

sleep 5

keepalive=0
stat="apache_stat_222140_100k.txt"
if [ ! -z "$1" ] ; then
	stat=$1
fi
if [ ! -z "$2" ]; then
	keepalive=1
fi
echo "$stat"
cmd="numactl -C 41 -m 0 ab -c 1 -n 100000  -r http://0.0.0.0/test.html"
if [ $keepalive -eq 1 ]; then
	cmd="numactl -C 41 -m 0 ab -c 1 -n 100000  -rk http://0.0.0.0/test.html"
fi
echo "$cmd"
sudo perf stat -C 1 -e cycles,cycles:k,instructions,instructions:k,stall_frontend,stall_backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x0163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\,\
armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/,\
armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/,\
armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/,\
l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/,\
armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/,\
remote_access \
 -o $stat \
$cmd
#numactl -C 41 -m 0 ab -c 1 -n 100000  -r http://0.0.0.0/test.html

#echo "keepalive on!!!"
