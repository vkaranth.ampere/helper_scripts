
#sudo perf report -i perf_7t_cycles_100k.out   --stdio  > report_7t_cycles_100k.txt
#sudo perf report -i perf_7t_cycles_allk_00k.out   --stdio  > report_7t_cycles_allk_100k.txt
#sudo perf report -i perf_7t_ins_100k.out   --stdio  > report_7t_ins_100k.txt
#sudo perf report -i perf_7t_ins_allk_00k.out   --stdio  > report_7t_ins_allk_100k.txt
#sudo perf report -i perf_7t_L1-icache-load-misses_100k.out   --stdio  > report_7t_L1-icache-load-misses_100k.txt
#sudo perf report -i perf_7t_L1-icache-load-misses_allk_100k.out   --stdio  > report_7t_L1-icache-load-misses_allk_100k.txt
#sudo perf report -i perf_7t_l2d_cache_refill_fetch_100k.out   --stdio  > report_7t_l2d_cache_refill_fetch_100k.txt
#sudo perf report -i perf_7t_l2d_cache_refill_fetch_allk_100k.out   --stdio  > report_7t_l2d_cache_refill_fetch_allk_100k.txt
#
#sudo perf report -i perf_cycles_100k.out   --stdio  > report_cycles_100k.txt
#sudo perf report -i perf_cycles_allk_00k.out   --stdio  > report_cycles_allk_100k.txt
#sudo perf report -i perf_ins_100k.out   --stdio  > report_ins_100k.txt
#sudo perf report -i perf_ins_allk_00k.out   --stdio  > report_ins_allk_100k.txt
#sudo perf report -i perf_L1-icache-load-misses_100k.out   --stdio  > report_L1-icache-load-misses_100k.txt
#sudo perf report -i perf_L1-icache-load-misses_allk_100k.out   --stdio  > report_L1-icache-load-misses_allk_100k.txt
#sudo perf report -i perf_l2d_cache_refill_fetch_100k.out   --stdio  > report_l2d_cache_refill_fetch_100k.txt
#sudo perf report -i perf_l2d_cache_refill_fetch_allk_100k.out   --stdio  > report_l2d_cache_refill_fetch_allk_100k.txt
#
sudo perf annotate -i perf_7t_cycles_100k.out --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --stdio  > annotate_7t_cycles_100k.txt
sudo perf annotate -i perf_7t_cycles_allk_00k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_cycles_allk_100k.txt
sudo perf annotate -i perf_7t_ins_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_ins_100k.txt
sudo perf annotate -i perf_7t_ins_allk_00k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_ins_allk_100k.txt
sudo perf annotate -i perf_7t_L1-icache-load-misses_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_L1-icache-load-misses_100k.txt
sudo perf annotate -i perf_7t_L1-icache-load-misses_allk_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_L1-icache-load-misses_allk_100k.txt
sudo perf annotate -i perf_7t_l2d_cache_refill_fetch_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_7t_l2d_cache_refill_fetch_100k.txt
sudo perf annotate -i perf_7t_l2d_cache_refill_fetch_allk_100k.out --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --stdio  > annotate_7t_l2d_cache_refill_fetch_allk_100k.txt

sudo perf annotate -i perf_cycles_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_cycles_100k.txt
sudo perf annotate -i perf_cycles_allk_00k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_cycles_allk_100k.txt
sudo perf annotate -i perf_ins_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_ins_100k.txt
sudo perf annotate -i perf_ins_allk_00k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_ins_allk_100k.txt
sudo perf annotate -i perf_L1-icache-load-misses_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_L1-icache-load-misses_100k.txt
sudo perf annotate -i perf_L1-icache-load-misses_allk_100k.out --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --stdio  > annotate_L1-icache-load-misses_allk_100k.txt
sudo perf annotate -i perf_l2d_cache_refill_fetch_100k.out --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --stdio  > annotate_l2d_cache_refill_fetch_100k.txt
sudo perf annotate -i perf_l2d_cache_refill_fetch_allk_100k.out  --vmlinux /boot/vmlinuz-5.4.0-1037-aws --stdio  > annotate_l2d_cache_refill_fetch_allk_100k.txt

