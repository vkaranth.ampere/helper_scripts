#!/bin/bash

#coreid mask where tasks need to be affinitized 02-> C1, 04-> C2 ..
core="03"

tid_affinity(){
	for i in `pgrep httpd` ; do
		for t in /proc/${i}/task/* ; do
			tid=$(basename $t)
			if [[ $tid != $i ]]; then
				sudo taskset -p $core  ${tid}
			else
				echo $tid
			fi
		done
	done
}

tid_affinity

