#!/bin/bash

HTTPD_CORE=1

start_server(){
sudo pkill httpd
sudo pkill apache2
sleep 2

echo "restart server"
sudo numactl -C $HTTPD_CORE -m0 /usr/local/apache/bin/httpd
sleep 5
for i in `pgrep httpd`; do
        sudo migratepages $i 1 0
        echo "migrated $i to 0"
done
}
start_server
