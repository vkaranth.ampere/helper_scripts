#!/bin/bash

#run the script from directory where perf stat files are present. 

for f in *; do
	#echo $f
	val=$(tail -4 $f | head -n 1 | awk '{for(i=NF;i>=1;i--) printf "%s,", $i;print""}')
	echo $f,$val
done
