#!/bin/bash
pkill httpd
sleep 1
echo "starting httpd"
numactl -C 1 -m 0 /usr/local/apache/bin/httpd
sleep 2

stat="strace_100k.txt"
if [ ! -z "$1" ]; then
	stat="$1"
fi
pids=''
for i in `pgrep httpd`; do
        if [ -z $pids ] ; then
                pids=$i
        else
                pids=$pids,$i
        fi
done

echo $pids

strace -fp "$pids" -C -o $stat &
spid=$!
sleep 2


#export LD_LIBRARY_PATH=/usr/local/apache/lib

numactl -C 41 -m 0 ab -r -n 10 -c 1 http://0.0.0.0/test.html

kill -INT $spid

echo "done"
