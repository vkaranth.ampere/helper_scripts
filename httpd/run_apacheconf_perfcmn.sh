#!/bin/bash

sudo pkill httpd
sudo pkill apache2
sleep 5

abcore=41

keepalive=0
stat="apache_cmnstat_100k.txt"
if [ ! -z "$1" ] ; then
	stat=$1
fi

p1=3
p2=75
p3=250
p4=25
p5=400

if [ ! -z "$2" ]; then
	p1=$2
fi

if [ ! -z "$3" ]; then
	p2=$3
fi
if [ ! -z "$4" ]; then
	p3=$4
fi
if [ ! -z "$5" ]; then
	p4=$5
fi
if [ ! -z "$6" ]; then
	p5=$6
fi

echo "Reconfigure" 
./config_apache.sh $p1 $p2 $p3 $p4 $p5

resname=res_test_"$p1"_"$p2"_"$p3"_"$p4"_"$p5".txt
echo $resname
echo "restart server"
numactl -C 1 -m0 /usr/local/apache/bin/httpd 
sleep 5

#if [ ! -z "$2" ]; then
#	keepalive=1
#fi

echo "$stat"



cmd="numactl -C $abcore -m 0 ab -c 1 -n 100000  -r http://0.0.0.0/test.html"
if [ $keepalive -eq 1 ]; then
	cmd="numactl -C $abcore -m 0 ab -c 1 -n 100000  -rk http://0.0.0.0/test.html"
fi
echo "$cmd"
perf stat  -e \
arm_cmn/hnf_slc_sf_cache_access,nodeid=12,name='cache_access_12'/,arm_cmn/hnf_cache_miss,nodeid=12,name='cache_miss_12'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=13,name='cache_access_13'/,arm_cmn/hnf_cache_miss,nodeid=13,name='cache_miss_13'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=20,name='cache_access_20'/,arm_cmn/hnf_cache_miss,nodeid=20,name='cache_miss_20'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=21,name='cache_access_21'/,arm_cmn/hnf_cache_miss,nodeid=21,name='cache_miss_21'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=28,name='cache_access_28'/,arm_cmn/hnf_cache_miss,nodeid=28,name='cache_miss_28'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=29,name='cache_access_29'/,arm_cmn/hnf_cache_miss,nodeid=29,name='cache_miss_29'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=36,name='cache_access_36'/,arm_cmn/hnf_cache_miss,nodeid=36,name='cache_miss_36'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=37,name='cache_access_37'/,arm_cmn/hnf_cache_miss,nodeid=37,name='cache_miss_37'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=332,name='cache_access_332'/,arm_cmn/hnf_cache_miss,nodeid=332,name='cache_miss_332'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=333,name='cache_access_333'/,arm_cmn/hnf_cache_miss,nodeid=333,name='cache_miss_333'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=340,name='cache_access_340'/,arm_cmn/hnf_cache_miss,nodeid=340,name='cache_miss_340'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=341,name='cache_access_341'/,arm_cmn/hnf_cache_miss,nodeid=341,name='cache_miss_341'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=348,name='cache_access_348'/,arm_cmn/hnf_cache_miss,nodeid=348,name='cache_miss_348'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=349,name='cache_access_349'/,arm_cmn/hnf_cache_miss,nodeid=349,name='cache_miss_349'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=356,name='cache_access_356'/,arm_cmn/hnf_cache_miss,nodeid=356,name='cache_miss_356'/,\
arm_cmn/hnf_slc_sf_cache_access,nodeid=357,name='cache_access_357'/,arm_cmn/hnf_cache_miss,nodeid=357,name='cache_miss_357'/,\
arm_cmn/hnf_mc_reqs,nodeid=12,name='mc_req_12'/,arm_cmn/hnf_mc_retries,nodeid=12,name='mc_retries_12'/,\
arm_cmn/hnf_mc_reqs,nodeid=13,name='mc_req_13'/,arm_cmn/hnf_mc_retries,nodeid=13,name='mc_retries_13'/,\
arm_cmn/hnf_mc_reqs,nodeid=20,name='mc_req_20'/,arm_cmn/hnf_mc_retries,nodeid=20,name='mc_retries_20'/,\
arm_cmn/hnf_mc_reqs,nodeid=21,name='mc_req_21'/,arm_cmn/hnf_mc_retries,nodeid=21,name='mc_retries_21'/,\
arm_cmn/hnf_mc_reqs,nodeid=28,name='mc_req_28'/,arm_cmn/hnf_mc_retries,nodeid=28,name='mc_retries_28'/,\
arm_cmn/hnf_mc_reqs,nodeid=29,name='mc_req_29'/,arm_cmn/hnf_mc_retries,nodeid=29,name='mc_retries_29'/,\
arm_cmn/hnf_mc_reqs,nodeid=36,name='mc_req_36'/,arm_cmn/hnf_mc_retries,nodeid=36,name='mc_retries_36'/,\
arm_cmn/hnf_mc_reqs,nodeid=37,name='mc_req_37'/,arm_cmn/hnf_mc_retries,nodeid=37,name='mc_retries_37'/,\
arm_cmn/hnf_mc_reqs,nodeid=332,name='mc_req_332'/,arm_cmn/hnf_mc_retries,nodeid=332,name='mc_retries_332'/,\
arm_cmn/hnf_mc_reqs,nodeid=333,name='mc_req_333'/,arm_cmn/hnf_mc_retries,nodeid=333,name='mc_retries_333'/,\
arm_cmn/hnf_mc_reqs,nodeid=340,name='mc_req_340'/,arm_cmn/hnf_mc_retries,nodeid=340,name='mc_retries_340'/,\
arm_cmn/hnf_mc_reqs,nodeid=341,name='mc_req_341'/,arm_cmn/hnf_mc_retries,nodeid=341,name='mc_retries_341'/,\
arm_cmn/hnf_mc_reqs,nodeid=348,name='mc_req_348'/,arm_cmn/hnf_mc_retries,nodeid=348,name='mc_retries_348'/,\
arm_cmn/hnf_mc_reqs,nodeid=349,name='mc_req_349'/,arm_cmn/hnf_mc_retries,nodeid=349,name='mc_retries_349'/,\
arm_cmn/hnf_mc_reqs,nodeid=356,name='mc_req_356'/,arm_cmn/hnf_mc_retries,nodeid=356,name='mc_retries_356'/,\
arm_cmn/hnf_mc_reqs,nodeid=357,name='mc_req_357'/,arm_cmn/hnf_mc_retries,nodeid=357,name='mc_retries_357'/ \
 -o $stat \
$cmd 1> results/res/$resname
#numactl -C 41 -m 0 ab -c 1 -n 100000  -r http://0.0.0.0/test.html

#echo "keepalive on!!!"
