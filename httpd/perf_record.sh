sudo perf record -C 1 -e cycles -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  -o perf_7t_cycles_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e cycles -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws --all-kernel -o perf_7t_cycles_allk_00k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e instructions -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  -o perf_7t_ins_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e instructions -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws --all-kernel -o perf_7t_ins_allk_00k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e L1-icache-load-misses -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  -o perf_7t_L1-icache-load-misses_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e L1-icache-load-misses -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --all-kernel -o perf_7t_L1-icache-load-misses_allk_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/ -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  -o perf_7t_l2d_cache_refill_fetch_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

sudo perf record -C 1 -e armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/ -s -g -i --vmlinux /boot/vmlinuz-5.4.0-1037-aws  --all-kernel -o perf_7t_l2d_cache_refill_fetch_allk_100k.out numactl -C 41 -m0 ab -c 1 -r -n 100000 http://0.0.0.0/test.html
sleep 10

echo "start reporting"

sudo perf report -i perf_7t_cycles_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_cycles_100k.txt
sudo perf report -i perf_7t_cycles_allk_00k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_cycles_allk_100k.txt
sudo perf report -i perf_7t_ins_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_ins_100k.txt
sudo perf report -i perf_7t_ins_allk_00k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_ins_allk_100k.txt
sudo perf report -i perf_7t_L1-icache-load-misses_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_L1-icache-load-misses_100k.txt
sudo perf report -i perf_7t_L1-icache-load-misses_allk_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_L1-icache-load-misses_allk_100k.txt
sudo perf report -i perf_7t_l2d_cache_refill_fetch_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_l2d_cache_refill_fetch_100k.txt
sudo perf report -i perf_7t_l2d_cache_refill_fetch_allk_100k.out --header -g flat,count --stdio -T --sort=dso,comm > report_7t_l2d_cache_refill_fetch_allk_100k.txt

echo "start flamegraph"

sudo perf script -i perf_7t_cycles_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra cycles 100k ubuntu" > perf_7t_cycles_100k.svg

sudo perf script -i perf_7t_cycles_allk_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra cycles kernel 100k ubuntu" > perf_7t_cycles_allk_100k.svg

sudo perf script -i perf_7t_ins_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra ins 100k ubuntu" > perf_7t_ins_100k.svg

sudo perf script -i perf_7t_ins_allk_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra ins kernel 100k ubuntu" > perf_7t_ins_allk_100k.svg

sudo perf script -i perf_7t_L1-icache-load-misses_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra l1i-miss 100k ubuntu" > perf_7t_L1-icache-load-misses_100k.svg

sudo perf script -i perf_7t_L1-icache-load-misses_allk_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra l1i-miss kernel 100k ubuntu" > perf_7t_L1-icache-load-misses_allk_100k.svg

sudo perf script -i perf_7t_l2d_cache_refill_fetch_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra l2refill_fetch 100k ubuntu" > perf_7t_l2d_cache_refill_fetch_100k.svg

sudo perf script -i perf_7t_l2d_cache_refill_fetch_allk_100k.out | /home/vkaranth/FlameGraph/stackcollapse-perf.pl > out-folded
/home/vkaranth/FlameGraph/flamegraph.pl out-folded --title="Altra l2refill_fetch kernel 100k ubuntu" > perf_7t_l2d_cache_refill_fetch_allk_100k.svg


