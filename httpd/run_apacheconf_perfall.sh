##!fbin/bash

HTML_FILE="test.html"
HTTPD_CORE=1
AB_CORE=41
RES_FILE=res.txt
STAT_FILE=stat.txt
THREAD_CFG="3,75,250,25,400"
PERF=1
PIDSTAT=0
EPYC=0
NUM_REQ=100000
CONNS=1
APACHE="apache"

config_apache(){
	IFS=',' read -r -a cfg_array <<< "$THREAD_CFG"
	echo config parameters "${cfg_array[0]}" "${cfg_array[1]}" "${cfg_array[2]}" "${cfg_array[3]}" "${cfg_array[4]}"
	./config_apache.sh "${cfg_array[0]}" "${cfg_array[1]}" "${cfg_array[2]}" "${cfg_array[3]}" "${cfg_array[4]}" "${APACHE}"
}

start_server(){
sudo pkill httpd
sudo pkill apache2
sleep 5

echo "restart server"
numactl -C $HTTPD_CORE -m0 /usr/local/$APACHE/bin/httpd 
sleep 5
for i in `pgrep httpd`; do
        migratepages $i 1 0
        echo "migrated $i to 0"
done
}

run_bench(){
	cmd="numactl -C $AB_CORE -m 0 ab -c $CONNS -n $NUM_REQ -r http://0.0.0.0/${HTML_FILE}"
echo "$cmd"
echo "output files $RES_FILE $STAT_FILE"

if [[ $PERF = 1 ]]; then
	if [[ $EPYC = 1 ]]; then
		sudo perf stat -C $HTTPD_CORE -e cycles,cycles:k,instructions,instructions:k,stalled-cycles-frontend,stalled-cycles-backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
branch-misses,\
branch-load-misses,\
branch-loads,\
iTLB-loads,\
iTLB-load-misses,\
dTLB-loads,\
dTLB-load-misses,\
ic_cache_inval.l2_invalidating_probe,\
ic_cache_fill_l2,\
ic_cache_fill_sys,\
cpu/event=0x64,umask=0x07,name='l2_cache_req_stat.ic_access_in_l2'/,\
cpu/event=0x64,umask=0x01,name='l2_cache_req_stat.ic_fill_miss'/,\
cpu/event=0x64,umask=0x02,name='l2_cache_req_stat.ic_fill_hit_s'/,\
cpu/event=0x64,umask=0x04,name='l2_cache_req_stat.ic_fill_hit_x'/,\
cpu/event=0x64,umask=0x09,name='l2_cache_req_stat.ic_dc_miss_in_l2'/,\
cpu/event=0x64,umask=0xf6,name='l2_cache_req_stat.ic_dc_hit_in_l2/',\
l2_cache_req_stat.ls_rd_blk_c,\
l2_cache_req_stat.ls_rd_blk_cs,\
l2_cache_req_stat.ls_rd_blk_l_hit_s,\
l2_cache_req_stat.ls_rd_blk_l_hit_x,\
l2_cache_req_stat.ls_rd_blk_x,\
l2_request_g1.cacheable_ic_read,\
l2_request_g1.change_to_x,\
l2_request_g1.l2_hw_pf,\
l2_request_g1.ls_rd_blk_c_s,\
l2_request_g1.rd_blk_l,\
l2_request_g1.rd_blk_x,\
l2_request_g1.group2,\
l2_request_g2.group1,\
cpu/event=0x60,umask=0xf9,name='l2_request_g1.all_no_prefetch/',\
l2_pf_hit_l2,\
l2_pf_miss_l2_hit_l3,\
l2_pf_miss_l2_l3,\
l3_comb_clstr_state.other_l3_miss_typs,\
l3_comb_clstr_state.request_miss,\
l3_lookup_state.all_l3_req_typs,\
l3_request_g1.caching_l3_cache_accesses,\
xi_ccx_sdp_req1.all_l3_miss_req_typs \
-o $STAT_FILE \
$cmd 1> $RES_FILE &
	else
sudo perf stat -C $HTTPD_CORE -e cycles,cycles:k,instructions,instructions:k,stall_frontend,stall_backend,cs,faults,\
L1-icache-loads,L1-icache-loads-misses,L1-icache-loads:k,L1-icache-loads-misses:k,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
armv8_pmuv3_0/event=0x0017,name=l2d_cache_refill/,\
armv8_pmuv3_0/event=0x0018,name=l2d_cache_wb/,\
armv8_pmuv3_0/event=0x0163,name=fetch_fq_empty/,\
armv8_pmuv3_0/event=0x002f,name=l2d_tlb/,\
armv8_pmuv3_0/event=0x002d,name=l2d_tlb_refill/\,\
armv8_pmuv3_0/event=0x0025,name=l1d_tlb/,\
armv8_pmuv3_0/event=0x0034,name=dtlb_walk/,\
armv8_pmuv3_0/event=0x005,name=l1d_tlb_refill/,\
armv8_pmuv3_0/event=0x0026,name=l1i_tlb/,\
armv8_pmuv3_0/event=0x0035,name=itlb_walk/,\
armv8_pmuv3_0/event=0x002,name=l1i_tlb_refill/,\
armv8_pmuv3_0/event=0x0120,name=flushes/,\
armv8_pmuv3_0/event=0x0121,name=mem_hazard_flushes/,\
armv8_pmuv3_0/event=0x0124,name=isb_flushes/,\
armv8_pmuv3_0/event=0x0154,name=bus_req_rd/,\
armv8_pmuv3_0/event=0x001d,name=bus_cycles/,\
armv8_pmuv3_0/event=0x0156,name=bus_req_sn/,\
armv8_pmuv3_0/event=0x0019,name=bus_access/,\
armv8_pmuv3_0/event=0x0013,name=mem_access/,\
armv8_pmuv3_0/event=0x036,name=ll_cache_rd/,\
armv8_pmuv3_0/event=0x037,name=ll_cache_miss_rd/,\
l1d_cache,l1d_cache_refill,l1d_cache_wb,l1d_cache_wb_clean,l2d_cache_wb,armv8_pmuv3_0/event=0x0108,name=l2d_cache_refill_fetch/,\
armv8_pmuv3_0/event=0x0100,name=l1_prefetch/,\
armv8_pmuv3_0/event=0x0101,name=l1_prefetch_late/,\
armv8_pmuv3_0/event=0x010a,name=l2_prefetch/,\
armv8_pmuv3_0/event=0x010b,name=l2_prefetch_late/,\
armv8_pmuv3_0/event=0x0145,name=prefetch_hits/,\
armv8_pmuv3_0/event=0x0146,name=prefetch_reqs/,\
armv8_pmuv3_0/event=0x056,name=l2d_cache_wb_victim/,\
armv8_pmuv3_0/event=0x057,name=l2d_cache_wb_clean/ \
-o $STAT_FILE \
$cmd 1> $RES_FILE &
	fi
elif [[ $PERF = 2 ]]; then
	sudo perf record -C $HTTPD_CORE -e cycles -g -o $STAT_FILE $cmd 1> $RES_FILE &
elif [[ $PERF = 3 ]]; then
sudo perf stat -C $HTTPD_CORE -e cycles,cycles:k,instructions,stall_frontend,stall_backend,cs,\
armv8_pmuv3_0/event=0x0021,name=br_retired/,\
armv8_pmuv3_0/event=0x0022,name=br_mis_pred_retired/,l2d_cache,armv8_pmuv3_0/event=0x0058,name=l2d_cache_inval/,\
r110,r111,r112,r113,r114,r115,r116,r117,r118,r119,r11a,r11b,r11c,r11d,r120,r121,r122,r123,r124,r125  \
-o $STAT_FILE \
$cmd 1> $RES_FILE &
else
	$cmd 1> $RES_FILE &
fi
runpid="$!"
wait $runpid
if [[ $PIDSTAT = 1 ]]; then
	sleep 2
	echo "start pid process"
	./process_pidstat.sh -o "$RES_FILE".pidstat.csv
fi

echo "benchmark completed"
}

#arm_cmn/hnf_slc_sf_cache_access,nodeid=12,name='cache_access_12'/,arm_cmn/hnf_cache_miss,nodeid=12,name='cache_miss_12'/,\
#arm_cmn/hnf_mc_reqs,nodeid=12,name='mc_req_12'/ \
parse_args() {
    options=$(getopt -o hf:r:s:c:b:t:n:x:p:a: -l noperf,epyc,pidstat -- "$@")
    [ $? -eq 0 ] || {
        echo "error: Incorrect option provided"
        exit 1
    }
    eval set -- "$options"
    while true; do
        case "$1" in
        -h)
            echo "$0 [-h] [-r] [-f] [-s] [-c] [-b] [-t] [-n] [-x] [-p] [-a] [--noperf] [--epyc] [--pidstat]"
	    echo "    -f: html file to serve(default test.html)"
	    echo "    -r: result filename(default res.txt)"
	    echo "    -s: stat file name(default stat.txt)"
	    echo "    -a: apache build to be used under /usr/local/<name>, default is apache"
	    echo "    -c: apache coreid (default 1)"
	    echo "    -b: ab core id(default 41)"
	    echo "    -t: thread config, 5 parameters separated by comma eg:1,2,2,5,10"
	    echo "    -n: number of requests to process(default 100000)"
	    echo "    -x: number of ab connections"
	    echo "    -p: perf type "record" or "stat" or "branch" (default perf stat)"
	    echo "    --noperf: disable perf stat(default Enabled)"
	    echo "    --pidstat: process pid stat(default disabled)"
	    echo "    --epyc: setup for epyc system(default Altra)"
            exit
            ;;
        -f)
            shift
            export HTML_FILE=$1
            ;;
	-r)
	    shift
	    export RES_FILE=$1
	    ;;
	-s)
	    shift
	    export STAT_FILE=$1
	    ;;
	-a)
	    shift
	    export APACHE=$1
	    ;;
	-c)
	    shift
	    export HTTPD_CORE=$1
	    ;;
	-b)
	    shift
	    export AB_CORE=$1
	    ;;
	-t)
	    shift
	    export THREAD_CFG=$1
	    ;;
	-n)
	    shift
	    export NUM_REQ=$1
	    ;;
	 -x)
            shift
	    export CONNS=$1
            ;;
	 -p)
	    shift
	    if [ "$1" =  "record" ]; then
		    export PERF=2
	    fi
	    if [ "$1" = "branch" ]; then
		    export PERF=3
	    fi
	    ;;
	--noperf)
	    export PERF=0
	    ;;
	--pidstat)
	    export PIDSTAT=1
	    ;;
	--epyc)
	    export EPYC=1
	    export AB_CORE=2
	    ;;
        --)
            shift
            break
            ;;
        esac
        shift
    done
}

parse_args $*
config_apache
start_server
run_bench
