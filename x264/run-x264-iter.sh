#!/bin/bash

WAIT=""
COPIES=1
RES=res_x264
if [ ! -z $1 ]; then
	COPIES=$1
fi

mkdir -p ${RES}
rm -rf ${RES}/*

if [ ! -f ./x264/x264 ]; then
  sudo apt install nasm build-essential -y
  git clone https://code.videolan.org/videolan/x264.git
  cd x264
  make -j10
fi

SMT_TRICK=1

for i in $(seq 1 $COPIES); do
  core=$((i-1))
  if [ $SMT_TRICK -eq 1 ]; then
	core=$(((i-1)*2))	  
     if [ $i -gt 8 ]; then
	core=$(((i-9)*2+1))
     fi
  fi
  numactl -C ${core} ./x264/x264  --preset medium --psnr --tune psnr --threads 1 --frames 100 --profile main ducks_take_off_1080p50.y4m  -o ${RES}/test_${i}.output 2> ${RES}/x264_out_${i}.txt &  
  WAIT="$WAIT ""$!"
done

wait $WAIT
total=0
for f in ${RES}/*.txt; do
  fps="$(grep encoded $f | tail -1 | awk '{printf("%.2f",$4)}')"
  echo $f " " $fps
  total=$(tr ',' '.' <<< "$total + $fps" | bc -l) 
done
echo "total fps = ${total}"
