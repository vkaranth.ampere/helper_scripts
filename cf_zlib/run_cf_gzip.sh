#!/bin/bash

if [ ! -d comp_bench ]; then
	git clone --recursive https://github.com/vkrasnov/comp_bench.git
fi

nprocs=`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l`


# Build compression
cd comp_bench
if [ ! -f ./bench ]; then
	make
fi
cd ..


comp () {
	res=`./comp_bench/bench -q $1 -c $2 $3 ./comp_bench/index.html | tail -1 | cut -f 2 -d','`
	echo $res MiB/s
}


echo benchmark,1 core,$nprocs cores | tee $out


echo "gzip performance (cloudflare zlib)" | tee -a $out
echo gzip -4 $( comp 4 1 ),$( comp 4 $nprocs )

#for q in {4..9}; do
#	echo gzip -$q,$( comp $q 1 ),$( comp $q $nprocs ) 
#done

