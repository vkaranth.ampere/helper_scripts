#!/bin/bash

#sudo yum install ca-certificates -y
#sudo update-ca-trust force-enable
#sudo cp key/NGINX_TEST_SSL.crt /etc/pki/ca-trust/source/anchors/
#sudo update-ca-trust extract
#ubuntu:
sudo apt update
sudo apt install ca-certificates -y
sudo cp key/NGINX_TEST_SSL.crt /usr/local/share/ca-certificates/
sudo update-ca-certificates --verbose
sudo apt install wget build-essential unzip -y 

#sudo yum install wget make gcc unzip -y
wget https://github.com/wg/wrk/archive/refs/tags/4.2.0.tar.gz
tar zxvf 4.2.0.tar.gz
cd wrk-4.2.0/
make -j`nproc`


echo "Adjust the parameter in nginx_https.sh and start the testing."
