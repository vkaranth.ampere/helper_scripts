#!/bin/bash

#sudo yum install nginx -y
sudo apt install nginx -y

sudo adduser --system --no-create-home --shell /bin/false --group --disabled-login nginx
sudo mkdir -p /etc/pki/tls/certs
sudo mkdir -p /etc/pki/tls/private

sudo cp key/NGINX_TEST_SSL.crt /etc/pki/tls/certs/
sudo cp key/NGINX_TEST_SSL.key /etc/pki/tls/private/
sudo cp key/NGINX_TEST_SSL.csr /etc/pki/tls/private/

sudo cp gzip_test.html /usr/share/nginx/html
sudo cp gzip.conf /etc/nginx/conf.d/
sudo chmod 644 /etc/nginx/conf.d/gzip.conf

sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
sudo cp nginx.conf /etc/nginx/nginx.conf

sudo systemctl restart nginx 
