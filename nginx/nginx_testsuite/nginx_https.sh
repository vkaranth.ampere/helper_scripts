#!/bin/bash

Platform="c6_64"
NGINX_HOST=172.31.26.121
if [ ! -z $1 ] ; then
	Platform=$1
fi
if [ ! -z $2 ] ; then
	NGINX_HOST=$2
fi

echo $Platform
echo $NGINX_HOST

WORKLOADS=(gzip_test.html)
THREADS=(128 150)
CONNECT=(400 500 600)
LOG=nginx_http_${Platform}_${workload}.log
echo `date` |tee -a nginx_${Platform}_${workload}.log
for workload in ${WORKLOADS[@]};
do
  for thread in ${THREADS[@]};
  do
    for connection in ${CONNECT[@]};
        do /home/ubuntu/nginx_testsuite_client/wrk-4.2.0/wrk -t$thread -c$connection -H 'Accept-Encoding: gzip' -d30s http://${NGINX_HOST}:80/${workload} --latency |tee -a ${LOG};
        #do /home/ubuntu/nginx_testsuite_client/wrk-4.2.0/wrk -t$thread -c$connection -H 'Accept-Encoding: gzip' -d30s https://${NGINX_HOST}:443/${workload} --latency |tee -a ${LOG}
    done
  done
  printf "%-10s %-10s %-10s %-10s %-10s %-10s %-10s\n" "workload" "thread" "cons" "req/s" "trans/s" "avg_lat" "99%_lat" | tee -a ${LOG}
  awk -v workload="$workload" '/threads and/ { thread=$1; connection=$4 } /Latency  / { latavg=$2 } /99%/ { lat99=$2 } /Requests\/sec/ { req=$2 } /Transfer\/sec/ { trans=$2; printf "%-10s %-10s %-10s %-10s %-10s %-10s %-10s\n", workload, thread, connection, req, trans, latavg, lat99} ' ${LOG} |tee -a ${LOG}
done


