#!/bin/bash
Platform="dp16_altra_opt"
NGINX_HOST=10.0.0.20
WORKLOADS=(test.html)
THREADS=(1 16 24 32)
CONNECT=(50 100 150 200 300 500)

echo `date` |tee -a nginx_${Platform}_${workload}.log
for workload in ${WORKLOADS[@]};
do
  for thread in ${THREADS[@]};
  do
    for connection in ${CONNECT[@]};
	    do /home/azureuser/wrk/wrk -t$thread -c$connection -H 'Accept-Encoding: gzip' -d10s http://${NGINX_HOST}:80/${workload} --latency |tee -a nginx_${Platform}_${workload}.log;
    done
  done
  printf "%-10s %-10s %-10s %-10s %-10s %-10s %-10s\n" "workload" "thread" "cons" "req/s" "trans/s" "avg_lat" "99%_lat" | tee -a nginx_${Platform}_${workload}.log
  awk -v workload="$workload" '/threads and/ { thread=$1; connection=$4 } /Latency  / { latavg=$2 } /99%/ { lat99=$2 } /Requests\/sec/ { req=$2 } /Transfer\/sec/ { trans=$2; printf "%-10s %-10s %-10s %-10s %-10s %-10s %-10s\n", workload, thread, connection, req, trans, latavg, lat99} ' nginx_${Platform}_${workload}.log |tee -a nginx_${Platform}_${workload}.log
done


