Enable gzip by adding following config in /etc/nginx/conf.d/gzip.conf
gzip on;
gzip_min_length  100;
gzip_buffers  8 32k;
gzip_types  text/plain text/css application/x-javascript text/xml application/xml text/javascript;
gzip_vary on;

The attached nginx_opt.conf is used for 8vCPU instance.
The worker_processes should be changed if used for Altra/AltraMax test.
The script and static html file are also attached. - goes to /usr/share/nginx/html . change the permission to 777 

nginx user needs to exist for nginx to start with attached conf. create a user with below command
sudo adduser --system --no-create-home --shell /bin/false --group --disabled-login nginx

test the connection by: curl -H "Accept-Encoding: gzip" -I http://10.1.0.7:80/test.html

Building nginx for ARM
Building nginx
wget http://nginx.org/download/nginx-1.18.0.tar.gz
wget https://www.openssl.org/source/old/1.1.1/openssl-1.1.1i.tar.gz
wget http://zlib.net/zlib-1.2.12.tar.gz
http://nginx.org/en/docs/configure.html
CC=/usr/bin/gcc-10 CFLAGS="-mcpu=neoverse-n1 -O3 -march=armv8.2-a+crypto+fp16+rcpc+dotprod+lse" ./configure --with-openssl=../openssl-1.1.1i --with-zlib=../zlib-1.2.12 --with-zlib-opt="-mcpu=neoverse-n1 -O3 -march=armv8.2-a+crypto+fp16+rcpc+dotprod+lse" --with-cc-opt="-mcpu=neoverse-n1 -O3 -march=armv8.2-a+crypto+fp16+rcpc+dotprod+lse" --prefix=/opt/nginx


wrk :
wget https://github.com/wg/wrk/archive/refs/tags/4.2.0.tar.gz
sudo apt install unzip
make

