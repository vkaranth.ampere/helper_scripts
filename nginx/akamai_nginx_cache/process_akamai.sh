
#res="results/04052023_vaishali/DUT/nginx_res_2LG_nocache_04062023_0214"
res="PostFix/PostFix-LimitRun/DUT/05102023_2246_LG1LG2_Q64-q06ngx-Lim2600M"
#res="results/04272023_64c_vaishali/nginx_all_res/DUT/04272023_1950_nginx_res_Q64_2600M"
#res="results/04062023_pwr_vaishali/DUT/nginx_res_1LG_pwr_04072023_0036"
#res="results/04052023_vaishali/DUT/nginx_res_1LG_04052023_1838"
#for f in 256K; do
for f in 1K 64K 128K 256K 512K 1M; do
	for r in 1000 5000 10000 15000 20000 30000; do
		
		echo "file =  $f, RPS = $r "
		python3 plot_stats.py  --network $res/test_"$f"/sar_"$f"."$r" --ifaces enp1s0f0np0,enp1s0f1np1 --mpstat $res/test_"$f"/mpstat_"$f"."$r" --dstat $res/test_"$f"/dstat_"$f"."$r" --power $res/test_"$f"/power_"$f"."$r" --output $res/test_"$f"/stats_"$f"_"$r".png --outpower $res/Q64-26_power_summary.csv --oututil $res/Q64-26_util_summary.csv
		#python3 plot_stats.py --dstat $res/test_"$f"/dstat_"$f"."$r" --network $res/test_"$f"/sar_"$f"."$r" --ifaces enp1s0f0,enp1s0f1 --mpstat $res/test_"$f"/mpstat_"$f"."$r" --power $res/test_"$f"/pwr_"$f"."$r".h --output $res/test_"$f"/stats_"$f"_"$r".png
       done
done
