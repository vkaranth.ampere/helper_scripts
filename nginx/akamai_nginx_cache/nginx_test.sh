#!/bin/bash 

DUT_IP="10.16.101.219"
DUT_ADDR="10.16.105.212"
OS_IP="10.16.101.160"
LG_IP=""
LG_IP2="10.16.101.145"
# LG_IP="172.25.173.67"
RUN_TIME=240 # Each test run time
INTER_SLEEP=5 # Sleep between each SSH


TEST_RESULTS_FOLDER="nginx_res_1LG_nocache_"`date +%m%d%Y_%H%M` 

THRD=10
CONNS=500

for SIZE in 1K 64K 128K 256K 512K 1M  
do
    for RPS in 1000 10000 20000 25000 30000
    do
        echo "$SIZE.$RPS: Running"
        echo "$SIZE.$RPS: Making test directory on LG"
        mkdir -p /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE

        #ssh vkaranth@$LG_IP2 "mkdir -p /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE">> outfile 2>&1&
        
	echo "$SIZE.$RPS: Making test directory on DUT"
        ssh vkaranth@$DUT_IP "mkdir -p /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE"
	echo "$SIZE.$RPS: Making test directory on OS"
        ssh vkaranth@$OS_IP "mkdir -p /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE"

	if [ $SIZE = "1K" ]; then
		CONNS=300
	fi
	#get the files cached
	echo "Start caching"
        /home/vkaranth/wrk2-aarch64/wrk -t"$THRD" -c"$CONNS" -d30 -R$RPS --latency http://$DUT_ADDR/hello/$SIZE > /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE/wrk.$SIZE.$RPS.30s.1 

        echo "$SIZE.$RPS: Starting load"

	/home/vkaranth/wrk2-aarch64/wrk -t"$THRD" -c"$CONNS" -d"$RUN_TIME" -R$RPS --latency http://$DUT_ADDR/hello/$SIZE > /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE/wrk.$SIZE.$RPS."$RUN_TIME"s.1  2>&1&
       
        c=1	
        for ip in $DUT_IP $OS_IP; do
          echo "$SIZE.$RPS: Starting gathering sar network stats"
          ssh vkaranth@$ip "sar -n DEV 1 > /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE/sar_$SIZE.$RPS" 2>&1&

          echo "$SIZE.$RPS: Starting to gather mpstat"
          ssh vkaranth@$ip "mpstat -P ALL 1  > /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE/mpstat_$SIZE.$RPS" 2>&1&

	  if [ $c -eq 1 ]; then
            echo "$SIZE.$RPS: Starting to gather dstat"
            ssh vkaranth@$ip "dstat 1  > /home/vkaranth/$TEST_RESULTS_FOLDER/test_$SIZE/dstat_$SIZE.$RPS" 2>&1&
          fi
	  c=0

        done
        
	sleep $RUN_TIME

	for ip in $DUT_IP $OS_IP; do
	  ssh root@$ip "pkill mpstat"
	  sleep 1
	  ssh root@$ip "pkill sar"
	  sleep 1
	  #dstat starts from python3
	  ssh root@$ip "pkill python3"
	  sleep 1
        done
        
    done
done
