Attached are the scripts I used for my Nginx tests. Please forward them internally to appropriate teams.
 
PGFkBx6jypkAAAAASUVORK5CYII=
 
LG is the load generating server.
DUT is device under test.
OS is Origin simulator (fake origin)
 
Eth0 interfaces and Eth1 interfaces should be different subnet. We need 100Gbps on all interfaces.
 
akamai@akamai:~$ cat /etc/os-release 
PRETTY_NAME="Ubuntu 22.04 LTS"
NAME="Ubuntu"
VERSION_ID="22.04"
VERSION="22.04 (Jammy Jellyfish)"
VERSION_CODENAME=jammy
ID=ubuntu
ID_LIKE=debian
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
UBUNTU_CODENAME=jammy
akamai@akamai:~$ uname -a
Linux akamai 5.15.0-48-generic #54-Ubuntu SMP Fri Aug 26 13:26:29 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
 
We use Ubuntu for all our tests. Please use Ubuntu LTS.
 
Test setup
 
Please install Nginx on DUT machine
sudo apt-get install nginx
sudo apt install sysstat
sudo cp ./nginx.conf /etc/nginx/nginx.conf 
sudo rm /etc/nginx/sites-enabled/default
sudo mkdir -p /data/nginx/cache
sudo systemctl restart nginx
 
Please install wrk2 on LG machine 
Clone wrk2 from its git repo and compile it
 
Please install apache on OS machine 
sudo apt install apache2
cd /var/www/html/hello/
sudo dd if=/dev/urandom of=./4M bs=4000000 count=1
sudo dd if=/dev/urandom of=./2M bs=2000000 count=1
sudo dd if=/dev/urandom of=./1M bs=1000000 count=1
sudo dd if=/dev/urandom of=./512K bs=512000 count=1
sudo dd if=/dev/urandom of=./256K bs=256000 count=1
sudo dd if=/dev/urandom of=./128K bs=128000 count=1
sudo dd if=/dev/urandom of=./64K bs=64000 count=1
sudo dd if=/dev/urandom of=./1K bs=1000 count=1
sudo systemctl restart apache2.service
sudo systemctl status apache2.service
 
 
Verify the network connections with iperf, verify if you can curl the Nginx and see if you are able to get Apache HTTP servers webpage through Nginx on your LG machine.
 
Basically, the client-side requests only reach eth0 on subnet1 and all the back-end requests to origin go via eth1 on subnet2.
 
It’s a crude setup but gives me my numbers.
Please let me know if you have any questions.
 
Note: I have also attached my 1LG and 2LG (2 load generator) test numbers for your CPU.
 
We can schedule a follow-on meeting to go over these test results.
Please let me know.
 
Would it be possible to get access to 3 of your lab machines in your lab? I would like to run SPEC, Coremark and some memory latency tests after the Nginx tests.
 
Regards,
Aditya
 

==============================
Vaishali's notes:

Tested on CentOS 8.4 
apache2 -> httpd on CentOS

Key changes:
* nginx.conf provided does not enable caching, the line for caching was commented. 
* Added caching valid and timeout and verified that files are cached on the location specified. This was durin gthe testing of 64K and 128K files. Increased timeout to 30m
 ls -lt /data/nginx/cache/
total 192
-rw------- 1 nginx nginx 128602 Apr  5 19:24 1eaaa04dbe0721833407a14ad4d55e4d
-rw------- 1 nginx nginx  64599 Apr  5 19:01 e672100f9b7f3f404afe9b962c709b1a

* Did a short 30s run to ensure caching before performance measurement run of 4m. Telemetry collected on DUT and OS for the longer runs
