#!/bin/bash 

DUT_IP="rtp-qs1-13-jade"  #1G IP for ssh
DUT_ADDR="10.16.105.241"
OS_IP="rtp-qs1-06-jade"   #1G IP for ssh
#LG2_IP="10.16.105.227" #change this to 1G IP 
RUN_TIME=240 # Each test run time


export TEST_RESULTS_FOLDER=`date +%m%d%Y_%H%M`"_nginx_res_Q64_"

THRD=10
CONNS=500

run_tests(){
for SIZE in 1K 64K 128K 256K 512K 1M  
do
    for RPS in 1000 5000 10000 15000 20000 25000 30000
    do
        echo "$SIZE.$RPS: Running"
        echo "$SIZE.$RPS: Making test directory on LG"
        mkdir -p /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE

	#second LG
	#echo "$SIZE.$RPS: Making test directory on LG2"
        #ssh root@$LG2_IP "mkdir -p /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE"
        
	echo "$SIZE.$RPS: Making test directory on DUT"
        ssh root@$DUT_IP "mkdir -p /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE"
	echo "$SIZE.$RPS: Making test directory on OS"
        ssh root@$OS_IP "mkdir -p /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE"

	if [ $SIZE = "1K" ]; then
		CONNS=300
	fi
        
	echo "$SIZE.$RPS: Starting load"

	/home/akamai/wrk2/wrk -t"$THRD" -c"$CONNS" -d"$RUN_TIME" -R$RPS --latency http://$DUT_ADDR/hello/$SIZE > /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/wrk.$SIZE.$RPS."$RUN_TIME"s  2>&1&
       
	#load gen 2
	#ssh root@$LG2_IP "/home/akamai/wrk2/wrk -t"$THRD" -c"$CONNS" -d"$RUN_TIME" -R$RPS --latency http://$DUT_ADDR/hello2/$SIZE > /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/wrk.$SIZE.$RPS."$RUN_TIME"s.1  2>&1&"

	dut_sys=1
        for ip in $DUT_IP $OS_IP; do
          echo "$SIZE.$RPS: Starting gathering sar network stats"
          ssh root@$ip "sar -n DEV 1 > /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/sar_$SIZE.$RPS" 2>&1&

          echo "$SIZE.$RPS: Starting to gather mpstat"
          ssh root@$ip "mpstat -P ALL 1  > /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/mpstat_$SIZE.$RPS" 2>&1&

          echo "$SIZE.$RPS: Starting to gather dstat"
          ssh root@$ip "dstat 1  > /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/dstat_$SIZE.$RPS" 2>&1&

	  if [ $dut_sys -eq 1 ]; then
		   ssh root@$ip "/home/akamai/sensors.sh /home/akamai/$TEST_RESULTS_FOLDER/test_$SIZE/power_$SIZE.$RPS tmppwr" 2>&1 &
	  fi
	  dut_sys=0
        done
        
	sleep $RUN_TIME

	for ip in $DUT_IP $OS_IP; do
	  ssh root@$ip "pkill mpstat"
	  sleep 1
	  ssh root@$ip "pkill sar"
	  sleep 1
	  ssh root@$ip "pkill dstat"
	  sleep 1
	  ssh root@$ip "rm -rf /tmp/tmppwr"
        done
        
    done
done
}


#crate res folders on the runner machine
ALL_RES="/home/akamai/nginx_all_res"
mkdir -p $ALL_RES"/DUT"
mkdir -p $ALL_RES"/OS"
mkdir -p $ALL_RES"/LG1"
TMP_TEST_FOLDER=$TEST_RESULTS_FOLDER

for f in "3000M" "2600M"; do
	echo "setting DUT to $f"
	ssh root@$DUT_IP "cpupower frequency-set -u $f -d $f " > /dev/null
	sleep 5

	TEST_RESULTS_FOLDER=${TMP_TEST_FOLDER}"$f"
	run_tests

	#copy all the results
        scp -r root@$DUT_IP:/home/akamai/$TEST_RESULTS_FOLDER $ALL_RES"/DUT/"
        scp -r root@$OS_IP:/home/akamai/$TEST_RESULTS_FOLDER $ALL_RES"/OS/"
	mv /home/akamai/$TEST_RESULTS_FOLDER $ALL_RES"/LG1/"

done

#reset the system back to 3G
ssh root@$DUT_IP "cpupower frequency-set -u 3000M -d 3000M" > /dev/null


echo "alldone"


