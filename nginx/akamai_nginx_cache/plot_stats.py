﻿import argparse
import sys,os,csv
from statistics import mean
import pandas as pd
#import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go


#global
fig = make_subplots(rows=4, cols=1, subplot_titles=('Network iface utilization',  'Network bw utilization', 'All CPU mpstat utilization', 'Power (W)'))
fig.update_layout(height=1600, width=1000,
                  title_text="Stats")


def process_dstat(infile):
    df = pd.read_csv(infile, delim_whitespace=True, skiprows = 2)
    #print(df)

    fig.append_trace(go.Scatter(y = pd.to_numeric(df['usr']), name = "usr%"), row=1, col=1)
    fig.add_trace(go.Scatter( y = pd.to_numeric(df['sys']), name = "sys%"), row=1, col=1)

   # fig.update_layout(
   # title="CPU utilization",
   # xaxis_title="samples",
   # yaxis_title="% utilization")

    # edit axis labelscd 
    fig['layout']['xaxis']['title']='samples'
    fig['layout']['yaxis']['title']='% CPU utilization'


def process_network(infile, ifaces):
    df = pd.read_csv(infile, delim_whitespace=True, skiprows = 2)
    iface_list = ifaces.split(",")
    filtered_df = []

    for i in iface_list:
        fdf = df.loc[df['IFACE'] == i]
        fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%ifutil"]), name = i+" %ifutil"), row=1, col=1)
        fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["rxkB/s"]), name = i+" rxkB/s"), row=2, col=1)
        fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["txkB/s"]), name = i+" txkB/s"), row=2, col=1)

         
    fig['layout']['xaxis2']['title']='Samples'
    fig['layout']['yaxis2']['title']='Network BW KB/s'
    fig['layout']['xaxis']['title']='Samples'
    fig['layout']['yaxis']['title']='% Network Interface utilization'
    

def process_mpstat(infile, outfile):
    df = pd.read_csv(infile, delim_whitespace=True, skiprows = 2)
    fdf = df.loc[df['CPU'] == "all"]
    fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%usr"]), name = "total %usr"), row=3, col=1)
    fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%sys"]), name = "total %sys"), row=3, col=1)
    fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%soft"]), name = "total %soft"), row=3, col=1)

    avg_util = int(100-mean(pd.to_numeric(fdf["%idle"])))
    max_util = int(100-min(pd.to_numeric(fdf["%idle"])))
    min_util = int(100-max(pd.to_numeric(fdf["%idle"])))
    util_data = infile + "," + str(avg_util) + "," + str(min_util) + "," + str(max_util) + "\n"
    with open(outfile, 'a') as ofile:
        ofile.write(util_data)

    fig['layout']['xaxis3']['title']='Samples'
    fig['layout']['yaxis3']['title']='All CPU utilization%'

    #specific to Altra 80 core
    #if false:
    #for i in range(1,80):
    #    fdf = df.loc[df['CPU'] == str(i)]
    #    print(fdf)
    #    fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%usr"]), name =  str(i)+" %usr"), row=3, col=1)
    #    fig.add_trace(go.Scatter( y = pd.to_numeric(fdf["%sys"]), name = str(i)+" %sys"), row=3, col=1)
    #fig['layout']['xaxis4']['title']='Samples'
    #fig['layout']['yaxis4']['title']='Per CPU utilization%'

def process_power(infile, outfile):
    df = pd.read_csv(infile, delim_whitespace=True, header=None)
    fig.add_trace(go.Scatter( y = pd.to_numeric(df[0]), name = "Socket power(W)"), row=4, col=1)
    fig['layout']['xaxis4']['title']='Samples'
    fig['layout']['yaxis4']['title']='Power'
    fig['layout']['yaxis4'].update(range=[0,210])
   
    avg_power = int(mean(pd.to_numeric(df[0])))
    min_power = int(min(pd.to_numeric(df[0])))
    max_power = int(max(pd.to_numeric(df[0])))

    power_data = infile + "," + str(avg_power) + "," + str(min_power) + "," +str(max_power) + "\n"
    with open(outfile, 'a') as ofile:
        ofile.write(power_data)
    

def main():
    parser = argparse.ArgumentParser(description="Process stat files.")
    parser.add_argument("--dstat", help="input dstat file.")
    parser.add_argument("--network", help="input sar network file.")
    parser.add_argument("--ifaces", help="list of interfaces separated by ,")
    parser.add_argument("--mpstat", help="input mpsat file")
    parser.add_argument("--power", help="input power sensor file")
    parser.add_argument("--output", help="output plot.")
    parser.add_argument("--outpower", help="output power data.")
    parser.add_argument("--oututil", help="output CPU util data.")
    args = parser.parse_args()

    #if args.dstat  and os.path.isfile(args.dstat):
    #    process_dstat(args.dstat)
    valid = False
    if args.network and os.path.isfile(args.network):
        if not args.ifaces:
            sys.exit("provided network interfaces to filter on")
        process_network(args.network, args.ifaces)
        valid = True
    if args.mpstat and os.path.isfile(args.mpstat):
        process_mpstat(args.mpstat, args.oututil)
        valid = True
    if args.power and os.path.isfile(args.power):
        process_power(args.power, args.outpower)
        valid = True

    if valid:
        fig.write_image(args.output)

if __name__ == "__main__":
    main()

