#!/bin/bash

set -x

if [ $EUID -ne 0 ]; then
  echo "Please use sudo to run this script."
  exit 1
fi

if [ $# -ne 0 ]; then
  echo "Set frequency specified by command line options"
  sudo cpupower frequency-set -f $1
else
  if sudo cpupower frequency-info | grep cppc_cpufreq; then
    echo "Set max frequency for performance"
    sudo cpupower frequency-set -g performance
  fi
fi
sudo tuned-adm off
sudo ulimit -s unlimited

# enable address randomization
echo 2 | sudo tee /proc/sys/kernel/randomize_va_space
# reset caches before invoking runcpu
echo 3 | sudo tee /proc/sys/vm/drop_caches
# swap only if necessary
echo 1 | sudo tee /proc/sys/vm/swappiness
# limit dirty cache to 8% of memory
echo 8 | sudo tee /proc/sys/vm/dirty_ratio
# free local node memory and avoid remote memory
echo 1 | sudo tee /proc/sys/vm/zone_reclaim_mode
# Disable numa balancing
echo 0 | sudo tee /proc/sys/kernel/numa_balancing
# Transparent huge pages set to 'never'
echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
