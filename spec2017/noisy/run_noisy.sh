for i in 48; do

./run_spec2017.sh --iterations=1 --copies=$i --tune=base 505 &
pid=$!
sleep 120
numactl -C 2 ./lat_mem_rd -t 128 128 2> latmem_"$i".out
numactl -C 2 ./multichase -c simple -m128m -s 128 > multichase_128m_"$i".out
numactl -C 2 ./multichase -c simple -m24m -s 128 > multichase_24m_"$i".out
wait $pid

done
