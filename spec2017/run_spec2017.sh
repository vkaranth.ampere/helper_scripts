#!/bin/bash

# Ampere Computing Copyright 2020

FULL_PATH=`readlink -f $0`
PKG_FOLDER=`dirname $FULL_PATH`
SPEC_DIR=${PKG_FOLDER}/spec2017

cd ${SPEC_DIR}
source ${SPEC_DIR}/shrc
cd ${SPEC_DIR}/bin

if [ $# -eq 0 ];
then
    ./runcpu --help
    exit 1
fi

if [[ "x${NUMASIZE}" == "x" ]]; then
    NUM_OF_CORES=`nproc --all`
    NUM_OF_NODES=`lscpu | grep -E "NUMA node[0-9]+" | wc -l`
    NUMASIZE=`expr $NUM_OF_CORES / $NUM_OF_NODES`
fi

# !!! WARNING !!! This folder is hard coded on purpose, because any compiler folder change may
# trigger spec2017 rebuild. When --nobuild is added to run, an error "NOT Building" will occur. 
PKG_BUILD_FOLDER="/home/vkaranth/SPEC2017/ampere_spec2017"
DEPS_FOLDER="/home/vkaranth/installs"

# If we want to rebuild spec2017, then reset the package build folder to current package folder.
if [[ "x$@" =~ "--rebuild" ]]; then
    PKG_BUILD_FOLDER=${PKG_FOLDER}
fi

if [[ "x${NUMACMD}" == "x" ]]; then
    NUMACMD="numactl --interleave=all"
fi

set -x

# It should not affect package build, and it will make sure the run stage can still work without
# having the package build folder.
export LD_LIBRARY_PATH=${DEPS_FOLDER}/jemalloc/lib:${LD_LIBRARY_PATH}
source ${PKG_FOLDER}/gcc/install/setup_env.sh

${NUMACMD} ./runcpu --config=ampere_noopt \
  --define numasize=${NUMASIZE} \
  --define gcc_dir=/opt/rh/gcc-toolset-10/root/usr/ \
  --define llvm_dir=${PKG_BUILD_FOLDER}/llvm/install \
  --define jemalloc_dir=${DEPS_FOLDER}/jemalloc \
  --define glibc_dir=${DEPS_FOLDER}/glibc-2.32 \
  $@
