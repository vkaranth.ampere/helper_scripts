
touch /tmp/pwr.tmp
./sensors.sh pwr_tmp_128_mono_1.csv pwr.tmp &
./cycles.sh &
./run_spec2017.sh --iterations 1 --copies 128 --tune=base intrate

pkill perf
rm -rf /tmp/pwr.tmp

