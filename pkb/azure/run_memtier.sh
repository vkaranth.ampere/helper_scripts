#!/bin/bash

d=32
c=2
t=1
p=16
resdir=results/d16ps_c64/redis_1
for t in 1 2 4; do
    for c in 1 2 4 8 16; do
        for p in 2 4 8 16 32 48 64 100; do
            if [[ "$c" -le 4  && "$p" -le 8 ]]; then
                   continue
            fi		   
            res=memtier_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir "$resdir"/"$res"

            #run the workload
	    #redis
	    #python3 pkb.py --owner=vaishali_karanth --benchmarks=memcached_memtier --static_vm_file=configs/static_vms.json --memtier_protocol=memcache_binary --memtier_run_duration=60 --memtier_clients="$c" --memtier_threads="$t"  --memtier_ratio='1:9' --memtier_data_size="$d" --memtier_pipeline="$p"  --csv_path="$resdir"/"$res"/memcached_t2a_res.csv --json_path="$resdir"/"$res"/memcached_t2a_res.json --memcached_num_threads=16 --run_stage=run --run_uri=1bc854d4 --mpstat --mpstat_publish  --mpstat_count=60 --ignore_package_requirements

	    python3 pkb.py --owner=vaishali_karanth   --log_level=debug --static_vm_file=configs/static_vms.json --benchmarks=redis_memtier --memtier_protocol=redis --memtier_run_duration=60 --memtier_clients="$c" --memtier_threads="$t" --memtier_ratio='1:9' --memtier_data_size=32 --memtier_pipeline="$p" --redis_total_num_processes=16 --ignore_package_requirements --run_stage=run --run_uri=1bc854d4 --mpstat --mpstat_publish  --mpstat_count=60 --csv_path="$resdir"/"$res"/redis_d16p_res.csv --json_path="$resdir"/"$res"/redis_d16p_res.json
	    #memcache
            #python3 pkb.py --owner=vaishali_karanth   --log_level=debug  --project=ampere-data-pipeline --gce_nic_type=GVNIC --benchmarks=memcached_memtier --static_vm_file=configs/static_vms.json --memtier_protocol=memcache_binary --memtier_run_duration=60 --memtier_clients="$c" --memtier_threads="$t"  --memtier_ratio='1:9' --memtier_data_size="$d" --memtier_pipeline="$p" --memcached_num_threads=32 --csv_path="$resdir"/"$res"/memcache_t2a_res.csv --json_path="$resdir"/"$res"/memcache_t2a_res.json --run_stage=run --run_uri=1bc854d4 --mpstat --mpstat_publish  --mpstat_count=60 --ignore_package_requirements 
            mv /tmp/perfkitbenchmarker/runs/1bc854d4/memtier_* "$resdir"/"$res"/
            mv /tmp/perfkitbenchmarker/runs/1bc854d4/*stdout "$resdir"/"$res"/
        done
    done
done
