#!/bin/bash

d=32
c=2
t=1
p=16
resdir=results/d16ps_arm/redis
for p in 50 25 100 1 ; do
    for c in 1 2 4; do
        for t in 1 2 4; do
            if [[ "$c" -eq 4  && "$t" -eq 4 ]]; then
                   continue
            fi		   
            res=redis_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir -p "$resdir"/"$res"

            #run the workload
	    python3 pkb.py --ignore_package_requirements --static_vm_file=configs/static_vms.json --benchmarks=redis_memtier --memtier_protocol=redis --memtier_data_size=32  --memtier_ratio='1:9'  --memtier_key_pattern='R:R' --redis_total_num_processes=16   --memtier_clients="$c" --memtier_threads="$t" --memtier_pipeline="$p"   --run_stage=run --run_uri=24727277 --memtier_run_duration=30 --mpstat --mpstat_publish  --mpstat_count=30

            mv /tmp/perfkitbenchmarker/runs/24727277/memtier_* "$resdir"/"$res"/
            mv /tmp/perfkitbenchmarker/runs/24727277/*stdout "$resdir"/"$res"/
        done
    done
done
