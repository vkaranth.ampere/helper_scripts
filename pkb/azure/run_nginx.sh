#!/bin/bash

d=32
c=2
t=1
p=16
resdir=results/d16ps_c64/nginx


for t in 100 50 25; do
    for c in  500 300 200; do
        for r in 50000 100000; do

            res=memtier_r"$r"_t"$t"_c"$c"
            mkdir -p "$resdir"/"$res"
            config="$r":60:"$t":"$c"
            cmd=" --nginx_load_configs=$config "
            echo $cmd
            python3  pkb.py --owner=vaishali_karanth   --log_level=debug --static_vm_file=configs/static_vms.json $cmd --csv_path="$resdir"/"$res"/res.csv  --json_path="$resdir"/"$res"/res.json --ignore_package_requirements --run_stage=run --run_uri=5c480f1c --benchmarks=nginx --nginx_use_ssl --mpstat --mpstat_publish  --mpstat_count=60

            mv /tmp/perfkitbenchmarker/runs/5c480f1c/*stdout "$resdir"/"$res"/
        done
    done
done
