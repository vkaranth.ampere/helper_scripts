#!/bin/bash

d=32
c=2
t=1
p=16
resdir=results/d16ps_arm/memcached
for p in 50 25 100 ; do
    for c in 1 2 4; do
        for t in 1 2 8 16 24; do
            if [[ "$c" -le 4  && "$p" -le 8 ]]; then
                   continue
            fi		   
            res=memcache_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir -p "$resdir"/"$res"

            #run the workload
	    python3 pkb.py --owner=vaishali_karanth --benchmarks=memcached_memtier --static_vm_file=configs/static_vms.json --memtier_protocol=memcache_binary --memtier_run_duration=30 --memtier_key_pattern='R:R' --memtier_clients="$c" --memtier_threads="$t"  --memtier_ratio='1:9' --memtier_data_size="$d" --memtier_pipeline="$p"  --csv_path="$resdir"/"$res"/memcached_arm_res.csv --json_path="$resdir"/"$res"/memcached_arm_res.json --memcached_num_threads=16 --run_stage=run --run_uri=bca0e520 --mpstat --mpstat_publish  --mpstat_count=60 --ignore_package_requirements

            mv /tmp/perfkitbenchmarker/runs/bca0e520/memtier_* "$resdir"/"$res"/
            mv /tmp/perfkitbenchmarker/runs/bca0e520/*stdout "$resdir"/"$res"/
        done
    done
done
