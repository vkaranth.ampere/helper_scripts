#!/bin/bash

d=32
c=2
t=1
p=16
resdir=results/d16av5/nginx


for t in 100 64 32 16 8 1; do
    for c in  200 250 300; do
        for r in 10000 50000 100000; do

            res=memtier_r"$r"_t"$t"_c"$c"
            mkdir -p "$resdir"/"$res"
            config="$r":60:"$t":"$c"
            cmd=" --nginx_load_configs=$config "
            echo $cmd
            python3  pkb.py --owner=vaishali_karanth   --log_level=debug --static_vm_file=configs/static_vms.json $cmd --csv_path="$resdir"/"$res"/res.csv  --json_path="$resdir"/"$res"/res.json --ignore_package_requirements --run_stage=run --run_uri=b69eab1d --benchmarks=nginx --nginx_use_ssl --mpstat --mpstat_publish  --mpstat_count=60

            mv /tmp/perfkitbenchmarker/runs/b69eab1d/*stdout "$resdir"/"$res"/
        done
    done
done
