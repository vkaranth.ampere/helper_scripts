#!/bin/bash

#script that will grab tpt p99 p99_9 latencies for the runs 

#folder where all memtier results are stored
basedir=t2a_32
#basedir=n1sky_32
for d in $basedir/*; do
   if [ -d "${d}" ]; then
      tpt="$(grep Totals $d/* | awk '{SUM+=$2}END{printf("%10d",SUM)}')" 
      p99="$(grep Totals $d/* | sort -nrk9 | head -1 | awk '{printf("%.2f",$9)}')"
      p99_9="$(grep Totals $d/* | sort -nrk10 | head -1 | awk '{printf("%.2f",$10)}')"
      echo $d $tpt $p99 $p99_9
   fi
done
 #grep Totals memtier_results_6* | awk '{SUM+=$2}END{print SUM}'
#grep Totals memtier_results_6* | sort -nrk9 | head -1 | awk '{print $9}'
