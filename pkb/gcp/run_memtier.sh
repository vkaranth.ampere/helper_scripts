#!/bin/bash
# script to run memtier for redis with varying datasize, client, pipeline depth
# to change: yaml file, protocol, res directories, run_uri
d=32
c=2
t=1
p=16
resdir=results/t2a_32
for d in 32 128 1024; do
    for c in 1 2 4; do
	for p in 2 4 8 16 32; do
	    res=memtier_t"$t"_c"$c"_p"$p"_d"$d"
            mkdir "$resdir"/"$res"

	    #run the workload
	    python3 pkb.py --owner=vaishali_karanth   --log_level=debug  --project=ampere-data-pipeline --gce_nic_type=GVNIC --benchmarks=redis_memtier --benchmark_config_file=configs/redis_t2a.yaml --memtier_protocol=redis --memtier_run_duration=60 --memtier_clients="$c" --memtier_threads="$t"  --memtier_ratio='1:9' --memtier_data_size="$d" --memtier_pipeline="$p" --redis_total_num_processes=32 --csv_path="$resdir"/"$res"/redis_t2a_res.csv --json_path="$resdir"/"$res"/redis_t2a_res.json --run_stage=run --run_uri=1d3db906 --mpstat --mpstat_publish  --mpstat_count=60

	    mv /tmp/perfkitbenchmarker/runs/1d3db906/memtier_* "$resdir"/"$res"/
	    mv /tmp/perfkitbenchmarker/runs/1d3db906/*stdout "$resdir"/"$res"/
        done
    done
done

#python3 pkb.py --owner=vaishali_karanth   --log_level=debug  --project=ampere-data-pipeline --gce_nic_type=GVNIC --benchmarks=redis_memtier --benchmark_config_file=configs/redis_t2a.yaml --memtier_protocol=redis --memtier_run_duration=60 --memtier_clients=16 --memtier_threads=1 --memtier_ratio='1:9' --memtier_data_size=32 --memtier_pipeline=16 --redis_total_num_processes=32 --csv_path=redis_t2a_res.csv --json_path=redis_t2a_res.json --run_stage=run --run_uri=1d3db906 --mpstat --mpstat_publish  --mpstat_count=60
