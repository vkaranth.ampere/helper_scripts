#!/bin/bash

#script that will grab tpt p99 p99_9 latencies for the runs

#folder where all memtier results are stored
basedir=d16ps_c64/nginx
#basedir=n1sky_32
for d in $basedir/*; do
      base=$(basename $d)
      rate="$(cut -d '_' -f 2 <<< $base)"
      rate=${rate:1}      
      p99="$(grep 'p99 latency' $d/res.json | awk '{printf("%.2f",$5)}')"
      echo $d $rate $p99 
done
 #grep Totals memtier_results_6* | awk '{SUM+=$2}END{print SUM}'
#grep Totals memtier_results_6* | sort -nrk9 | head -1 | awk '{print $9}'
